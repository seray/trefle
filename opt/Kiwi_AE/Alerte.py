#!/usr/bin/python3
# -*- coding: utf-8 -*-
from tkinter import *
import tkinter as ttk
#import Tkinter as tk

from tkinter import messagebox
import os,pwd
os.getlogin = lambda: pwd.getpwuid(os.getuid())[0]
import locale
from tkcalendar import Calendar, DateEntry
import time
from datetime import date

from lib.libAuxilaire import *
from Config  import *
Nnom=os.getlogin()
date = time.strftime("%d-%m-%y")
affiche_heure = locale.setlocale(locale.LC_ALL, 'fr_FR.utf8')

class PassPlusApp(ttk.Frame):
	def __init__(self, parent,sett,frame_look={}, **look):
		ttk.Frame.__init__(self, parent)
		global time_plus ,date,semaine,moi_anni,date_anni,age,libelle,lieu,adresse,lieudit,tel,portable,mail,thier,list_rd,Sauve_variable,vue,fen02,frame,fen03
		global anni,save,actu,moi_anni,suit,suit_anni,save_anni,actu_anni,submit_anni,anniversaire,list_anni,Sauve_anniversaire,moi_anni,entry_1,entry_2,vbar
		
		self.parent = parent
		self.frame_header = ttk.Frame(parent)
		self.frame_header.pack()
		ttk.Label(self.frame_header, text = "Bienvenue")
		self.frame_pass = ttk.Frame(parent)
		args = dict(relief=SUNKEN, border=1)
		#args.update(frame_look)
		Frame.__init__(self, **args)
		args = {'relief': FLAT}
		args.update(look)		
		
		Date_semaine = time.strftime(" %W") 
		Date_jour = time.strftime("%A %d %B")
		Date_Moi = time.strftime('%B')
		date  = time.strftime("#%d-%m-%y")
		date_anni = time.strftime("%d-%m")
		Affiche_jour = time.strftime('%A %d %B %Y')
		self.frame_pass.pack()
		line5 = '#FFFF00'
		# Format Date
		jours = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]
		mois = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembtre", "Octobre","Novembre","Decembre"]
		list_rd = []
		list_rd = []
		list_anni = []
		list_anni = []
		date_ = int(time.strftime("%d"))
		Sauve_variable = eval(open(chem_gestion+'Plannig/Plannig.dat',"r").read(),{},{})
		Sauve_anniversaire = eval(open(chem_gestion+"Plannig/Anniversaire.dat","r").read(),{},{})
		moi_anni = []
		for x in range(7):
			x = x - time.localtime()[6]
			moi_anni.append(date_+x)
		Semaine = []
		for x in range(7):
			x = x - time.localtime()[6]
			Semaine.append(date_+x)
		
		crearor = tkinter.Label(info_zone,text = "Nom Prénom",width=12,anchor=W,font = 'Arial 12' ).grid(row=1,column=0,padx=10,pady=3,sticky=NW)
		self.libelle = tkinter.Entry(info_zone,font = 'Arial 12')
		self.libelle.grid(row=1,column=0,padx=130,pady=3,ipadx=20,sticky=NW)	
		
		crearor = tkinter.Label(info_zone,text = "Ville ",width=12,anchor=W,font = 'Arial 12' ).grid(row=2,column=0,padx=10,pady=3,sticky=NW)
		self.lieu = tkinter.Entry(info_zone,font = 'Arial 12')
		self.lieu.grid(row=2,column=0,padx=130,pady=3,ipadx=20,sticky=NW)		
				
		crearor = tkinter.Label(info_zone,text = "Adresse ",width=12,anchor=W,font = 'Arial 12' ).grid(row=3,column=0,padx=10,pady=3,sticky=NW)	
		self.adresse = tkinter.Entry(info_zone,font = 'Arial 12')
		self.adresse.grid(row=3,column=0,padx=130,pady=3,ipadx=20,sticky=NW)	
		
		crearor = tkinter.Label(info_zone,text = "Lieu-dit Aptt N°",anchor=W,width=12,font = 'Arial 12' ).grid(row=4,column=0,padx=10,pady=3,sticky=NW)
		self.lieudit = tkinter.Entry(info_zone,font = 'Arial 12')
		self.lieudit.grid(row=4,column=0,padx=130,pady=3,ipadx=20,sticky=NW)
				
		crearor = tkinter.Label(info_zone,text = "Mobile:",width=12,anchor=W,font = 'Arial 12' ).grid(row=5,column=0,padx=10,pady=3,sticky=NW)
		self.portable = tkinter.Entry(info_zone,font = 'Arial 12',width=14)
		self.portable.grid(row=5,column=0,padx=130,pady=3,sticky=NW)
		
		crearor = tkinter.Label(info_zone,text = "Fix :",width=12,anchor=W,font = 'Arial 12' ).grid(row=6,column=0,padx=10,pady=3,sticky=NW)
		self.tel = tkinter.Entry(info_zone,font = 'Arial 12',width=14)
		self.tel.grid(row=6,column=0,padx=130,pady=3,sticky=NW)
		
		crearor = tkinter.Label(info_zone,text = "Mail ",width=12,anchor=W,font = 'Arial 12' ).grid(row=7,column=0,padx=10,pady=3,sticky=NW)
		self.mail = tkinter.Entry(info_zone,font = 'Arial 12',width=24 )
		self.mail.grid(row=7,column=0,padx=130,pady=3,sticky=NW)
		def jour_view():
			global date
			def print_sel():
				print(date.get_date())
			date = DateEntry(info_zone, width=10, background='darkblue',
							foreground='white', borderwidth=2,
							font=("Times",12,""))
			date.grid(row=8,column=0,padx=10,pady=3,ipady=2,sticky=NW)
			date.bind("<<DateEntrySelected>>", print_sel)
		jour_view()
		def mode_view():
			mode_hrs = [" 8.00 hrs"," 9.00 hrs"," 10.00 hrs"," 11.00 hrs"," 12.00 hrs"," 13.00 hrs",
						 " 14.00 hrs" ," 15.00 hrs"," 16.00 hrs"," 17.00 hrs"," 18.80 hrs"," 19.00 hrs"]
			self.mode_hrs = ttk.Combobox(info_zone,values =mode_hrs,font=("Times",12,""),width=8)
			self.mode_hrs.current(0)
			self.mode_hrs.bind("<<ComboboxSelected>>")#,action_hrs)
			self.mode_hrs.grid(row=8,column=0,columnspan=2,padx=110,pady=3,ipady=2,sticky=NW)
		mode_view()
		def mode_sujet():
			liste_sujet = ['Famille','Ami'," Client"," Fourniseur"," Adminstration"," Banque"," Personnel"]
			self.sujet = ttk.Combobox(info_zone,values = liste_sujet,font=("Times",12,""),width=8)
			self.sujet.current(0)
			self.sujet.bind("<<ComboboxSelected>>")#,action_sujet)
			self.sujet.grid(row=8,column=1,columnspan=2,padx=65,pady=3,ipady=2,sticky=NW)
		mode_sujet()
		crearor = tkinter.Label(info_zone,text = "Détail:",width=12,anchor=W,font = 'Arial 12' ).grid(row=9,column=0,padx=0,pady=0,sticky=NW)
		self.thier = tkinter.Text(info_zone,font = 'Arial 12')
		self.thier.configure(height=3,width=30)
		self.thier.grid(row=10,column=0,columnspan=8,padx=0,pady=0,ipady=0,sticky=NW)
		def submit():
			Semaine = lambda d: time.strftime('%W',time.strptime(d,'%d/%m/%Y'))
			Sauve_variable[str(date.get())+self.libelle.get()] = [self.libelle.get(),self.adresse.get(),self.lieu.get(),self.lieudit.get(),self.tel.get(),self.portable.get(),self.mail.get(),self.mode_hrs.get(),self.sujet.get(),self.thier.get("1.0",END),str(date.get()),str(Semaine(date.get()))]
			save()
		def save():
			file_save = open(chem_gestion+'Plannig/Plannig.dat',"w")
			file_save.write(str(Sauve_variable))
			file_save.close()
		def loupe():
			
			#fen02.wm_attributes('-type', 'splash');
			'''
			photo = PhotoImage(file='/opt/Trefle/Icon/exit.png')
			photo = photo.subsample(1,1)
			Ba = Button(fen02, image = photo,command=fen02.destroy, compound=BOTTOM)
			Ba.image = photo
			Ba.pack(side='left', anchor='s',padx=4,pady=3,ipady=8,ipadx=8)
			'''
			actu(save)
		def Mymessagebox():
			global fen2
			fen2 = LabelFrame(anni,bd=1,relief=RAISED)
			fen2.place(x=2,y=0,width=250,height=120) 

		photo = PhotoImage(file=chem_icon+'Ok.png')
		photo = photo.subsample(5,5)
		flecheg = Button(info_zone, image = photo,command = submit)
		flecheg.image = photo
		flecheg.grid(row=11,column=0,padx=5,pady=5,sticky=NW)

		photo = PhotoImage(file=chem_icon+'Loupe.png')
		photo = photo.subsample(6,6)
		loupe = Button(info_zone, image = photo,command = loupe)
		loupe.image = photo
		loupe.grid(row=11,column=0,padx=56,ipady=4,ipadx=1,sticky=NW)

		photo = PhotoImage(file=chem_icon+'aide.png')
		photo = photo.subsample(5,5)
		Ba = Button(info_zone, image = photo,command=aide_alerte)
		Ba.image = photo
		Ba.grid(row=11,column=0,padx=106,ipady=2,ipadx=2,sticky=NW)
		
		def sortie_():
			sett()
			plann.destroy()
		
		photo = PhotoImage(file=chem_icon+'sortie.png')
		photo = photo.subsample(5,5)
		Ba = Button(info_zone, image = photo,command=sortie_, compound=BOTTOM)
		Ba.image = photo
		Ba.grid(row=11,column=0,padx=158,ipady=0,ipadx=0,sticky=NW)
		#### *** Anniversaire ***
		def submit_anni():
			global moi_anni,es1,fen2
			es1 = self.entry_1.get()
			es2 = self.entry_2.get()
			'''
			if self.entry_1.get() == "":
				return
			if self.entry_2.get() == "":
				return
			'''
			if int(es1) > 0 and  int(es1) < 32:	
				if int(es2) > 0 and  int(es2) < 13:
					moi_anni = self.entry_1.get() + "-" + self.entry_2.get()
					Sauve_anniversaire[str(moi_anni)+self.entry_2.get()] = [self.entry_2.get(),self.anni1.get(),self.adresse_anni.get(),self.ville_anni.get(),self.tel_anni.get(),self.portable_anni.get(),self.mail_anni.get(),self.detail_anni.get("1.0",END),str(moi_anni)]#(self.entry_1.get()))]
				else:
					print('no')
					Mymessagebox()
					Mywarning(fen2)
					Label(fen2, text="  Date invalide\n01 à 31 jour\n 01 à 12 mois",font=('verdana', 12)).place(x=50,y=10,width=180)
					sortie(fen2)
			else:
				print('no')
				Mymessagebox()
				Mywarning(fen2)
				Label(fen2, text="  Date invalide\n01 à 31 jour\n 01 à 12 mois",font=('verdana', 12)).place(x=50,y=10,width=180)
				sortie(fen2)
			save_anni()
		def save_anni():
			file_save = open(chem_gestion+'Plannig/Anniversaire.dat',"w")
			file_save.write(str(Sauve_anniversaire))
			file_save.close()
		def loupe_anni():
			#global Sauve_anniversaire,list_anni,fen03,fenpf_anni,frame,canvas
			actu_anni(save_anni)
		crearor = tkinter.Label(anni,text = "Nom Prénom",width=15,anchor=W,font = 'Arial 12' ).grid(row=0,column=0,padx=5,pady=3,sticky=NW)
		self.anni1 = tkinter.Entry(anni,font = 'Arial 12',width=20)
		self.anni1.grid(row=0,column=0,padx=130,pady=3,ipadx=20,sticky=NW)
		
		crearor = tkinter.Label(anni,text = "Date jour & mois",width=15,anchor=W,font = 'Arial 12').grid(row=1,column=0,padx=5,pady=3,sticky=NW)
				
		crearor = tkinter.Label(anni,text = "Ville ",width=15,anchor=W,font = 'Arial 12' ).grid(row=2,column=0,padx=5,pady=3,sticky=NW)
		self.ville_anni = tkinter.Entry(anni,font = 'Arial 12')
		self.ville_anni.grid(row=2,column=0,padx=130,pady=3,ipadx=20,sticky=NW)		
				
		crearor = tkinter.Label(anni,text = "Adresse ",width=12,anchor=W,font = 'Arial 12' ).grid(row=3,column=0,padx=5,pady=3,sticky=NW)	
		self.adresse_anni = tkinter.Entry(anni,font = 'Arial 12')
		self.adresse_anni.grid(row=3,column=0,padx=130,pady=3,ipadx=20,sticky=NW)	
						
		crearor = tkinter.Label(anni,text = "Mobile:",width=12,anchor=W,font = 'Arial 12' ).grid(row=4,column=0,padx=5,pady=3,sticky=NW)
		self.portable_anni = tkinter.Entry(anni,font = 'Arial 12',width=10)
		self.portable_anni.grid(row=4,column=0,padx=130,pady=3,sticky=NW)
		
		crearor = tkinter.Label(anni,text = "Fix :",width=12,anchor=W,font = 'Arial 12' ).grid(row=5,column=0,padx=5,pady=3,sticky=NW)
		self.tel_anni = tkinter.Entry(anni,font = 'Arial 12',width=10)
		self.tel_anni.grid(row=5,column=0,padx=130,pady=3,sticky=NW)
		
		crearor = tkinter.Label(anni,text = "Mail ",width=12,anchor=W,font = 'Arial 12' ).grid(row=6,column=0,padx=5,pady=3,sticky=NW)
		self.mail_anni = tkinter.Entry(anni,font = 'Arial 12',width=24 )
		self.mail_anni.grid(row=6,column=0,padx=130,pady=3,sticky=NW)		
		mode_sujet()
		crearor = tkinter.Label(anni,text = "Détail:",width=12,anchor=W,font = 'Arial 12' ).grid(row=7,column=0,sticky=NW)
		self.detail_anni = tkinter.Text(anni,font = 'Arial 12')
		self.detail_anni.configure(height=1,width=30)
		self.detail_anni.grid(row=8,column=0,columnspan=8,sticky=NW)
				
		photo = PhotoImage(file=chem_icon+'Ok.png')
		photo = photo.subsample(6,6)
		flecheg = Button(anni,borderwidth=1, image = photo,command = submit_anni)
		flecheg.image = photo
		flecheg.grid(row=10,column=0,ipady=1,ipadx=1,sticky=NW)	

		photo = PhotoImage(file=chem_icon+'Loupe.png')
		photo = photo.subsample(7,7)
		loupe = Button(anni,borderwidth=1, image = photo,command = loupe_anni)
		loupe.image = photo
		loupe.grid(row=10,column=0,padx=48,ipady=1,ipadx=1,sticky=NW)
		def sortie2():
			sett()
			plann.destroy()
			
		photo = PhotoImage(file=chem_icon+'sortie.png')
		photo = photo.subsample(7,7)
		Ba = Button(anni,borderwidth=1, image = photo,command=sortie2)
		Ba.image = photo
		Ba.grid(row=10,column=0,padx=92,ipady=1,ipadx=1,sticky=NW)			

		self.entry_1 = Entry(anni,font = 'Arial 12',bd=4,bg='White',width=2,**args)
		self.entry_1.grid(row=1,column=0,padx=130,pady=3,ipadx=0,sticky=NW)		
		self.entry_2 = Entry(anni,font = 'Arial 12',bg='White',bd=4 , width=2,**args)
		self.entry_2.grid(row=1,column=0,padx=180,pady=3,ipadx=0,sticky=NW)						
		
		self.entries = [self.entry_1, self.entry_2]
		self.entry_1.bind('<KeyRelease>', lambda e: self._check(0,2))
		self.entry_2.bind('<KeyRelease>', lambda e: self._check(1,2))			
	
	def _check(self, index, size):
		entry = self.entries[index]
		next_index = index + 1
		next_entry = self.entries[next_index] if next_index < len(self.entries) else None
		data = entry.get()
		if len(data) >= size and next_entry:
			next_entry.focus()
	def give_date(self):
		return self.titre[int(self.entry_2.get())]#+'-'+self.entry_3.get()+'-'+self.entry_4.get()+'-'+self.entry_5.get()+'-'+self.entry_6.get()+'-'+self.entry_7.get()+'-'+self.entry_8.get()		
######### Anniversaire
def suit_anni(text):
	def execute():
		global anni
		fenpf_anni = tkinter.Toplevel(plann)
		fenpf_anni.geometry("380x100+600+410") 
		L = tkinter.Label(fenpf_anni,text=text)
		L.pack()
	return execute
def suprimLigne_anni(k,save_anni):
	def sup():
		global Sauve_anniversaire,fen03
		del Sauve_anniversaire[k]
		actu_anni(save_anni)
	return sup				
def actu_anni(save_anni):
	global Sauve_anniversaire,list_anni,fen04,fen03,frame2
	try:
		fen04.destroy()
	except:
		pass
	fen04=tkinter.Toplevel(plann)
	fen04.geometry("380x100+600+210")
	fen03=Canvas(fen04,width=350,height=100)
	frame2=Frame(fen03)
	myscrollbar=Scrollbar(fen04,orient="vertical",command=fen03.yview) # will be visible if the frame2 is to to big for the canvas
	fen03.create_window((0,0),window=frame2,anchor='nw')
	for k in range(len(list_anni)):
		list_anni[0][0].place_forget()
		list_anni[0][1].place_forget()
		list_anni[0][2].place_forget()
		del list_anni[0]
	list_anni= []	
	t = 0
	for k in Sauve_anniversaire:
		print(k,t)
		frame_line = tkinter.Frame(frame2)
		list_anni1 = tkinter.Label(frame_line,text = Sauve_anniversaire[k][8]+'  '+Sauve_anniversaire[k][1],width=30,anchor="w", bg='#FFFF00',font = 'Arial 10' )
		list_anni2 = tkinter.Button(frame_line,text = "Lire",width=2,command = suit_anni(
			Sauve_anniversaire[k][8]+"  "+Sauve_anniversaire[k][1]+' \n '+ Sauve_anniversaire[k][2]+'\n  '+Sauve_anniversaire[k][3]+'\n  '+Sauve_anniversaire[k][4]+'\n'+Sauve_anniversaire[k][5]+'\n  '+Sauve_anniversaire[k][6]+'\n  '+Sauve_anniversaire[k][7]+'\n  '+Sauve_anniversaire[k][8])
		)
		list_anni3 = tkinter.Button(frame_line,text="Suprimer",width=8,command = suprimLigne_anni(k,save_anni))
		list_anni1.pack(side="left",ipady=4)
		list_anni2.pack(side="left")
		list_anni3.pack(side="left")
		frame_line.pack(side="bottom")
		t += 1
	frame2.update() 
	fen03.configure(yscrollcommand=myscrollbar.set, scrollregion="0 0 0 %s" % frame2.winfo_height())
	fen03.pack(side=LEFT)
	myscrollbar.pack(side=RIGHT, fill = Y)
	save_anni()
######## planning
def suit(text):
	def execute():
		global plann
		fenpf = tkinter.Toplevel(plann)
		fenpf.geometry("380x100+600+210")
		L = tkinter.Label(fenpf,text=text)
		L.pack()
	return execute
def suprimLigne(k,save):
	def sup():
		global Sauve_variable,fen02
		del Sauve_variable[k]
		actu(save)
	return sup		
def actu(save):
	global Sauve_variable,list_rd,fenpf,fen14,fen13,frame3
	try:
		fen14.destroy()
	except:
		pass
	fen14=tkinter.Toplevel(plann)
	fen14.geometry("380x100+600+430")
	#fen14.wm_attributes("-topmost", 1)
	fen13=Canvas(fen14,width=350,height=100)
	frame3=Frame(fen13)
	frame3.place(x=590, y=3,height=57, width=57)

	myscrollbar=Scrollbar(fen14,orient="vertical",command=fen13.yview) 
	fen13.create_window((0,0),window=frame3,anchor='nw')
	for k in range(len(list_rd)):
		list_rd[0][0].place_forget()
		list_rd[0][1].place_forget()
		list_rd[0][2].place_forget()
		del list_rd[0]
	list_rd= []
	t = 0
	for k in Sauve_variable:
		print(k,t)
		frame_line = tkinter.Frame(frame3)
		list_rd1 = tkinter.Label(frame_line,text = Sauve_variable[k][10]+'  '+Sauve_variable[k][0],width=28, bg='#FFFF00',font = 'Arial 11' )
		list_rd2 = tkinter.Button(frame_line,text = "Lire",command = suit(
			Sauve_variable[k][0]+"  "+Sauve_variable[k][1]+'\n  '+ Sauve_variable[k][2]+'  '+Sauve_variable[k][3]+' \n   '+Sauve_variable[k][4]+'  '+Sauve_variable[k][5]+'  '+Sauve_variable[k][6]+' \n   '+Sauve_variable[k][7]+'  '+Sauve_variable[k][8]+'\n'+Sauve_variable[k][9]+'  '+Sauve_variable[k][10]+'  '+Sauve_variable[k][11])
		)
		list_rd3 = tkinter.Button(frame_line,text = "suprimer",command = suprimLigne(k,save))
		list_rd1.pack(side="left",ipady=4)
		list_rd2.pack(side="left")
		list_rd3.pack(side="left")
		frame_line.pack(side="bottom")
		t += 1
	frame3.update() 
	fen13.configure(yscrollcommand=myscrollbar.set, scrollregion="0 0 0 %s" % frame3.winfo_height())
	fen13.pack(side=LEFT)
	myscrollbar.pack(side=RIGHT, fill = Y)
	save()	
def aide_alerte():
	try:
		plann.wm_attributes('-topmost', False)
		os.system('xdg-open '+ chem_aide + 'Module-Alerte.pdf')	
	except:
		messagebox.showerror("Erreure","Pas de fichier ",parent=plann)	
		#plann.wm_attributes('-topmost', True)
def main(sett,fenètrePrimère = None):
	global info_zone,anni,plann,fen02,frame,fen03,fen04,fen2,setBotun
	plann = tkinter.Toplevel(fenètrePrimère)
	plann.config(relief = RAISED,bd=1 )
	#plann.bind('<Escape>',  lambda event:plann.destroy())
	#plann.protocol('WM_DELETE_WINDOW', lambda: None)
	plann.wm_attributes('-type','splash')
	plann.transient(fenètrePrimère)
	#plann.attributes('-topmost', True)
	try:
		x  = fenètrePrimère.winfo_x()
		y  = fenètrePrimère.winfo_y()
		width  = 360
		height  = 420
		dx = ligne[20] #80
		dy = ligne[21]
		plann.geometry("%dx%d+%d+%d" % (width,height, x + int(str(dx)), y + int(str(dy))))
	except:
		messagebox.showinfo("Information","Dimention par defaut\n Probléme voire configuration") 
		plann.geometry('790x350')
		ligne[9] = '790x350'
	
	notebook =  ttk.Notebook(plann,style="TNotebook")
	notebook.place(x=16, y=16)
	info_zone = ttk.Frame(notebook)
	anni = ttk.Frame(notebook)
	notebook.add(info_zone, text="Rendez_vous")
	notebook.add(anni, text="Rappel annuel")
	notebook.pack(expand=0, fill="both")
	
	plann.bind('<Return>')
	passPlusApp = PassPlusApp(plann,sett)
	passPlusApp.pack()

	plann.mainloop()	
if __name__ == '__main__':
	main()