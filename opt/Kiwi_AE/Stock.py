#!/usr/bin/python3
# -*- coding: utf-8 -*-
import tkinter
from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
import tkinter.font as tkFont
#from tkinter import messagebox
import os
import locale
from tkcalendar import Calendar, DateEntry
import time
from datetime import date
import tkinter as tk
from lib.libAuxilaire import *
from Config  import *
import sqlite3
from tkinter import simpledialog
#from random import choice
import os
#from PIL import Image
import csv
import Tier as Tier
import saveLoadTier
police = ("arial", 10,'bold')

#function to define database
def Database():
    global conn, cursor
    conn = sqlite3.connect(liste_stock)
    cursor = conn.cursor()
    cursor.execute(
        "CREATE TABLE IF NOT EXISTS REGISTRE (RID INTEGER NOT NULL  PRIMARY KEY AUTOINCREMENT,REFERENCE TEXT,DDATE DATETIME,DESIG TEXT,QTE REAL,MIS TEXT,MARQUE TEXT,FNAME TEXT,FAMILLE TEXT,UHT REAL,THT REAL)")
def acount_stock():
    conn = sqlite3.connect(liste_stock)
    cur = conn.cursor()
    value = cur.execute("SELECT SUM(QTE) FROM REGISTRE")
    value = value.fetchone()[0]
    conn.commit()
    conn.close()
    return value
def acount_achat():
    conn = sqlite3.connect(liste_stock)
    cur = conn.cursor()
    value = cur.execute("SELECT SUM(QTE*UHT) FROM REGISTRE")
    value = value.fetchone()[0]
    conn.commit()
    conn.close()
    return value
def acount_vente():
    conn = sqlite3.connect(liste_stock)
    cur = conn.cursor()
    value = cur.execute("SELECT SUM(QTE*THT) FROM REGISTRE")
    value = value.fetchone()[0]
    conn.commit()
    conn.close()
    return value
def acount_Marge():
    conn = sqlite3.connect(liste_stock)
    cur = conn.cursor()
    valeur = cur.execute("SELECT SUM(100-UHT / THT *100) FROM REGISTRE")
    valeur = valeur.fetchone()[0]
    conn.commit()
    conn.close()
    return valeur
def print_acount(func):
    def run(*args,**kwargs):
        
        func(*args,**kwargs)
        sto = int(acount_stock())
        if sto != None:
            stock1.config(text = str(sto),anchor=CENTER)   
            if sto  > 1:
                stock1.config(text = str(sto)+' Articles',width=15,height=2,bg='#F6F6D0',anchor=CENTER)
            else:
                stock1.config(text = str(sto)+' Articles',width=15,height=2,bg='#F6F6D0',anchor=CENTER)    
        ach2 = acount_achat()
        if ach2 != None:
            achat2.config(text = 'Achat : ' + str(float(ach2))+' €',width=16,height=2,bg='#F6F6D0',anchor=CENTER)   
        vt3 = acount_vente()
        if vt3 != None:
            vente3.config(text = 'Ventes : ' + str(float(vt3))+' €',width=16,height=2,bg='#F6F6D0',anchor=CENTER)
        marbrut = acount_Marge()
        if marbrut != None:
            marge_brut.config(text = 'Marge Stock '+ str(round(marbrut,2))+' %',width=20,height=2,bg='#F6F6D0',anchor=CENTER) 
    return run
    combox_style() 
def write_to_ods():
    global fetch,fichier_ods
    Database()
    try:    
        file = open(r''+ '/home/'+ Nnom +'/' + '/test.csv','w')
        text = ""
        for rows in fetch:
            text += 'N°{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10}'.format(rows[0],rows[1],rows[2],rows[3],rows[4],rows[5],rows[6],rows[7],rows[8],rows[9],rows[10])
            text += "\n"
        file.write(text)
        file = open(r''+ '/home/'+ Nnom +'/test.csv')
        file.close()
        Label(fen2,text='/home/'+ Nnom +'/test.csv', font=('verdana', 10 )).place(x=30,y=59) 
    except:
        fen_stock.attributes('-topmost',  False) 
        messagebox.showinfo("Ok", "il faut selectionnais Ref ! ou\n Marque Identifiant\nFourniseur, Famille ...",parent=fen_stock) 
        fen_stock.attributes('-topmost',  True)    
def Couleur():
    colors = ["#FFFFFF", "#E0FAE0"]
    t = 0
    for child in tree.get_children():
        tree.item(child, tags=(colors[t]))
        t += 1
        t %= len(colors)
    for color in colors:
        tree.tag_configure(color, background=color)   
####################################################################
########################## Boutton Action Bas ######################
####################################################################
@print_acount
def DisplayData():
    Database()
    tree.delete(*tree.get_children())
    cursor=conn.execute("SELECT * FROM REGISTRE")
    cursor=conn.execute("SELECT * FROM REGISTRE  ORDER BY  (substr(REFERENCE, 7, 4) || '-' || substr(REFERENCE, 4, 2) || '-' || substr(REFERENCE, 1, 2))ASC;")   
    fetch = cursor.fetchall()
    for data in fetch:
        tree.insert('', 'end', values=(data))
        tree.bind("<Double-1>",OnDoubleClick)
    cursor.close()
    conn.close()                # Départ de la base 
@print_acount
def Reset():
    global jour
    tree.delete(*tree.get_children())
    DisplayData()
    SEARCH.set("")
    #label_uht['state'] = NORMAL
    #label_tht['state'] = NORMAL
    ref        .set('')
    #ddate      .set('')
    desig      .set("")
    qte        .set("")
    mis        .set('')
    marque     .set('')
    fname      .set("")
    famille    .set('')
    uht        .set('')
    tht        .set('')
    Couleur()   
def ExitApp():
    if not tree.selection():
        messagebox.showerror("Ok", "Sélectionnez la ligne à supprimer",parent=fen_stock)
    else:
        result = messagebox.askquestion("Attention", "Êtes-vous sûr de vouloir\n supprimer cet\n enregistrement?",parent=fen_stock)
        if result == 'yes':
            Yes_yes()
        else:
            pass
def Yes_yes():
    curItem = tree.focus()
    contents = (tree.item(curItem))
    selecteditem = contents['values']
    tree.delete(curItem)
    cursor=conn.execute("DELETE FROM REGISTRE WHERE RID = %d" % selecteditem[0])
    conn.commit()
    cursor.close()
    conn.close()
    Couleur()
@print_acount
def Delete():
    Database()
    ExitApp()
def thier():
    Tier.main()
def nom_Tier():
    return list(saveLoadTier.load_to_list())
def _validate(s):  #_validate(s):
    global nom
    dic_liste = Chem_gestion_Kiwi_AE+'tier/listTier.dat'
    nv = [ x for x in dic_liste() if x.startswith(s) ]
    nom['values'] = nv
    return True
def Mymessagebox():
    global fen2
    fen2 = LabelFrame(fen_stock,bd=1,relief=RAISED)
    fen2.place(x=2,y=0,width=250,height=120)   
@print_acount
def up():
    if not tree.selection():
        messagebox.showerror("Ok", "Sélectionnez la ligne à monter",parent=fen_stock) 
    else:
        rows = tree.selection()
        for row in rows:
            tree.move(row, tree.parent(row), tree.index(row)-1)         
@print_acount
def down():
    if not tree.selection():
        messagebox.showerror("Ok", "Sélectionnez la ligne à descentre",parent=fen_stock) 
    else:
        rows = tree.selection()
        for row in rows:
            tree.move(row, tree.parent(row), tree.index(row)+1) 
@print_acount
def OnDoubleClick(self):
    global ajour,Menu_saisie,jour,fen2
    curItem = tree.focus()
    contents = (tree.item(curItem))
    selecteditem = contents['values']
    ref.set        (selecteditem[1])
    #ddate.set      (selecteditem[2])
    desig.set      (selecteditem[3])
    qte.set        (selecteditem[4])
    mis.set        (selecteditem[5])
    marque.set     (selecteditem[6])
    fname.set      (selecteditem[7])
    famille.set    (selecteditem[8])
    uht.set        (selecteditem[9])
    tht.set        (selecteditem[10]) 
    Couleur()      
@print_acount
def register():
    Database()
    ref1     = ref.get()
    ddate1   = ddate.get()
    desig1   = desig.get()
    qte1     = qte.get()
    mis1     = mis.get()
    marque1  = marque.get()
    fname1   = fname.get()
    famille1 = famille.get()
    uht1     = uht.get()
    tht1     = tht.get()
    ref1     = ref.get()
    if  ref1 =='' or ddate1=='' or desig1=='' or qte1=='' or mis=='' or marque1=='' or fname1=='' or famille1=='' or uht1=='' or tht1=='':   
        messagebox.showerror("Erreure", "Remplir le\nou les champs vide!!!",parent=fen_stock)
    else:
        #  Exécuter l'ordre
        conn.execute('INSERT INTO REGISTRE (REFERENCE,DDATE,DESIG,QTE,MIS,MARQUE,FNAME,FAMILLE,UHT,THT) \
              VALUES (?,?,?,?,?,?,?,?,?,?)',(ref1,ddate1,desig1,qte1,mis1,marque1,fname1,famille1,uht1,tht1));
        conn.commit()
        messagebox.showinfo("Ok", "Mis à jour avec succés",parent=fen_stock)
        fetch = cursor.fetchall()
        for data in fetch:
            tree.insert('', 'end', values=(data))
        DisplayData()
        conn.close()              # Mettre a jour
    Couleur()    
@print_acount
def Update():
    Database()
    ref1     = ref.get()
    ddate1   = ddate.get()
    desig1   = desig.get()
    qte1     = qte.get()
    mis1     = mis.get()
    marque1  = marque.get()
    fname1   = fname.get()
    famille1 = famille.get()
    uht1     = uht.get()
    tht1     = tht.get()
    #tht1 = calcultht()
    
    if  ref1 =='' or ddate1=='' or desig1=='' or qte1==''or mis=='' or marque1=='' or fname1=='' or famille1=='' or uht1=='' or tht1=='':     
            messagebox.showerror("Erreure", "Remplir le \nchamp vide!!!",parent=fen_stock)
    else:
        curItem = tree.focus()
        contents = (tree.item(curItem))
        selecteditem = contents['values']
        conn.execute('UPDATE REGISTRE SET REFERENCE=?,DDATE=?,DESIG=?,QTE=?,MIS=?,MARQUE=?,FNAME=?,FAMILLE=?,UHT=?,THT=? WHERE RID = ?',(ref1,ddate1,desig1,qte1,mis1,marque1,fname1,famille1,uht1,tht1,selecteditem[0]))
        conn.commit()                                                                                                                                           
        messagebox.showinfo("Ok", "Mis à jour \n avec succés",parent=fen_stock)
        Reset()
        DisplayData()
        conn.close()
    Couleur()                # Sauvegarde     
def aide_stock():
    try:
        fen_stock.attributes('-topmost', False)
        os.system('xdg-open '+ chem_aide + 'Article.pdf')
    except:
        messagebox.showerror("Erreure","Pas de fichier ",parent=fen_stock)    

        pass
####################################################################
########################## Boutton barre ###########################
####################################################################    
def Edition_ref():
    global fen2
    Mymessagebox()
    def RechecheRecord():
        global fetch,fichier_ods
        Database()
        if REF.get() != "":
            tree.delete(*tree.get_children())
            cursor=conn.execute("SELECT * FROM REGISTRE WHERE REFERENCE LIKE ?", ('%' + str(REF.get()) + '%',))
            fetch = cursor.fetchall()
            for data in fetch:
                tree.insert('', 'end', values=(data))
            cursor.close()
            conn.close()   
    btn_recheche = Button(fen2, text="Sélection Reférence",font=('verdana', 10), command=RechecheRecord,background='#4E9A06',foreground='white',relief=RAISED,bd=4)#,bg=pale)
    btn_recheche.place(x=65,y=5,height=25,width=180)
    
    ref = Entry(fen2, textvariable=REF, font=('verdana', 10 ),width=10)
    ref.place(x=65,y=29,width=180,height=25) 
    interro(fen2)
    sortie(fen2)
    Couleur()
def Edition_fname():
    global fen2
    Mymessagebox()
    def RechecheRecord():
        global fetch,fichier_ods
        Database()
        if RECHERCHER.get() != "":
            tree.delete(*tree.get_children())
            cursor=conn.execute("SELECT * FROM REGISTRE WHERE FNAME LIKE ?", ('%' + str(RECHERCHER.get()) + '%',))
            fetch = cursor.fetchall()
            for data in fetch:
                tree.insert('', 'end', values=(data))
            cursor.close()
            conn.close()   
    btn_recheche = Button(fen2, text="Sélection Nom",font=('verdana', 10), command=RechecheRecord,background='#4E9A06',foreground='white',relief=RAISED)#,bg=pale)
    btn_recheche.place(x=65,y=5,height=25,width=180)
    listecredit = nom_Tier()   
    validate = (fen2.register(_validate), '%P') 
    recheche = ttk.Combobox(fen2,width=14,values = listecredit,font=('arial', 12),textvariable=RECHERCHER, validatecommand=validate,validate='key')
    recheche.place(x=65,y=29,width=180,height=25) 
    Couleur()
    interro(fen2)
    sortie(fen2)
    ######### Bon
def Edition_marque():
    global fen2
    Mymessagebox()
    def voire_marque():
        global fetch,fichier_ods
        Database()
        if AFFICHE_MARQUE.get() != "":
            tree.delete(*tree.get_children())
            cursor=conn.execute("SELECT * FROM REGISTRE WHERE MARQUE LIKE ?", ('%' + str(AFFICHE_MARQUE.get()) + '%',))
            fetch = cursor.fetchall()
            for data in fetch:
                tree.insert('', 'end', values=(data))
            cursor.close()
            conn.close()   
    btn_recheche = Button(fen2, text="Sélection marque",font=('verdana', 10), command=voire_marque,background='#4E9A06',foreground='white',relief=RAISED)#,bg=pale)
    btn_recheche.place(x=65,y=5,height=25,width=180)

    Famille_Marque = open(chem_note+'Famille_Marque',"r").readlines()
    for t in range(len(Famille_Marque)):
        Famille_Marque[t] = Famille_Marque[t][:len(Famille_Marque[t])-1]
    Famille_Marque = ttk.Combobox(fen2,width=10,values = Famille_Marque,state="readonly",font=("calibri",10),textvariable=AFFICHE_MARQUE)
    Famille_Marque.place(x=65,y=29,width=180,height=25)
    interro(fen2)
    sortie(fen2)
def Edition_famille():
    global fen2,fetch
    Mymessagebox()
    def voire_famille():
        global fetch,fichier_ods
        Database()
        if AFFICHE_FAMILLE.get() != "":
            tree.delete(*tree.get_children())
            cursor=conn.execute("SELECT * FROM REGISTRE WHERE FAMILLE LIKE ?",('%' + str(AFFICHE_FAMILLE.get()) + '%',))
            fetch = cursor.fetchall()
            for data in fetch:
                tree.insert('', 'end', values=(data))
            cursor.close()
            conn.close()
            fichier_ods = AFFICHE_FAMILLE.get()
    btn_famille = Button(fen2,text="Famille Article",font=('verdana', 11),width=12,command=voire_famille,background='#4E9A06',foreground='white',relief=RAISED)#,bg=pale)
    btn_famille.place(x=65,y=5,height=25,width=130)
    
    Famille = open(chem_note+'Famille_article',"r").readlines()
    for t in range(len(Famille)):
        Famille[t] = Famille[t][:len(Famille[t])-1]
    famille = ttk.Combobox(fen2,values = Famille,state="readonly",font=("calibri",10),textvariable=AFFICHE_FAMILLE)
    famille.place(x=65,y=29,width=180,height=25)
    interro(fen2)
    sortie(fen2)
######### Menu #########
def set_value_combobox():
    global country
    c = []
    Database()
    conn = sqlite3.connect(liste_stock)
    cursor=conn.execute("SELECT REFERENCE FROM REGISTRE;")
    fetch = cursor.fetchall()
    for data in fetch:
        c.append(data[0])
    cursor.close()
    conn.close()
    country['values'] = c
    Couleur()
def calcul_Marge(*args):
    global M_brut,uht,tht   
    if not isInt_str(uht.get()):
        marge.config(text="Erreure")
        return None
    if not isInt_str(tht.get()):
        marge.config(text="Erreure")
        return None    
    M_brut = 100 - float(float(uht.get())) / float(float(tht.get())) * 100.0
    M_brut = str(round(M_brut,2))+ " %"
    marge.config(text=M_brut)
    return M_brut 
    Couleur() 
def isInt_str(v):
    v = str(v).strip()
    return v== '0' or (v if v.find('..') > -1 else v.lstrip('-+'))
def combox_style():
    dropdown_width = 200
    style = ttk.Style()
    style.configure('TCombobox', postoffset=(0, 0, dropdown_width, 0))
@print_acount
def Menu_saisie(sett):#metre_a_jour = True):
    global tree,ajour,country,Sortie_Stock ,width,marge_B,label_Vente,M_brut,marge
    global affiche_saisie,fname,famille,ref,ddate,tht,qte,mis,marque,uht,Menu_saisie,label_tht,label_uht 
    curItem = tree.focus()
    contents =(tree.item(curItem))
    selecteditem = contents['values']
    color01 = ligne[10]#('#15244C')
    color03 = 'black'
    bg_color = '#EEEEEC'
    
    date = time.strftime("%d-%m-%Y")
    ddate=Entry(affiche_saisie,width=9,font=("calibri",10),textvariable=ddate)
    ddate.grid(row=0,column=0,padx=4,ipadx=4,sticky=NW)
    ddate.insert(0,date)
    # *** Nom prénom tier
    Label(affiche_saisie,text="Réference",font=("calibri",11),bg=ligne[10],fg=color03).grid(row=0,column=1,padx=2,ipadx=4,sticky=NW)  
    combox_style()
    def callbackFunc(event):
        country = event.widget.get() 
    # Creation Combobox
    country = ttk.Combobox(affiche_saisie,width=8,font=("calibri",11),textvariable=ref) 
    country.grid(row=0,column=2,padx=2,ipadx=2,sticky='nw')
    country.current()
    set_value_combobox()
    
    Label(affiche_saisie, text="Designation",font=("calibri",11),bg=ligne[10],fg=color03).grid(row=0,column=3,padx=5,sticky='nw')
    designation = open(chem_note +'designation',"r").readlines()
    for t in range(len(designation)):
        designation[t] = designation[t][:len(designation[t])-1]
    designation = ttk.Combobox(affiche_saisie,width=30,values = designation,font=("calibri",11),textvariable=desig)
    designation.grid(row=0,column=4,padx=1,ipadx=0,pady=3,sticky=NW)
    
    Label(affiche_saisie, text="Fourniseur ", font=("calibri",11),bg=ligne[10],fg=color03).grid(row=0,column=5,padx=0,sticky=NW)
    def mouse_click(event):
        global nom
        Tier.main(affiche_saisie,sett)
    listecredit = nom_Tier()   
    validate = (affiche_saisie.register(_validate), '%P') 
    fname_Combobox = ttk.Combobox(affiche_saisie,width=13,values = listecredit,font=("calibri",11),textvariable=fname, validatecommand=validate,validate='key')
    fname_Combobox.grid(row=0,column=6,padx=0,sticky=NW)
    
    Label(affiche_saisie, text="Famille",font=("calibri",11),bg=ligne[10],fg=color03).grid(row=1,column=0,padx=5,ipadx=1,ipady=1,sticky=SW)
    ListFamille = open(chem_note+'Famille_article',"r").readlines()
    for t in range(len(ListFamille)):
        ListFamille[t] = ListFamille[t][:len(ListFamille[t])-1]
    famille = ttk.Combobox(affiche_saisie,values = ListFamille,state="readonly",font=("calibri",11),width=10,textvariable=AFFICHE_FAMILLE)
    famille.grid(row=1,column=0,columnspan=3,padx=65,ipadx=0,sticky=SW)
    
    Label(affiche_saisie, text="Marque", font=("calibri",11),bg=ligne[10],fg=color03).grid(row=1,column=1,columnspan=4,padx=85,sticky=NW)
    Famille_Marque = open(chem_note+'Famille_Marque',"r").readlines()
    for t in range(len(Famille_Marque)):
        Famille_Marque[t] = Famille_Marque[t][:len(Famille_Marque[t])-1]
    Famille_Marque = ttk.Combobox(affiche_saisie,width=15,values = Famille_Marque,state="readonly",font=("calibri",11),textvariable=marque)
    Famille_Marque.grid(row=1,column=2,columnspan=5,padx=60,ipadx=0,sticky=SW)
    ###
    Label(affiche_saisie, text="Quantité",font=("calibri",11),bg=ligne[10],fg=color03).grid(row=1,column=3,columnspan=5,padx=120,ipadx=0,sticky=NW)
    Entry(affiche_saisie,font=("Arial",12),width=5,textvariable=qte).grid(row=1,column=4,padx=90,ipadx=0,sticky=NW)
    
    Label(affiche_saisie, text="Unité", font=("calibri",11),bg=ligne[10],fg=color03).grid(row=1,column=4,columnspan=5,padx=145,ipadx=1,sticky=NW)
    unite = open(chem_note+'Conditionnement',"r").readlines()
    for t in range(len(unite)):
        unite[t] = unite[t][:len(unite[t])-1]
    unite = ttk.Combobox(affiche_saisie,width=4,values = unite,state="readonly",font=("calibri",11),textvariable=mis)
    unite.grid(row=1,column=4,columnspan=5,padx=195,ipadx=10,sticky=NW)
   
    Label(affiche_saisie, text="Prix Achat", font=("calibri",11),bg=ligne[10],fg=color03).grid(row=1,column=5,padx=0,sticky=NW)
    label_uht=Entry(affiche_saisie,font=("Arial",11),width=9,textvariable=uht)
    label_uht.grid(row=1,column=6,padx=0,ipadx=10,sticky=NW)
    uht.trace("w", calcul_Marge)

    Label(affiche_saisie, text="Prix Vente", font=("calibri",11),bg=ligne[10],fg=color03).grid(row=2,column=5,padx=0,pady=1,sticky=NW)
    label_tht=Entry(affiche_saisie,font=("Arial",11),width=9,textvariable=tht)
    label_tht.grid(row=2,column=6,padx=0,ipadx=10,pady=1,sticky=NW)
    tht.trace("w", calcul_Marge)
    
    Label(affiche_saisie, text="Marge", font=("calibri",11),bg=ligne[10],fg=color03).grid(row=2,column=5,padx=0,ipadx=1,pady=30,sticky='sw')
    marge=Label(affiche_saisie,bg='#F6F6D0',width=10,fg="black",relief="groove",font=("calibri",11))
    marge.grid(row=2,column=6,padx=0,ipadx=1,pady=30,sticky=NW) 

    color02='#D3D7CF'
    photo = PhotoImage(file=chem_icon+'haut.png')
    photo = photo.subsample(7,7)
    move_up_button = Button(affiche_saisie,height=50,image = photo,command= up,bg='#FFFFFF')
    move_up_button.image = photo
    move_up_button.grid(row=2,column=0,padx=5,ipadx=5,pady=10,sticky=SW)
    
    photo = PhotoImage(file=chem_icon+'bas.png')
    photo = photo.subsample(7,7)
    move_down_button = Button(affiche_saisie,height=50,image = photo,command= down,bg='#FFFFFF')
    move_down_button.image = photo
    move_down_button.grid(row=2,column=0,columnspan=2,padx=46,ipadx=5,pady=10,sticky=SW)
    
    photo = PhotoImage(file=chem_icon+'calc.png')
    photo = photo.subsample(5,5)
    pdf = Button(affiche_saisie,height=50,image = photo,command=write_to_ods,bg='#FFFFFF')
    pdf.image = photo
    pdf.grid(row=2,column=0,columnspan=3,padx=88,ipadx=0,pady=10,ipady=0,sticky=SW)

    photo = PhotoImage(file=chem_icon+'oeil.png')
    photo = photo.subsample(5,5)
    pdf = Button(affiche_saisie,text='Tableur',height=50,width=51,image = photo,command=Reset,bg='#FFFFFF')
    pdf.image = photo
    pdf.grid(row=2,column=1,columnspan=4,padx=42,ipadx=0,pady=10,ipady=0,sticky=SW)
    
    photo = PhotoImage(file=chem_icon+'poubelle.png')
    photo = photo.subsample(4,4)
    pdf = Button(affiche_saisie,height=50,image = photo,command=Delete,bg='#FFFFFF')
    pdf.image = photo
    pdf.grid(row=2,column=2,columnspan=4,padx=10,ipadx=0,pady=10,ipady=0,sticky=SW)
          
    photo = PhotoImage(file=chem_icon+'Save.png')
    photo = photo.subsample(4,4)
    pdf = Button(affiche_saisie,height=50,image = photo,command=register,bg='#FFFFFF')
    pdf.image = photo
    pdf.grid(row=2,column=2,columnspan=6,padx=75,pady=9,sticky=SW)
    try:
        photo = PhotoImage(file=chem_icon+'Mise_jour.png')
        photo = photo.subsample(4,4)
        pdf = Button(affiche_saisie,height=50,image = photo,command=Update,bg='#FFFFFF')
        pdf.image = photo
        pdf.grid(row=2,column=3,columnspan=6,padx=40,pady=9,sticky='sw')
    except:
        Mymessagebox()
        Travaux(fen2)
    
    def sortie_3(sett):
        def func():
            fen_stock.destroy()
            sett()
        return func    
    photo = PhotoImage(file=chem_icon+'aide.png')
    photo = photo.subsample(4,4)
    Ba = Button(affiche_saisie,height=50,width=50,image = photo,command=aide_stock)
    Ba.image = photo
    Ba.grid(row=2,column=3,columnspan=6,padx=98,pady=9,sticky=SW)

    photo = PhotoImage(file=chem_icon+'sortie.png')
    photo = photo.subsample(5,5)
    Ba = Button(affiche_saisie,height=50,width=50,image = photo,command=sortie_3(sett))
    Ba.image = photo
    Ba.grid(row=2,column=4,columnspan=6,padx=54,pady=9,sticky='sw') 
@print_acount
def main(sett,fenètrePrimère = None):
    global tree
    global SEARCH
    global RECHERCHER,REF,AFFICHE_FNAME,AFFICHE_FAMILLE,AFFICHE_MARQUE,FAMILLE,MARQUE,RESULTAT,AFFICHE_UHT,AFFICHE_THT
    global fname_label,nom_label,nom,stock1,achat2,vente3,marge_B,marge_brut
    global ref,ddate,qte,mis,marque,desig,fname,famille,uht,tht,M_brut,marge,label_info
    global choix_imprim,bth_date,style_combo
    global hat,Menu_saisie,fname,block_01
    global qte_Combobox,fen_stock,Affiche_donnee,fen2,block_saisie,affiche_saisie,Mymessagebox,label_stock1,width,label_Vente

    fen_stock=tkinter.Toplevel(fenètrePrimère)
    try:
        fen_stock.configure(bg=ligne[12])
    except :
        Mymessagebox()
        Mywarning(fen2)
        sortie(fen2)
        Label(fen2, text="Couleur blanc par defaut\nVoire configuration",font=('verdana', 11)).place(x=50,y=10,width=180)  
        fen_stock.configure()
        ligne[12]='#FFFFFF'
    try:
        x = fenètrePrimère.winfo_x()
        y = fenètrePrimère.winfo_y()
        width = int(ligne[13].split('x',1)[0]) 
        height = int(ligne[13].split('x',1)[1].split('+',1)[0])
        dx = ligne[20] #80
        dy = ligne[21] #
        #print (dx,'<<<<<<<<<<<<<')
        fen_stock.geometry("%dx%d+%d+%d" % (width, height, x + int(str(dx)), y + int(str(dy))))   
        fen_stock.config(relief = RAISED,bd=1 )
        #fen_stock.bind('<Escape>',lambda event:sortie_3(sett))
        #fen_stock.minsize(width=948,height=500)
        #fen_stock.maxsize(w=1200,h=700)
    except:
        Mymessagebox()
        Mywarning(fen2)
        Label(fen2, text="Dimention par defaut\nVoire configuration",font=('verdana', 11)).place(x=50,y=10,width=180)
        sortie(fen2)
        fen_stock.geometry('949x398')
        ligne[13] = '949x398'
        width=1200
        height=400   
    #fen_stock.protocol('WM_DELETE_WINDOW', lambda: None)
    fen_stock.transient(fenètrePrimère)
    fen_stock.wm_attributes('-type','splash')
    fen_stock.minsize(width=500,height=300)
    fen_stock.maxsize(width=1100,height=700)
    SEARCH          = StringVar()
    RECHERCHER      = StringVar()
    REF             = StringVar()
    DDATE           = StringVar()
    AFFICHE_FNAME   = StringVar()
    AFFICHE_FAMILLE = StringVar()
    AFFICHE_MARQUE  = StringVar()
    AFFICHE_UHT     = StringVar()
    AFFICHE_THT     = StringVar()
    RESULTAT        = StringVar()

    ref      = StringVar()
    ddate    = StringVar()
    desig    = StringVar()
    qte      = StringVar()
    mis      = StringVar()
    marque   = StringVar()
    fname    = StringVar()
    famille  = StringVar()
    uht      = StringVar()
    tht      = StringVar()
    #marge    = StringVar()
    M_brut   = StringVar()

    bordure_left = 1
    haut_screen = 0
    
    helv36 = tkFont.Font(family="Times New Roman", size=16,weight="bold")
    label_info = LabelFrame(fen_stock,text='Gestion STOCK',bd=1,relief=RAISED,font=helv36,bg=ligne[10],fg='green')
    label_info.pack(side=TOP,fill=X)

    affiche_saisie = LabelFrame(fen_stock,bd=1,relief=RAISED,font=helv36,bg=ligne[10])
    affiche_saisie.pack(side=BOTTOM)

    affiche_sqlite = Frame(fen_stock,bg=ligne[10])
    affiche_sqlite.pack(side=TOP,anchor=E)

    #affiche_saisie.rowconfigure(0, weight=1)
    affiche_saisie.columnconfigure(0, weight=1)
     
    # Total Articles 
    stock1 = Label(label_info,relief=RAISED,anchor=CENTER,font=("Arial", 12))
    stock1.pack(side=LEFT,padx=2)
    # Total Achat 
    achat2 = Label(label_info,relief=RAISED,anchor=CENTER,font=("Arial", 12))
    achat2.pack(side=LEFT,padx=2)
    # Total Vente
    vente3 = Label(label_info,relief=RAISED,anchor=CENTER,font=("Arial", 12))
    vente3.pack(side=LEFT,padx=2)
    # ¢alcul marge
    marge_brut = Label(label_info,relief=RAISED,anchor=CENTER,font=("Arial", 12))
    marge_brut.pack(side=LEFT,padx=2)

    color02='#BFBFBF'
    #bar barre de défilement
    scrollbarx = Scrollbar(affiche_sqlite, orient=HORIZONTAL)
    scrollbary = Scrollbar(affiche_sqlite, orient=VERTICAL)
    tree = ttk.Treeview(affiche_sqlite,columns=("Student N°",'Ref','Date',"Designation","Quantite",'Unité','Marque',"Fname","Famille",'Prix achat','Prix vente'),#'Marge'), 
                                selectmode="extended", height=100, yscrollcommand=scrollbary.set, xscrollcommand=scrollbarx.set) 
    scrollbary.config(command=tree.yview)
    scrollbary.pack(side=RIGHT, fill=Y)
    scrollbarx.config(command=tree.xview)
    scrollbarx.pack(side=BOTTOM, fill=X)

    #  Définition des en-têtes des colonnes
    style = ttk.Style(fen_stock)
    style.theme_use('default')
    style.configure("Treeview.Heading",
        background="#D3D3D3",
        foreground="blue",
        rowheight=25,
        relief= RAISED,
        bd=2,
        font=('calibri', 10),                     # police bande
        fieldbackground="#D3D3D3")
    style.configure(".", font=('Helvetica', 10))  # police Treeview 
    tree.heading('Student N°'  ,text= "N°"        )
    tree.heading('Ref'         ,text='Ref'          ,command =lambda: Edition_ref())
    tree.heading('Date'        ,text='Date'      )# ,command =lambda: Edition_date()       ,anchor=W)
    tree.heading('Designation' ,text="Designation")
    tree.heading('Quantite'    ,text="Quantite"   )
    tree.heading('Unité'       ,text="Unité"   )
    tree.heading('Marque'      ,text="Marque"      ,command =lambda: Edition_marque())
    tree.heading('Fname'       ,text=" Fourniseur" ,command =lambda: Edition_fname())
    tree.heading('Famille'     ,text="Famille"     ,command =lambda: Edition_famille())
    tree.heading('Prix achat'  ,text='Prix achat')
    tree.heading('Prix vente'  ,text='Prix vente'        )
    
    # Réglage de la largeur des colonnes
    tree.column('#0',  stretch=NO, minwidth=0, width=0)
    tree.column('#1',  stretch=NO, minwidth=0, width=0)
    tree.column('#2',  stretch=NO, minwidth=10,width=110)
    tree.column('#3',  stretch=NO, minwidth=0, width=75)    # Ref
    tree.column('#4',  stretch=NO, minwidth=0, width=305)    # Designation
    tree.column('#5',  stretch=NO, minwidth=0, width=80)     # quantité
    tree.column('#6',  stretch=NO, minwidth=0, width=50)     # unité
    tree.column('#7',  stretch=NO, minwidth=0, width=140)
    tree.column('#8',  stretch=NO, minwidth=0, width=150)    # Fourniseur
    tree.column('#9',  stretch=NO, minwidth=0, width=90)     # Famille
    tree.column('#10', stretch=NO, minwidth=0, width=100)    # 
    tree.column('#11', stretch=NO, minwidth=0, width=100)    # prix tht
    tree['show'] = 'headings'
    tree.pack()
    try:
        DisplayData()
    except:
        pass
    Menu_saisie(sett)
    Couleur()
    fen_stock.mainloop()       
if __name__=='__main__':
    main()