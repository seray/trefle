#!/usr/bin/python3
# coding:utf-8
from tkinter import *
import tkinter as tk
import tkinter
from tkinter import ttk
import calendar
import datetime
import locale
import time
import pytz
from PIL import Image 
import os
from tkinter.font import Font
from Config  import *
from lib.libAuxilaire import *
 
SAFE_FILE_CALANDAR = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Plannig/Planning_Kiwi_AE.dat'

fiche_heure = locale.setlocale(locale.LC_ALL, 'fr_FR.utf8')
Date_semaine = time.strftime(" %W") 
Date_jour = time.strftime("%A %d %B")

annee = time.strftime("%Y")
mois = time.strftime("%m")
jour = time.strftime("%d")
from datetime import date, timedelta 
class PassPlusApp:
	def __init__(self, master):
		global Date_jour,Date_semaine,date,Date_Moi
		self.open_content()
		self.master = master
		self.frame_header = ttk.Frame(master)
		self.frame_header.pack()
		self.day = None
		self.content_semaine = []
		self.frame_pass = ttk.Frame(master)
		self.frame_pass.pack()	
		self.chaine = ['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche']
		self.key_list = ['7.00 Heures','7.50 Heures','8.00 Heures','8.50 Heures','9.00 Heures','9.50 Heures','10.00 Heures','10.50 Heures','11.00 Heures','11.50 Heures','12.00 Heures']
		self.key_heures = ['14.00 Heures','14.50 Heures','15.00 Heures','15.50 Heures','16.00 Heures','16.50 Heures','17.00 Heures','17.50 Heures','18.00 Heures','18.50 Heures','19.00 Heures','19.50 Heures','20.00 Heures']
		self.list_label = []
		
		self.ft = ("Times New Roman", 11, "bold")
		self.ftt = ("Times New Roman", 15, "bold")
		h = []
		self.heig  =11
		self.heigth  =13
		wid = 7
		delta=0
		self.minus_btn = Button(fen2_inner, text='<',font=self.ftt,padx=5, command = self.index_moin)
		self.minus_btn.grid(row=0, column=0,sticky=tk.W)# Bouton incrémente mois  -1
		
		self.plus_btn = Button(fen2_inner, text='>',font=self.ftt,padx=5, command=self.index_plus)
		self.plus_btn.grid(row=0, column=0,pady=5 )# Bouton incrémente mois +1
		
		self.entries = []#['Hrs8','Hrs825','Hrs85','Hrs875','Hrs9','Hrs925','Hrs95','Hrs975','Hrs10','Hrs1025','Hrs105','Hrs1075','Hrs12']
		self.annee = time.strftime("%Y")
		self.Date_semaine = int(Date_semaine) + 1
		self.semaine =0
		self.n = int(self.Date_semaine)+self.semaine
		self.annee = int(self.annee)   
		self.d=date(self.annee, 1, 1)
		self.unjour=timedelta(days=1)
		bigjump=timedelta(days=(self.n-2)*7)
		self.d+=bigjump #pour éviter une longue boucle
		iso=self.d.isocalendar()
		j = 0
		while iso[1]!=self.n:
			self.d+=self.unjour
			iso=self.d.isocalendar()
		while iso[1]==self.n:
			print (self.d)
			self.jour_semaine = tk.Label(fen2_inner, text=str(self.chaine[j]) +"  " + str(self.d),font=("Arial 13"),fg = "black")#,bg='#FFFFED')
			self.jour_semaine.grid(row=0, column=2+j)
			self.list_label.append(self.jour_semaine)
			self.d+=self.unjour
			iso=self.d.isocalendar()
			j+=1
			
		self.action = []
		
		for i in range(self.heig): #Rows
			newrow = []
			for j in range(wid): #Columns
				self.Hrs_tableauM = Text(fen2_inner,bg=ligne[17],fg= '#048304',font=("Arial 13"),width=23,height=2)  # Hauteur de la ligne
				self.Hrs_tableauM.grid(row=3+i, column=2+j,sticky=tk.N+tk.E)
				self.key_listD = Label(fen2_inner, text=self.key_list[i],font=self.ft)
				self.key_listD.grid(row=3+i, column=0)
				self.action.append(self.Hrs_tableauM)
		
		#l1 = Label(fen2_inner, text = "") 	l1.grid(row = 19, column = 3)
		for j in range(wid): #Columns
			self.Hrs = Label(fen2_inner, text=self.chaine[j],font=("Arial 14"))
			self.Hrs.grid(row=19, column=2+j,sticky=tk.N,pady=1)
		
		for l in range(self.heigth): #Rows
			newrow = []
			for k in range(wid): #Columns
				self.Hrs_tableauA = Text(fen2_inner,bg=ligne[17],fg= '#048304',font=("Arial 13"),width=23,height=2)  # Hauteur de la ligne
				self.Hrs_tableauA.grid(row=21+l ,column=2+k,sticky=tk.N+tk.E)
				self.Hrsm = Label(fen2_inner, text=self.key_heures[l],font=self.ft)
				self.Hrsm.grid(row=21+l, column=0,padx=5)
				self.action.append(self.Hrs_tableauA)
		l1 = Label(fen2_inner, text = "") 
		l1.grid(row = 34, column = 0,pady=5)
		Dict = {'entrs': 'Hrs', 1:[1, 2, 3, 4,5,6,7,8,9,10,11,12,13,14,15,16,17]} 		 
		self.index =0		
		self.charge_semaine()
	def index_moin (self):
		self.add_to_dict_semain(int(Date_semaine) + self.index)
		self.index -= 1
		self.charge_semaine()
	def index_plus (self):
		self.add_to_dict_semain(int(Date_semaine) + self.index)
		self.index += 1
		self.charge_semaine()
	def charge_semaine(self):
		#self.add_to_dict_semain()
		name_semain = self.day
		if name_semain in self.semain_content.keys():
			[entry.set(self.semain_content[name_semain][n_entry]) for n_entry in range(len(self.entries))]
		self.entries = []
		self.annee = time.strftime("%Y")
		self.Date_semaine = int(Date_semaine) + self.index
		self.semaine =1
		self.n = int(self.Date_semaine)+self.semaine
		self.annee = int(self.annee)   
		self.d=date(self.annee, 1, 1)
		self.unjour=timedelta(days=1)
		bigjump=timedelta(days=(self.n-2)*7)
		self.d+=bigjump #pour éviter une longue boucle
		iso=self.d.isocalendar()
		j = 0
		while iso[1]!=self.n:
			self.d+=self.unjour
			iso=self.d.isocalendar()
		for label in self.list_label:
			label.config(text =str(self.chaine[j]) +"  " + str(self.d))
			self.d+=self.unjour
			iso=self.d.isocalendar()
			j+=1
		
		if self.Date_semaine in self.semain_content:
			print('restor : ',self.semain_content[self.Date_semaine])
			t = 0
			for entry in self.action:
				entry.delete('@0,0','end')
				entry.insert('end',self.semain_content[self.Date_semaine][t])
				t += 1
		else :
			for entry in self.action:
				entry.delete('@0,0','end')
		
		l1 = Label(fen2_inner, text = "") 
		l1.grid(row = 34, column = 0,pady=5)	
		#
	def add_to_dict_semain(self,num_semain):
		self.semain_content[num_semain] = [entry.get('@0,0','end') for entry in self.action]
		self.semain_content[num_semain] = [item[:-1] for item in self.semain_content[num_semain]]
		self.save_content()
	def save_content(self):
		open(SAFE_FILE_CALANDAR,"w").write(self.compact())
	def open_content(self):
		self.semain_content = self.uncompact(open(SAFE_FILE_CALANDAR,"r").read())
		print(self.semain_content)
	#Compretion du fichier agenda
	def compact(self):
		dictionary = self.semain_content
		string_output = ""
		for key in dictionary:
			string_output += "\n$"
			string_output += str(key)
			i = 0
			while i < len(dictionary[key]):
				item = dictionary[key][i]
				t = 1
				cont = i+t < len(dictionary[key])
				while cont:
					cont = i+t < len(dictionary[key])
					if cont:
						cont = item == dictionary[key][i+t]
					t += 1
				i += t-1
				t -= 1
				if t == 1:
					string_output += "$!"
				else:
					string_output += "$" + str(t) + "!"
				string_output += item.replace("$","").replace("\n","¤")
		return string_output 

	#Decompretion du fichier agenda
	def uncompact(self,text):
		self.return_dictionary = {}
		for line in text.split("\n"):
			if len(line) != 0:
				if line[0] == "$":
					
					list_item = line.split("$")[1:]
					
					key = int(list_item[0])
					
					self.return_dictionary[key] = []
						
					for celle in list_item[1:]:
						part = celle.split("!")
						
						if part[0] == '':
							
							self.return_dictionary[key].append(part[1].replace("¤","\n"))
							
						else:
							numb_repet = int(part[0])
							for i in range(numb_repet):
								self.return_dictionary[key].append(part[1].replace("¤","\n"))
								
		print( self.return_dictionary)
		return self.return_dictionary
def aide_planning():
	try:
		FP.wm_attributes('-topmost',False)
		os.system('xdg-open /home/'+Nnom+'/Gestion_Kiwi_AE/Parametres/Aide_Kiwi_AE/Planning.pdf')
	except:
		messagebox.showerror("Erreure","Pas de fichier ",parent=FP)
def icon_exe(FP,sett):
	photo = PhotoImage(file='/opt/Kiwi_AE/Icon/aide.png')
	photo = photo.subsample(5,5)
	bout3 = Button(FP,height=43, width=43, image = photo,command=  aide_planning)
	bout3.image = photo
	bout3.place(x=155, y=1)
	def sortie3():
		sett()
		FP.destroy()
	photo = PhotoImage(file='/opt/Kiwi_AE/Icon/sortie.png')
	photo = photo.subsample(5,5)
	bout4 = Button(FP,height=43, width=43, image = photo,command= sortie3)
	bout4.image = photo
	bout4.place(x=205,y=1)
def resize_image(event): 
	new_width = event.width 
	new_height = event.height 
	image = copy_of_image.resize((new_width, new_height)) 
	photo = ImageTk.PhotoImage(image) 
	label.config(image = photo) 
	label.image = photo #avoid garbage collection 
def Mymessagebox():
	global fen2
	fen2 = tkinter.Toplevel()
	fen2.geometry("300x140+10+10")
	fen2.title("Liste")
	Mywarning(fen2)
	sortie(fen2)
	Label(fen2, text=" Couleur blanc ou \ndimention par defaut\n ou dimention\n Voire configuration",font=('verdana', 11)).place(x=50,y=10,width=180)  
		
def main(sett,fenètrePrimère = None):
	global FP,fen2,line,select,fen2_inner
	FP = tk.Toplevel()# Fenêtre mère
	try:
		FP.configure(bg=ligne[17])
	except :
		Mymessagebox()
		FP.configure(bg='#FFFFFF')
		ligne[17]='#FFFFFF'
	try:
		#FP.geometry(ligne[16])
		x = fenètrePrimère.winfo_x()
		y = fenètrePrimère.winfo_y()
		w = int(ligne[16].split('x',1)[0]) 
		h = int(ligne[16].split('x',1)[1].split('+',1)[0])
		dx = ligne[20]
		dy = int(ligne[21])
		FP.geometry("%dx%d+%d+%d" % (w, h, x + int(str(dx)), y + int(str(dy))))

	
		FP.minsize(width=1000, height=500)
		FP.maxsize(width=1260,height=730)
	except:
		Mymessagebox()
		w=800
		haut=200
		FP.geometry('790x650')
		ligne[16] = '790x450' 
	#FP.protocol('WM_DELETE_WINDOW', lambda: None)
	#FP.attributes('-topmost', True)
	FP.transient(fenètrePrimère)
	FP.wm_attributes('-type','splash')
	haut = int(ligne[16].split('x',1)[1].split('+',1)[0])
	#haut = haut -50
	frameq = Frame(FP,relief=SUNKEN, bd = 1)#,bg ="White")#,bd = 2)
	frameq.place(y=50,width=w, height=haut - 50) #x=5, y=70,width=890, height=730)
	frame = DoubleScrolledFrame(frameq, 
			width=300, 
			borderwidth=1, 
			relief=tk.SUNKEN, 
			background="light gray")
	frame.pack(fill=tk.BOTH, expand=True) # fill window
	fen2_inner = frame.inner
	fen2_inner.bind('<Escape>',  lambda event:fen2_inner.destroy())
	
	icon_exe(FP,sett)
	passPlusApp = PassPlusApp(FP)
	FP.mainloop()
if __name__ == '__main__':
	main(Tk())
