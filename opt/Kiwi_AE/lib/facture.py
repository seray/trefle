#coding:utf-8
from math import *
from tkinter import messagebox
import lib.lireFacture
import lib.class_nline as ClnL
from tkinter import *
import lib.NumeroClass

def adapte(nobre_d_esspace_a_vide,variable):
	variable = racourcire(variable,nobre_d_esspace_a_vide)
	lenVar = len(str(variable))
	nonbre_d_espace = nobre_d_esspace_a_vide - lenVar
	if nonbre_d_espace <= 0:
		return ''
	return nonbre_d_espace * ' '		
def positif(val):
	if val < 0:
		val = val * -1
	return val
def up_ligne(ligne):
	global colone,createFileBu,frame
	def func():
		global colone,createFileBu,frame
		if ligne != 0:
			colone[ligne],colone[ligne-1] = colone[ligne-1],colone[ligne]
			t = 0
			for ligne_saisi in colone:
				ligne_saisi.replacer(t)
				t += 1
				createFileBu.pack(pady=0+(len(colone)-1)*27)
	return func
def down_ligne(ligne):
	global colone ,createFileBu,frame
	def func():
		if ligne != len(colone)-1:
			colone[ligne],colone[ligne+1] = colone[ligne+1],colone[ligne]
			t = 0
			for ligne_saisi in colone:
				ligne_saisi.replacer(t)
				t += 1
				createFileBu.pack(pady=0+(len(colone)-1)*27)
	return func			
def copie_ligne(ligne):
	global colone,createFileBu,frame,copier
	def func():
		global colone,createFileBu,frame,copier,up_ligne,down_ligne,copie_ligne
		if ligne == len(colone)-1:
			colone.append(None)
		tmp = colone[ligne+1]
		colone[ligne+1] = ClnL.nligne((len(colone)),frame,supre_colone_select,up_ligne,down_ligne,copie_ligne)
		colone[ligne+1].sette(*colone[ligne].give_element())
		for l in range(ligne+2,len(colone)):
			colone[l],tmp = tmp,colone[l]
		if tmp != None:
			colone.append(tmp)
		t = 0
		for ligne_saisi in colone:
			ligne_saisi.replacer(t)
			t += 1
			createFileBu.pack(pady=0+(len(colone)-1)*27)
	return func	
def racourcire(ref,l):
	return ref[:l]
def main(fileName,affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date, colone,acompte,netht,entier,select):
	try:
		data = open(fileName,"w")
		# ======== Tierh
		affai       = affai.get()       
		nom         = nom.get()     
		rue         = rue.get()
		rue2        = rue2.get()
		codepos     = codepos.get()
		ville       = ville.get()
		objet       = objet.get()
		objet_suite = objet_suite.get()
		date        = date.get()
		acompte     = acompte.get()
		netht       = netht.cget('text') 
		
		data.write("#zone_a_imprimer#\n")	
		data.write(racourcire (affai,5))  
		data.write(adapte(5,affai))
		data.write(racourcire ( nom,37)+ adapte(36,nom )+"\n")
		data.write(racourcire (objet,54)+"\n")
		data.write(adapte(5,objet)+"\n")
		data.write(racourcire (rue,37)+ adapte(36,rue )+"\n")
		data.write(racourcire (objet_suite,54)+"\n")
		data.write(adapte(5,objet_suite)+"\n")
		data.write(racourcire (rue2,37)+ adapte(36,rue2 )+"\n")
		data.write(6 * "" + racourcire(codepos,36)+"\n")
		data.write(adapte (36,codepos)+"\n")
		data.write(6* ""+racourcire(ville,63)+"\n") 
		data.write(adapte(36,ville))
		data.write(str(date)+"\n")	
		for x in colone:
			
			ref   = x.ref.get()
			desig = x.desig.get()
			qte   = x.qte.get()
			unite = x.unite.get()
			remi  = x.remi.get()
			uht   = x.uht.get()
			tht   = x.tht.get()
			
			data.write(racourcire(ref,10))
			data.write(adapte(10,ref)  + " " + racourcire(desig,64))
			data.write(adapte(64,desig)+ " " + racourcire(remi,3))
			data.write(adapte(3,remi)  + " " + racourcire(qte,3))  
			data.write(adapte(3,qte)   + " " + racourcire(unite,3))
			data.write(adapte(3,unite) + " " + racourcire(uht,7))
			data.write(adapte(7,uht)   + " " + racourcire(tht,9))
			data.write(adapte(9,tht)   +"\n")
			
		data.write(" Sous total :" + racourcire(tht,9)     + "\n")	
		data.write(" Acompte  :"   + racourcire(acompte,9) + "\n")
		data.write(" Total HT :"   + racourcire(str(netht),9)   + "\n")
		data.write("#end#\n")
		data.write("#zone_de_donner#\n")
		
		data.write("affai~'"       + affai       + "'~\n")
		data.write("nom~'"         + nom         + "'~\n")
		data.write("rue~'"         + rue         + "'~\n")
		data.write("rue2~'"        + rue2        + "'~\n")
		data.write("codepos~'"     + codepos     + "'~\n")
		data.write("ville~'"       + ville       + "'~\n")
		data.write("objet~'"       + objet       + "'~\n")
		data.write("objet_suite~'" + objet_suite + "'~\n")
		data.write("ref~'"         + entier      + "'~\n") 
		data.write("date~'"        + date        + "'~\n")     
		data.write("tht~'"         + tht         + "'~\n")
		data.write("acompte~'"     + acompte     + "'~\n")
		data.write("netht~'"       + str(netht)  + "'~\n")
		
		data.write("colone~[")
		for x in colone:
			ref   = x.ref  .get()
			desig = x.desig.get()
			qte   = x.qte  .get()
			unite = x.unite.get()
			remi  = x.remi .get()
			uht   = x.uht  .get()
			tht   = x.tht  .get()
			
			data.write('{')
			data.write('"ref":"'   + str(ref)   + '",')
			data.write('"desig":"' + str(desig) + '",')
			data.write('"qte":"'   + str(qte)   + '",')
			data.write('"remi":"'  + str(remi)  + '",')
			data.write('"unite":"' + str(unite) + '",')
			data.write('"uht":"'   + str(uht)   + '",')
			data.write('"tht":"'   + str(tht)   + '"')
			data.write('},')
		data.write(']~\n')
		data.write("#end#\n")
		data = open(fileName,"r") # 
		print(data.read())
		data.close()
	except:
		pass
def openf(filename,affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date,acompte,netht, entier,colone, createFileBu,fen,supre_colone_select,frame,entier_label,var1_banck,select = "FAC"):
	try:
		text,a_apliqué = lib.lireFacture.lireDat(filename)
		
		affai.delete(0,END)
		affai.insert(0, (str(a_apliqué['affai'])))
		nom.delete(0,END)
		nom.insert(0, (str(a_apliqué['nom'])))
		rue.delete(0,END)
		rue.insert(0, (str(a_apliqué['rue'])))
		rue2.delete(0,END)
		rue2.insert(0, (str(a_apliqué['rue2'])))
		codepos.delete(0,END)
		codepos.insert(0, (str(a_apliqué['codepos'])))
		ville.delete(0,END)
		ville.insert(0, (str(a_apliqué['ville'])))
		objet.delete(0,END)
		objet.insert(0, (str(a_apliqué['objet'])))
		objet_suite.delete(0,END)
		objet_suite.insert(0, (str(a_apliqué['objet_suite'])))
		entier_label.config(text = entier)
		date.delete(0,END)
		date.insert(0, (str(a_apliqué['date'])))
		acompte.delete(0,END)
		acompte.insert(0, (str(a_apliqué['acompte'])))
		netht.config(text = '')#.delete(0,END)
		#netht.insert(0, (str(a_apliqué['netht'])))
			
		for ligne in range(len(colone)):
			colone[len(colone)-1].delette()
			del colone[len(colone)-1]
			createFileBu.pack()
		for x in range(len(a_apliqué['colone'])):
			colone.append(ClnL.nligne((len(colone)),frame,supre_colone_select,up_ligne, down_ligne, copie_ligne))
			colone[len(colone)-1].sette(
				a_apliqué['colone'][x]['ref'],
				a_apliqué['colone'][x]['desig'],
				a_apliqué['colone'][x]['remi'],
				a_apliqué['colone'][x]['qte'],
				a_apliqué['colone'][x]['unite'],
				a_apliqué['colone'][x]['uht'],
				a_apliqué['colone'][x]['tht']) 
			createFileBu.pack()
		return filename.split("/")[-1] 
	except:
		pass

