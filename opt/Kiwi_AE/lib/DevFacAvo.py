# coding: utf-8
#!/usr/bin/python3
from tkinter import *
import tkinter as tk
from tkinter import ttk
from tkinter.font import Font
from PIL import Image #, ImageTk 
from os import system
from lib.libAuxilaire import *
#import WidgetsFac as WidgetsFac_fen
#import WidgetsDev as WidgetsDev_fen
#import WidgetsAvo as WidgetsAvo_fen
import saveLoadTier
import Config

Nnom=os.getlogin()

	#DevFacAvo.affic
def bande(barre):
	bar = 'antiquewhite'
	ft = ("Times New Roman", 11)
	Label(barre,text="Référence",  bg =bar,fg = "black",font=ft).place(x=57 ,width=175)
	Label(barre,text="Désignation",bg =bar,fg = "black",font=ft).place(x=234,width=378)
	Label(barre,text="Unité"      ,bg =bar,fg = "black",font=ft).place(x=614,width=55)
	Label(barre,text="Qté"        ,bg =bar,fg = "black",font=ft).place(x=670,width=45)
	Label(barre,text="R"          ,bg =bar,fg = "black",font=ft).place(x=717,width=27)
	Label(barre,text="Prix"       ,bg =bar,fg = "black",font=ft).place(x=746,width=77) 
	Label(barre,text="Montant"    ,bg =bar,fg = "black",font=ft).place(x=825,width=81) 
	Label(barre,text="Action"     ,bg =bar,fg = "black",font=ft).place(x=908,width=105) 
def affiche_carte(fram4):
	def regarde():
		carte_pro.destroy()
		myfile = open(chem_note +'Entete', 'r')
		i=5
		le = myfile.readline()
		nomb = 0
		while nomb < 6:
			le = myfile.readline()
			label = tkinter.Label(fram4,text=le,bg='white',font=("Times",13),relief = RAISED,anchor=W).place(x=5,y=0+i,width=280)
			i+=20
			nomb = nomb +1 
	photo = PhotoImage(file=chem_note +'Carte.png')
	photo = photo.subsample(1,1)
	carte_pro = Button(fram4,height=147,width=294, image = photo,command= regarde)
	carte_pro.image = photo
	carte_pro.grid(row=0,column=0,pady=2,padx=2)
def labelfacdevavohaut(fen5):
	label = tkinter.    Label(fen5,text="Affaire :"		 ,fg="blue",font=("Times",15)).place(x=1,y=2) 
	label = tkinter.    Label(fen5,text="n°/Objet "		 ,fg="blue",font=("Times",15)).place(x=1,y=31) 
	label = tkinter.    Label(fen5,text="Suite/Ob "		 ,fg="blue",font=("Times",15)).place(x=1,y=60) 
	labeldate = tkinter.Label(fen5,text="Date "		     ,fg="blue",font=("Times",15)).place(x=1,y=93)   
	label = tkinter.    Label(fen5,text="Adresse "		 ,fg="blue",font=("Times",15)).place(x=300,y=31)
	label = tkinter.    Label(fen5,text="AD-Suite "      ,fg="blue",font=("Times",15)).place(x=300,y=60)   
	label = tkinter.    Label(fen5,text="Postal & Ville ",fg="blue",font=("Times",15)).place(x=300,y=93) 
	label = tkinter.    Label(fen5,text="Nom Prénon "    ,fg="blue",font=("Times",15)).place(x=300,y=2)    
if __name__ == '__main__':
	main(Tk())