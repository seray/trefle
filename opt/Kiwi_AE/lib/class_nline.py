# coding: utf-8$
import tkinter as tk
from tkinter import ttk
import sqlite3
from WidgetsFac import *
import lib.NumeroClass
import os

class nligne():
	def __init__(self,p,frame,supre_colone_select,up_ligne,down_ligne,copie_ligne):
		self.coucouJeSuisLaFrame = Frame(frame.inner)
		self.coucouJeSuisLaFrame.pack()
		self.supre_colone_select = supre_colone_select
		self.up_ligne = up_ligne
		self.down_ligne = down_ligne
		self.copie_ligne = copie_ligne
				
		listecredit = self.list_valid_date()
		validate = (frame.register(self._valiStock), '%P')    
		self.chosen_var = StringVar(self.coucouJeSuisLaFrame)
		ft = ("Times New Roman", 12)
		self.ref = ttk.Combobox(self.coucouJeSuisLaFrame, values = listecredit,textvariable = self.chosen_var,width=20,font=ft, validatecommand=validate,validate='key')
		self.ref.bind('<<ComboboxSelected>>', self.select_Stock)
		self.ref.grid(row=0,column=0,ipady=1,sticky=NW)
		
		desig = open(chem_note+'designation',"r").readlines()
		for t in range(len(desig)):
			desig[t] = desig[t][:len(desig[t])-1]
		self.desig = ttk.Combobox(self.coucouJeSuisLaFrame,width=45,values = desig,font=ft)
		self.desig.grid(row=0,column=1,ipady=1,ipadx=2,sticky=NW)
		
		unite = open(chem_note +'Conditionnement',"r").readlines()
		for t in range(len(unite)):
			unite[t] = unite[t][:len(unite[t])-1]
		self.unite = ttk.Combobox(self.coucouJeSuisLaFrame,width=5,values = unite,font=ft)
		self.unite.grid(row=0,column=2,ipady=1,sticky=NW)

		self.qte = tkinter.Entry(self.coucouJeSuisLaFrame,width=5,justify=CENTER,bg='White',font =ft)
		self.qte.grid(row=0,column=3,ipady=1,sticky=NW)
		
		self.remi = tkinter.Entry(self.coucouJeSuisLaFrame ,width=2,justify=CENTER,bg='White',font = ft)
		self.remi.grid(row=0,column=4,ipady=1,ipadx=3,sticky=NW)
		
		self.uht = tkinter.Entry(self.coucouJeSuisLaFrame ,width=9,justify=CENTER,bg='White',font =ft)
		self.uht.grid(row=0,column=5,ipady=1,sticky=NW)
		self.uht.insert(END,"0.00")

		self.tht = tkinter.Entry(self.coucouJeSuisLaFrame  ,width=9,justify=CENTER,bg='#E8F5E9',font=ft)
		self.tht.grid(row=0,column=6,ipady=1,ipadx=4,sticky=NW)
		self.tht.insert(END,"0.00")
		
		photo = PhotoImage(file=chem_icon + 'X.png')
		photo = photo.subsample(10,11)
		self.buton = Button(self.coucouJeSuisLaFrame,image = photo,command = supre_colone_select(p))
		self.buton.image = photo
		self.buton.grid(row=0,column=7,sticky=NW)

		photo = PhotoImage(file=chem_icon + 'haut.png')
		photo = photo.subsample(10,15)
		self.monter = Button(self.coucouJeSuisLaFrame,image = photo,command = up_ligne(p))
		self.monter.image = photo
		self.monter.grid(row=0,column=8,sticky=NW)
	
		photo = PhotoImage(file=chem_icon + 'bas.png')
		photo = photo.subsample(10,15)
		self.descendre = Button(self.coucouJeSuisLaFrame,image = photo,command = down_ligne(p))
		self.descendre.image = photo
		self.descendre.grid(row=0,column=9,sticky=NW)
		
		photo = PhotoImage(file=chem_icon + 'Dublique.png')
		photo = photo.subsample(10,12)
		self.Dub = Button(self.coucouJeSuisLaFrame, image = photo,command = copie_ligne(p))
		self.Dub.image = photo
		self.Dub.grid(row=0,column=10,sticky=NW)

		###self.texte = tkinter.Button(self.coucouJeSuisLaFrame,text='Texte',fg = "black",font= ("Helvetica", 10),bg="White",command = supre_colone_select(p))
		###self.texte.place(x=973, y=0, width=40, height=haut)
		
		self.IdicateurLigne = tkinter.Label(self.coucouJeSuisLaFrame,text = p + 1,bg ="White",fg = "red",font = ("Helvetica", 10))
		#self.IdicateurLigne.grid(row=0,column=0,ipady=0,ipadx=0,sticky=NW)		
	'''
	def select_Conditionnement(self):
		Famille_Marque = open(chem_note +'conditionnement',"r").readlines()
		comboExample = ttk.Combobox(self.coucouJeSuisLaFrame, values=Famille_Marque)
		pprint(dict(comboExample)) 
		comboExample.grid(column=0, row=1)
		comboExample.current(1)
	'''
	def list_valid_date(self):
		conn = sqlite3.connect(liste_stock)
		cursor = conn.cursor()
		cursor.execute("SELECT REFERENCE FROM REGISTRE")
		out = cursor.fetchall()
		conn.close()
		return out
	def ligne_data(self,ref):
		conn = sqlite3.connect(liste_stock)
		cursor = conn.cursor()
		cursor.execute("SELECT * FROM REGISTRE WHERE REFERENCE LIKE ?",(ref,))
		out = cursor.fetchall()
		conn.close()
		return out
	def _valiStock(self,s):
		nv = [ x for x in nligne.ref_Stock() if x.startswith(s) ]
		self.ref['values'] = nv
		return True
	def select_Stock(self,x):
		global conn, cursor,fetch
		conn = sqlite3.connect(liste_stock)
		cursor = conn.cursor()
		cursor.execute(
			"CREATE TABLE IF NOT EXISTS REGISTRE (RID INTEGER NOT NULL  PRIMARY KEY AUTOINCREMENT,REFERENCE TEXT,DESIG TEXT,QTE REAL,FNAME TEXT, FAMILLE TEXT,UTH REAL,THT REAL)")
		Stock = self.ligne_data(self.ref.get())[0]
		"""
		{"ref" : "Tube25",
		"Desig" : "PER 25",
		#"tarif_achat" : "20.00",
		"uht" :"10"
		"""
		### Affichage Tableau de saisie Facture devis avoir ####
		self.desig.delete(0,END)
		self.desig.insert(0, (str(Stock[3])))
		self.qte.delete(0,END)
		self.qte.insert(0, (str(Stock[4])))
		self.unite.delete(0,END)
		self.unite.insert(0, (str(Stock[5])))
		self.uht.delete(0,END)
		self.uht.insert(0, (str(Stock[9])))
	def calcul(self):
		remi = self.remi.get()
		if remi == '':
			remi = 100
		remi = 100 - (float(remi) % 100)
		if self.qte.get() != '' and self.uht.get() != '':
			thtf= float(self.qte.get()) * float(self.uht.get()) * (int(remi)/100)
			thtf = int(thtf * 100) / 100
			self.tht.delete(0,END)
			self.tht.insert(0, (str(thtf)))
			return thtf
		else:
			self.tht.delete(0,END)
			self.tht.insert(0, (str(0)))
			return 0		
	def delette(self):
		self.ref.destroy()
		self.desig.destroy()
		self.remi.destroy()
		self.qte.destroy()
		self.unite.destroy()
		self.uht.destroy()
		self.tht.destroy()
		self.buton.destroy()
		self.IdicateurLigne.destroy()
		self.coucouJeSuisLaFrame.destroy()	
	def replacer(self,p):
		self.coucouJeSuisLaFrame.forget()
		self.coucouJeSuisLaFrame.pack()
		self.IdicateurLigne.config(text=p)
		self.monter.config(command = self.up_ligne(p))
		self.descendre.config(command = self.down_ligne(p))
		self.Dub.config(command = self.copie_ligne(p))
		self.buton.config(command = self.supre_colone_select(p))
	def sette(self,ref,desig,remi,qte,unite,uht,tht):
		self.chosen_var.set(str(ref))
		self.desig.delete(0,END)
		self.desig.insert(0,(str(desig)))
		self.remi.delete(0,END)
		self.remi.insert(0,(str(remi)))
		self.qte.delete(0,END)
		self.qte.insert(0,(str(qte)))
		self.unite.delete(0,END)
		self.unite.insert(0,(str(unite)))
		self.uht.delete(0,END)
		self.uht.insert(0,(str(uht)))
		self.tht.delete(0,END)
		self.tht.insert(0,(str(tht)))
	def give_element(self):
		ref   = self.chosen_var.get()
		desig = self.desig.get()
		remi  = self.remi.get()
		qte   = self.qte.get()
		unite = self.unite.get()
		uht   = self.uht.get()
		tht   = self.tht.get()
		return ref,desig,remi,qte,unite,uht,tht	
class nligneClien():
	def __init__(self,p,frame,supre_colone_select,frame_inter):
		self.p = p
		sombre = "#000000"
		pastel ='#E2DCDC'
		self.coucouJeSuisLaFrame = Frame(frame.inner,width=frame_inter, height=25)
		self.coucouJeSuisLaFrame.pack()
		self.supre_colone_select = supre_colone_select
		decalage = 0
		self.nom = tkinter.Entry(self.coucouJeSuisLaFrame,fg = sombre ,bg=pastel,width=20,font = ("Helvetica", 11))
		self.nom.grid(row=0,column=3,ipadx=13,ipady=4,sticky=N) 
		self.rue = tkinter.Entry(self.coucouJeSuisLaFrame,fg = sombre ,bg=pastel,width=26,font = ("Helvetica", 11))
		self.rue.grid(row=0,column=4,ipadx=8,ipady=4,sticky=N)
		self.rue2 = tkinter.Entry(self.coucouJeSuisLaFrame,fg = sombre ,bg=pastel,width=25,font = ("Helvetica", 11))
		self.rue2.grid(row=0,column=5,ipadx=4,ipady=4,sticky=N)
		self.codePostal = tkinter.Entry(self.coucouJeSuisLaFrame,fg = sombre ,bg=pastel,width=4,font = ("Helvetica", 11))
		self.codePostal.grid(row=0,column=6,ipadx=5,ipady=4,sticky=N)
		self.ville = tkinter.Entry(self.coucouJeSuisLaFrame,fg =sombre,bg=pastel,width=20,font = ("Helvetica", 11))
		self.ville.grid(row=0,column=7,ipadx=5,ipady=4,sticky=N)
		self.mobil = tkinter.Entry(self.coucouJeSuisLaFrame,fg = sombre,bg=pastel,width=12,font = ("Helvetica", 11))
		self.mobil.grid(row=0,column=8,ipadx=5,ipady=4,sticky=N)
		self.fix = tkinter.Entry(self.coucouJeSuisLaFrame,fg = sombre,bg=pastel,width=12,font = ("Helvetica", 11))
		self.fix.grid(row=0,column=9,ipadx=5,ipady=4,sticky=N)
		self.email = tkinter.Entry(self.coucouJeSuisLaFrame,fg =sombre,bg=pastel,width=27,font = ("Helvetica", 11))
		self.email.grid(row=0,column=10,ipadx=7,ipady=4,sticky=N)
		self.fax = tkinter.Entry(self.coucouJeSuisLaFrame,fg =sombre,bg=pastel,width=12,font = ("Helvetica", 11))
		self.fax.grid(row=0,column=11,ipadx=5,ipady=4,sticky=N)
		self.buton = tkinter.Button(self.coucouJeSuisLaFrame,text='X',fg = "red",bg="White",font=("Times",10,'bold'),command = supre_colone_select(p))
		self.buton.grid(row=0,column=0,sticky=W)
		self.IdicateurLigne = tkinter.Label(self.coucouJeSuisLaFrame,text = p + 1,bg ="White",width=0,relief=RAISED,fg = "green",font =("Times",11,'bold'))
		self.IdicateurLigne.grid(row=0,column=1,ipadx=12,ipady=4,sticky=W)
	def normalize_format_mobile(self):
		val = self.mobil.get()
		val = val.replace(".","")
		val = val.replace("-","")
		val = val.replace(" ","")
		if val != "":
			if val[0] == "0":
				pass
			elif val[:3] == "+33":
				if val[3] == "0":
					val = val[3:]
				else:
					val = "0"+val[3:]
			else:
				val = "0"+val
			c = 2
			while c < len(val):
				val = val[:c] + " " + val[c:]
				c += 3
			self.mobil.delete(0,END)
			self.mobil.insert(0, (str(val)))
		val = self.fix.get()
		val = val.replace(".","")
		val = val.replace("-","")
		val = val.replace(" ","")
		if val != "":
			if val[0] == "0":
				pass
			elif val[:3] == "+33":
				if val[3] == "0":
					val = val[3:]
				else:
					val = "0"+val[3:]
			else:
				val = "0"+val
			c = 2
			while c < len(val):
				val = val[:c] + " " + val[c:]
				c += 3
			self.fix.delete(0,END)
			self.fix.insert(0, (str(val)))		
	def paste_to_dict(self,key = "nom"):
		return {self.__getattribute__( key).get() + str(self.p) : self}
	def delette(self):
		self.nom.destroy()
		self.rue.destroy()
		self.rue2.destroy()
		self.codePostal.destroy()
		self.ville.destroy()
		self.mobil.destroy()
		self.fix.destroy()
		self.email.destroy()
		self.fax.destroy()
		self.IdicateurLigne.destroy()
		self.coucouJeSuisLaFrame.destroy()
	def replacer(self,p):
		self.p = p
		#self.coucouJeSuisLaFrame= p - 1
		if p == 0:self.coucouJeSuisLaFrame.pack()
		else:self.coucouJeSuisLaFrame.pack()
		self.IdicateurLigne.config(text=p)
	def set(self,nom,rue,rue2,codePostal,ville,mobil,fix,email,fax):
		self.nom.delete(0,END)
		self.nom.insert(0, (str(nom)))
		self.rue.delete(0,END)
		self.rue.insert(0, (str(rue)))
		self.rue2.delete(0,END)
		self.rue2.insert(0, (str(rue2)))
		self.codePostal.delete(0,END)
		self.codePostal.insert(0, (str(codePostal)))
		self.ville.delete(0,END)
		self.ville.insert(0, (str(ville)))
		self.mobil.delete(0,END)
		self.mobil.insert(0, (str(mobil)))
		self.fix.delete(0,END)
		self.fix.insert(0, (str(fix)))
		self.email.delete(0,END)
		self.email.insert(0, (str(email)))
		self.fax.delete(0,END)
		self.fax.insert(0, (str(fax)))
