#coding:utf-8
from math import *
import lib.lireFacture
import lib.class_nline as ClnL
from tkinter import *

def adapte(nobre_d_esspace_a_vide,variable):
	variable = racourcire(variable,nobre_d_esspace_a_vide)
	lenVar = len(str(variable))
	nonbre_d_espace = nobre_d_esspace_a_vide - lenVar
	if nonbre_d_espace <= 0:
		return ''
	return nonbre_d_espace * ' '
		
def positif(val):
	if val < 0:
		val = val * -1
	return val
	
def racourcire(ref,l):
	return ref[:l]#ref[:positif(len(ref))-2-l] 
def main(fileName,affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date, entier_Int):
	data = open(fileName,"w")
	# ======== Tierh
	affai = affai.get()       
	nom =nom.get()     
	rue =rue.get()
	rue2 =rue2.get()
	codepos =codepos.get()
	ville =ville.get()
	objet = objet.get()
	objet_suite = objet_suite.get()
	date = date.get()
		
	data.write("#zone_a_imprimer#\n")	
		
	data.write("Affaire :" + racourcire (affai,52))  
	data.write(adapte(52,affai))
	data.write(racourcire (nom,37)+ adapte(36,nom ) +"\n")
	
	data.write("Objet :" + racourcire (objet,54))
	data.write(adapte(54,objet))
	data.write(racourcire (rue,37)+ adapte(36,rue ) +"\n")
	
	data.write(racourcire (objet_suite,54))
	data.write(adapte(54,objet_suite))
	data.write(racourcire (rue2,37)+ adapte(36,rue2 ) +"\n")
		
	data.write( racourcire(codepos,36))
	data.write(adapte (36,codepos) + "\n")
	
	data.write(racourcire(ville,63)) 
	data.write(adapte(36,ville)+"\n")
	data.write(str(date) + "\n")	
		# ============= Facture
	compteur = 0
	while compteur < 10:
					
		#data.write("│"+ racourcire('',10))
		data.write(adapte(10,'') + racourcire('',64))
		data.write(adapte(64,'') + racourcire('',3))
		data.write(adapte(3,'')  + racourcire('',3))  
		data.write(adapte(3,'') + racourcire('',7))
		data.write(adapte(7,'')  + racourcire('',9))
		data.write(adapte(9,'') + "\n")	
		compteur += 1	
		
	#  ===== fin total
	data.write("                                                                        Sous total :" + racourcire('',9) + "\n")	
	data.write("                                                                          Acompte  :" + racourcire('',9) + "\n")
	data.write("                                                                          Total HT :" + racourcire('',9) + "\n")
	
	data.write("#end#\n")
	data.write("#zone_de_donner#\n")
	
	data.write("affai~'" + affai + "'~\n")
	data.write("nom~'" + nom + "'~\n")
	data.write("rue~'" + rue + "'~\n")
	data.write("rue2~'" + rue2 + "'~\n")
	data.write("codepos~'" + codepos + "'~\n")
	data.write("ville~'" + ville + "'~\n")
	data.write("objet~'" + objet + "'~\n")
	data.write("objet_suite~'" + objet_suite + "'~\n")
	data.write("ref~'" + entier_Int + "'~\n") 
	data.write("date~'" + date + "'~\n")     ####################
	
	data = open(fileName,"r") # 
	print(data.read())
	data.close()
	