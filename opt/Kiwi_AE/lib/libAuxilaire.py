#!/usr/bin/python3
from tkinter import *
import tkinter as tk
import tkinter
from tkinter import ttk
import calendar
import time
import datetime
import locale
from PIL import Image 
import os 
import subprocess
from tkinter.font import Font
from tkcalendar import Calendar, DateEntry
from os import system
date = time.strftime("%d-%m-%y")
from tkinter.messagebox import * 
utilisateur = os.getlogin()
from Config  import *
from datetime import date,timedelta

utilisateur = os.getlogin()
Nnom=os.getlogin()

affiche_heure = locale.setlocale(locale.LC_ALL, 'fr_FR.utf8')
arial = "arial", 11
# Dossier Devis teste exit! et création
if not os.path.exists('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Devis/Devis_PDF/Devis_PDF_'                       +ligne[42]+'/'):
	os.makedirs(      '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Devis/Devis_PDF/Devis_PDF_'                       +ligne[42]+'/')
if not os.path.exists('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Devis/Devis_brut/Devis_brut_'                     +ligne[43]+'/'):
	os.makedirs(      '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Devis/Devis_brut/Devis_brut_'                     +ligne[43]+'/')
if not os.path.exists('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Facture/Facture_PDF/Facture_PDF_'                 +ligne[44]+'/'):
	os.makedirs(      '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Facture/Facture_PDF/Facture_PDF_'                 +ligne[44]+'/')
if not os.path.exists('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Facture/Facture_brut/Facture_brut_'               +ligne[45]+'/'):
	os.makedirs('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Facture/Facture_brut/Facture_brut_'                     +ligne[45]+'/')
if not os.path.exists('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Avoir/Avoir_PDF/Avoir_PDF_'                       +ligne[46]+'/'):
	os.makedirs('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Avoir/Avoir_PDF/Avoir_PDF_'                             +ligne[46]+'/')
if not os.path.exists('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Avoir/Avoir_brut/Avoir_brut_'                     +ligne[47]+'/'):
	os.makedirs('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Avoir/Avoir_brut/Avoir_brut_'                           +ligne[47]+'/')
if not os.path.exists('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/InterVention/InterVention_PDF/InterVention_PDF_'  +ligne[48]+'/'):
	os.makedirs(      '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/InterVention/InterVention_PDF/InterVention_PDF_'  +ligne[48]+'/')
if not os.path.exists('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/InterVention/InterVention_brut/InterVention_brut_'+ligne[49]+'/'):
	os.makedirs(      '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/InterVention/InterVention_brut/InterVention_brut_'+ligne[49]+'/')
if not os.path.exists('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Recette/Recette_PDF/Banque_PDF_'                  +ligne[50]+'/'):
	os.makedirs(      '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Recette/Recette_PDF/Banque_PDF_'                  +ligne[50]+'/')

if not os.path.exists('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Recette/Recette_db/'):
	os.makedirs(      '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Recette/Recette_db/')

if not os.path.exists('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Recette/Recette_ods/'):
	os.makedirs(      '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Recette/Recette_ods/')
# Dossier stock teste exit! et création
if not os.path.exists('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/stock/'):
	os.makedirs      ('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/stock/')
####################
chem_icon           = '/opt/Kiwi_AE/Icon/'
chem_para           = '/home/'+Nnom+'/Gestion_Kiwi_AE/Parametres/Bank/'
chem_parametre      = '/home/'+Nnom+'/Gestion_Kiwi_AE/Parametres/'
chem_note           = '/home/'+Nnom+'/Gestion_Kiwi_AE/Parametres/Note/'
chem_gestion        = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/'
chem_gestion_Kiwi_AE= '/home/'+Nnom+'/Gestion_Kiwi_AE/'
chem_aide           = '/home/'+Nnom+'/Gestion_Kiwi_AE/Parametres/Aide_Kiwi_AE/'
########### MessageBox #############
liste_devis         = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Devis/Devis_PDF/Devis_PDF_'                       +ligne[42]+'/' 
liste_devis_brut    = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Devis/Devis_brut/Devis_brut_'                     +ligne[43]+'/'
liste_facture       = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Facture/Facture_PDF/Facture_PDF_'                 +ligne[44]+'/'
liste_facture_brut  = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Facture/Facture_brut/Facture_brut_'               +ligne[45]+'/'
liste_avoir         = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Avoir/Avoir_PDF/Avoir_PDF_'                       +ligne[46]+'/'
liste_avoir_brut    = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Avoir/Avoir_brut/Avoir_brut_'                     +ligne[47]+'/'
liste_avoir_bt      = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Avoir/Avoir_brut_'
liste_inter_brut    = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/InterVention/InterVention_brut/InterVention_brut_'+ligne[48]+'/'

racine_Devis        = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Devis/Devis_PDF/'
racine_Devis_brut   = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Devis/Devis_brut/'
racine_Facture      = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Facture/Facture_PDF/'
racine_Facture_brut = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Facture/Facture_brut/'
racine_Avoir        = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Avoir/Avoir_PDF/'
racine_Avoir_brut   = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Avoir/Avoir_brut/'
racine_Inter        = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/InterVention/InterVention_PDF/'
racine_Inter_brut   = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/InterVention/InterVention_brut/'
racine_banck        = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Recette/Recette_PDF/'
racine_banck_ods    = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Recette/Recette_ods'
racine_recette		= '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Recette'
racine_tier         = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/tier/'
racine_stock        = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/stock'
racine_plann        = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Plannig/'

liste_tier          = '/home/'+Nnom+'/Gestion_Kiwi_AE/tier/listTier.dat'
liste_stock         = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/stock/Stock.db'
aide_Kiwi_AE        = '/home/'+Nnom+'/Gestion_Kiwi_AE/Parametres/Aide_Kiwi_AE/livre.pdf'
liste_inter         = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/InterVention/InterVention_PDF/InterVention_PDF_'+ligne[48]+'/'
liste_banck         = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Recette/Recette_PDF/Banque_PDF_'                +ligne[49]+'/'
dossier 			= '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Recette/Recette_db/livre_de_bank.db'
FILE_NAME		    = '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Agenda/Agenda_brut/Agenda.dat'
FILE_TIER 			= '/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/tier/listTier.dat'

def interro(fen2):     # Stock recette
	photo = PhotoImage(file=chem_icon + 'interrogation.png')
	photo = photo.subsample(4,4)
	Ba = Label(fen2,image = photo) 
	Ba.image = photo
	Ba.place(x=0,y=0,width=60,height=60)
def sortie(fen2):  # Stock recette
	sortie=Button(fen2, text="Sortie",font=('verdana',10),command=fen2.destroy)
	sortie.place(x=15,y=80)
'''
def Mywarning(fen2): # Stock recette
	#global fen2   
	photo = PhotoImage(file=chem_icon + 'warning.png')
	photo = photo.subsample(6,6)
	Ba = Label(fen2,image = photo)#,command=fen2.destroy)
	Ba.image = photo
	Ba.place(x=0,y=0,width=60,height=60)

def Trefle(fen2): # Stock recette
    photo = PhotoImage(file=chem_icon + 'Trefle.gif')
    photo = photo.subsample(6,6)
    Ba = Label(fen2,image = photo)#,command=fen2.destroy)
    Ba.image = photo
    Ba.place(x=0,y=0,width=60,height=60)
# Calendrier
def Travaux(fen2):
	photo = PhotoImage(file=chem_icon + 'warning.png')
	photo = photo.subsample(4,4)
	Ba = Label(fen2,image = photo)#,command=fen2.destroy)
	Ba.image = photo
	Ba.place(x=80,y=30,width=60,height=60)
	Label(fen2,text='Appuyer sur icon ').place(x=25,y=10)
	sortie=Button(fen2, text="Sortie",font=('verdana',10),command=fen2.destroy)
	sortie.place(x=5,y=80,width=60)
'''
def Wcal():
	annee = time.strftime("%Y")
	mois = time.strftime("%m")
	jour = time.strftime("%d")
	def exa():
		def print_sel():
			print(cal.selection_get())
		top = Toplevel()
		top.geometry("322x250+350+410")
		top.wm_attributes('-type', 'splash');
		es=Button (top,text = "Sortie",command=top.destroy)
		es.pack(padx=5,pady=5,side=BOTTOM,fill=X)
		cal = Calendar(top,
			font="Arial 14", selectmode='day',
						cursor="hand1", year=int(annee) , month=int(mois), day=int(jour))
		cal.pack(fill="both")#, expand=True)
		#top.bind('<Escape>',  lambda event:top.destroy())
	exa()
# Appelle menu_Trefle planning
def Planning():
	global cn,plann
	plann =tkinter.LabelFrame(text="Rappel",relief = RAISED,font=("Times", "13", "bold italic"),bg =ligne[0])#,fg='White')
	plann.place(relx = 0.4,rely = 0.0,anchor ='ne')
	f =eval(open('/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/Plannig/Plannig.dat',"r").read(),{},{})
	L = []
	W = time.strftime('%W')
	t=0
	for c in f:
		if f[c][11] == W:
			L.append(Label(plann,bg =ligne[0],fg='black',anchor='w',font=(arial, 12),text =' M. et Mr '+ f[c][7]+' '+f[c][0]))
			L[-1].pack(fill='both',expand=1)
			t += 1
def del_plann():
	plann.destroy()
# Appelle menu_Kiwi_AE rappel annuel
def anniversaire():
	global date_jour,cadrerappele
	#Date_jour = time.strftime("%A %d %B")
	date_ = int(time.strftime("%d"))
	cadrerappele = tkinter.LabelFrame(text="Rappel annuelle",relief = RAISED,font=("Times", "12", "bold italic"),bg =ligne[0],fg='black')
	cadrerappele.place(x=170,relx = 0.1,rely = 0.0,anchor ='ne')#pack(anchor='ne',pady=1,padx=5,ipadx=5,ipady=5) 
	a =eval(open("/home/"+Nnom+"/Gestion_Kiwi_AE/Gestion/Plannig/Anniversaire.dat","r").read(),{},{})		
	L = []
	jour = date(year = 2022,month = int(time.strftime('%m')), day = int(time.strftime('%d')))
	jourp10 = date(year = 2022,month = int(time.strftime('%m')), day = int(time.strftime('%d')))+timedelta(days = 12)
	t=0
	for date_jour in a:
		if len(a[date_jour][-1].split("-")) == 2:
			aniveDate = date(year = 2022,month = int(a[date_jour][-1].split("-")[1]), day = int(a[date_jour][-1].split("-")[0]))
			if jourp10 >= aniveDate and jour <= aniveDate:
				L.append(Label(cadrerappele,bg =ligne[0],fg='black',anchor='w',font=(arial, 12),text ='Date ' + a[date_jour][-1]+'\n M. et Mr '+ a[date_jour][1]))
				L[-1].pack(fill='both',expand=1)
				t += 1	
def del_anniversaire():
	cadrerappele.destroy()
########
def bien_venue(): 
	#fiche =os.system ('sudo' + ' /opt/Kiwi_AE/.Kiwi_AE')
	fiche = '/home/'+Nnom+'/Gestion_Kiwi_AE/Parametres/Aide_Kiwi_AE/bien_venue'
	def sauvegarder():
		open(fiche, "w").write(text.get(0., END))
	text = Text(font=('Arial 13'),bg ='#F2E78D',fg='black')
	text.insert(1.,open(fiche,'r').read())
	text.place(x=250, y=150,height=435,width=607)

	photo = PhotoImage(file=chem_icon + 'exit.png')
	photo = photo.subsample(1,1)
	Ba = Button(text, image = photo,command=text.destroy, compound=BOTTOM)
	Ba.image = photo
	Ba.pack(padx=5,pady=5,side='right',anchor='sw')
# apple dossier fac dev recette
def DossierFac_Pdf():
	global cadrefac,listfac,selected_item,ligne
	#block_Droite = LabelFrame(fg='black',background=ligne[0]).place(relx=0.8,rely=0.0,y=36,anchor='nw',relwidth=1.0,relheight=1)
	def updateFac(event=None):
		global listfac,selected_item,ligne
		"Callback quand sélection d'un élément de ListBox."
		line = listfac.curselection()[0]
		os.system('xdg-open '+liste_facture+ str(listfac.get(line)))
	selected_item = StringVar()
	files = os.listdir(liste_facture)
	cadrefac = tkinter.LabelFrame(text='Gestion/Facture/'+ligne[43],fg='blue',relief = RAISED,font=("Times","11","bold"),bg=ligne[0])#,bg=ligne[0])
	cadrefac.place(relx = 1.0,rely = 0.0,relwidth=0.2,y=40,anchor ='ne') 
	listfac = Listbox(cadrefac,font=(arial, 11),bg ='black',fg='White',height=6,width=25)
	for name in files:
		listfac.insert(1,name )
	listfac.pack( side = 'left',fill='both',expand=1)
	listfac.bind('<Return>', updateFac)
	listfac.bind('<KP_Enter>', updateFac)
	listfac.bind('<Double-1>', updateFac)
def del_cadrefac():
	cadrefac.destroy()
def DossierDev_Pdf():
	global cadredev,listdev,selected_item
	def updateDev(event=None):
		global cadredev,listdev,selected_item
		"Callback quand sélection d'un élément de ListBox."
		line = listdev.curselection()[0]
		os.system('xdg-open '+liste_devis+str(listdev.get(line)))
	selection_item = StringVar()
		
	files = os.listdir(liste_devis)
	cadredev = tkinter.LabelFrame(text='/Gestion/Devis/'+ligne[42],fg='blue',relief = RAISED,font=("Times","11","bold"),bg=ligne[0])
	cadredev.place(relx = 1.0,rely = 0.3,relwidth=0.2,y=30,anchor ='e')
	listdev = Listbox(cadredev,font=(arial, 11),bg ='black',fg='White',height=6,width=25)
	for name in files:
		listdev.insert(1,name )
	listdev.pack( side = 'left',fill='both',expand=1)
	listdev.bind('<Return>', updateDev)
	listdev.bind('<KP_Enter>',updateDev)
	listdev.bind('<Double-1>',updateDev)
def del_cadredev():
	cadredev.destroy()
def Recette_Pdf():
	global cadreRec,listRec,selected_item
	def updateDev(event=None):
		global Recette_Pdf,cadreRec,listRec,selected_item
		"Callback quand sélection d'un élément de ListBox."
		line = listRec.curselection()[0]
		os.system('xdg-open '+liste_banck+ str(listRec.get(line)))
	selection_item = StringVar()
	
	files = os.listdir(liste_banck)
	cadreRec = tkinter.LabelFrame(text='/Gestion/Recette/'+ligne[50],fg='blue',relief = RAISED,font=("Times","11","bold"),bg=ligne[0])
	cadreRec.place(relx = 1.0,rely = 0.7,y=20,relwidth=0.2,anchor ='e')
	listRec = Listbox(cadreRec,font=(arial, 11),bg ='black',fg='White',height=6,width=25)
	for name in files:
		listRec.insert(1,name )
	listRec.pack( side = 'left',fill='both',expand=1)	
	listRec.bind('<Return>', updateDev)
	listRec.bind('<KP_Enter>',updateDev)
	listRec.bind('<Double-1>',updateDev)
def del_cadreRec():
	cadreRec.destroy()
def del_avoir():
	cadreavo.destroy()
def DossierAvo():
	global cadreavo,listavo,selected_item
	def updateAvo(event=None):
		global cadredev,listavo,selected_item
		"Callback quand sélection d'un élément de ListBox."
		line = listavo.curselection()[0]
		os.system('xdg-open '+liste_avoir+  str(listdev.get(line)))
	selection_item = StringVar()
	files = os.listdir(liste_avoir)
	cadreavo = tkinter.LabelFrame(text='Gestion/Avoir/'+ligne[44],fg='blue',relief = RAISED,font=("Times", "11", "bold"),bg=ligne[0])
	cadreavo.place(relx = 1.0,rely = 0.5,relwidth=0.2,y=25,anchor ='e')#pack( anchor='ne',pady=1,padx=5,ipadx=10)
	listavo = Listbox(cadreavo,font=(arial, 11),bg ='black',fg='White',height=6,width=25)
	for name in files:
		listavo.insert(1,name )
	listavo.pack( side = 'left',fill='both',expand=1)
	listavo.bind('<Return>', updateAvo)
	listavo.bind('<KP_Enter>',updateAvo)
	listavo.bind('<Double-1>',updateAvo)
def modifi_pass():
	#fiche =os.system ('sudo' + ' /opt/Kiwi_AE/.Kiwi_AE')
	fiche = chem_note + 'Pass_Kiwi_AE'
	def sauvegarder():
		open(fiche, "w").write(text.get(0., END))
	text = Text(font=('Arial 13'),bg ='black',fg='White')
	text.insert(1.,open(fiche,'r').read())
	text.place(x=250, y=150,height=35,width=207)

	photo = PhotoImage(file=chem_icon + 'Ok.png')
	photo = photo.subsample(8,8)
	B = Button(text, image = photo,command = sauvegarder, compound=BOTTOM)
	B.image = photo
	B.place(x = 138, y =0,width=30, height=30)
	
	photo = PhotoImage(file=chem_icon + 'exit.png')
	photo = photo.subsample(1,1)
	Ba = Button(text, image = photo,command=text.destroy, compound=BOTTOM)
	Ba.image = photo
	Ba.place(x = 170, y =0,width=30, height=30)
# edition texte	
def del_propos():
	propos.destroy()	
def A_propos():
	global propos
	load = '/home/'+Nnom+'/Gestion_Kiwi_AE/Parametres/Aide_Kiwi_AE/propos_Kiwi_AE.png'
	photo = PhotoImage(file=load)
	photo = photo.subsample(1,1)
	propos = Button(image = photo,command= del_propos)
	propos.image = photo
	propos.place(relx = 0.5,rely = 0.5, anchor = 'center')
def del_A_proposs():
	proposs.destroy()		
def rapporter_un_bug():
	global propos
	load = '/home/'+Nnom+'/Gestion_Kiwi_AE/Parametres/Aide_Kiwi_AE/propos_Kiwi_AE.png'
	photo = PhotoImage(file=load)
	photo = photo.subsample(1,1)
	proposs = Button(image = photo,command= del_A_proposs)
	proposs.image = photo
	proposs.place(relx = 0.5,rely = 0.5, anchor = 'center')
def del_contact():
	_contact.destroy()
def contact_Kiwi_AE():
	global _contact
	load = '/home/'+Nnom+'/Gestion_Kiwi_AE/Parametres/Aide_Kiwi_AE/contact.png'
	photo = PhotoImage(file=load)
	photo = photo.subsample(1,1)
	_contact = Button(image = photo,command= del_contact)
	_contact.image = photo
	_contact.place(relx = 0.5,rely = 0.5, anchor = 'center')
def Entete():
	def sauvegarder():
		open(chem_note +'Entete', "w").write(text.get(0., END))
	text = Text(width=40, height=10)
	text.insert(1.,open(chem_note +'Entete','r').read())
	text.place(x=100, y=10)

	photo = PhotoImage(file=chem_icon + 'Ok.png')
	photo = photo.subsample(8,8)
	B = Button(text, image = photo,command = sauvegarder, compound=BOTTOM)
	B.image = photo
	B.place(x = 10, y =120,width=30, height=30)
	
	photo = PhotoImage(file=chem_icon + 'exit.png')
	photo = photo.subsample(1,1)
	Ba = Button(text, image = photo,command=text.destroy, compound=BOTTOM)
	Ba.image = photo
	Ba.place(x = 50, y =120,width=30, height=30)
def del_compta():
	pass
def pied_livre():
	def sauvegarder():
		open(chem_note +'Pied_page', "w").write(text.get(0., END))
	text = Text(width=147, height=6)
	text.insert(1.,open(chem_note +'Pied_page','r').read())
	text.place(x=100, y=355)

	photo = PhotoImage(file=chem_icon + 'Ok.png')
	photo = photo.subsample(8,8)
	B = Button(text, image = photo,command = sauvegarder, compound=BOTTOM)
	B.image = photo
	B.place(x = 10, y =60,width=30, height=30)
	
	photo = PhotoImage(file=chem_icon + 'exit.png')
	photo = photo.subsample(1,1)
	Ba = Button(text, image = photo,command=text.destroy, compound=BOTTOM)
	Ba.image = photo
	Ba.place(x = 50, y =60,width=30, height=30)
def devis_reserve():
	def sauvegarder():
		open(chem_note +'Sous_reserve', "w").write(text.get(0., END))
	
	text = Text(width=146, height=5)
	text.insert(1.,open(chem_note +'Sous_reserve','r').read())
	text.place(x=100, y=170)

	photo = PhotoImage(file=chem_icon + 'Ok.png')
	photo = photo.subsample(8,8)
	B = Button(text, image = photo,command = sauvegarder, compound=BOTTOM)
	B.image = photo
	B.place(x = 10, y =40,width=30, height=30)
	
	photo = PhotoImage(file=chem_icon + 'exit.png')
	photo = photo.subsample(1,1)
	Ba = Button(text, image = photo,command=text.destroy, compound=BOTTOM)
	Ba.image = photo
	Ba.place(x = 50, y =40,width=30, height=30)
def Pied():
	def sauvegarder():
		open(chem_note +'Condition_generale', "w").write(text.get(0., END))
	text = Text(width=147, height=6)
	text.insert(1.,open(chem_note +'Condition_generale','r').read())
	text.place(x=100, y=255)

	photo = PhotoImage(file=chem_icon + 'Ok.png')
	photo = photo.subsample(8,8)
	B = Button(text, image = photo,command = sauvegarder, compound=BOTTOM)
	B.image = photo
	B.place(x = 10, y =60,width=30, height=30)
	
	photo = PhotoImage(file=chem_icon + 'exit.png')
	photo = photo.subsample(1,1)
	Ba = Button(text, image = photo,command=text.destroy, compound=BOTTOM)
	Ba.image = photo
	Ba.place(x = 50, y =60,width=30, height=30)
# carte	
def carte_visite():
	global carte_pro
	photo = PhotoImage(file='/home/'+Nnom+'/'+ligne[49])
	photo = photo.subsample(1,1)
	carte_pro = Button(image = photo,command= del_carte_visite)
	carte_pro.image = photo
	carte_pro.place(relx = 0.5,rely = 0.5, anchor = 'center')
def del_carte_visite():
	carte_pro.destroy()
# Graphique Info
# Recette chiffre entier sans lettre
def OnValidate(S):
	if S.isdigit() or S == ".":
		return True
	return False
 
if __name__ == '__main__':
		main=Tk()
