# coding: utf-8
import tkinter
from tkinter import filedialog
import os,pwd 
os.getlogin = lambda: pwd.getpwuid(os.getuid())[0]
import os
import shutil
import subprocess
from Config  import *
from lib.libAuxilaire import *

def demanderLienRec(Open = False):
	filepdf = filedialog.askopenfilename(initialdir= racine_banck,title='Sélection Livre ',filetypes =[("Dat Files","**.pdf")])
	subprocess.Popen(["xdg-open", filepdf])
def recheche_facture():
	filepdf = filedialog.askopenfilename(initialdir= racine_Facture,title = "Selection une Facture",filetypes = (("pdf files","*.pdf"),("all files","*.*")))
	subprocess.Popen(["xdg-open", filepdf],shell=True)											
def recheche_devis():
	filepdf = filedialog.askopenfilename(initialdir= racine_Devis,title = "Selection un Devis",filetypes = (("pdf files","*.pdf"),("all files","*.*")))											
	subprocess.Popen(["xdg-open", filepdf])
def recheche_avoir():
	filepdf = filedialog.askopenfilename(initialdir= racine_Avoir,title = "Selection un Avoir",filetypes = (("pdf files","*.pdf"),("all files","*.*")))											
	subprocess.Popen(["xdg-open", filepdf])
def recheche_Inter():
	filepdf = filedialog.askopenfilename(initialdir= racine_Inter,title = "Selection un Avoir",filetypes = (("pdf files","*.pdf"),("all files","*.*")))											
	subprocess.Popen(["xdg-open", filepdf])
def recheche_pdf():
	filepdf = filedialog.askopenfilename(initialdir= chem_gestion_Kiwi_AE + '/Donnees_PDF',title = "Selection un Avoir",filetypes = (("pdf files","*.pdf"),("all files","*.*")))	
	subprocess.Popen(["xdg-open", filepdf])
#def bien_venue():
	#try:
	#	os.system('xdg-open '+ '/home/'+Nnom+'/Gestion_Trefle/Donnees/Parametres/Aide_Trefle/bienvenue.png')
	#except:
	#	messagebox.showinfo("Information","Message de bienvenue")

'''
def termil_sudo_passe():
	from Tkinter import *
	import os

	root = Tk.Toplevel()
	termf = Frame(root, height=400, width=500)

	termf.pack(fill=BOTH, expand=YES)
	wid = termf.winfo_id()
	os.system('xterm -into %d -geometry 40x20 -sb &' % wid)

	root.mainloop()
'''
##################################
### del remise à jour ############
##################################
def del_compta_devis():
	result = messagebox.askquestion("Purge Devis_"+ ligne[42] , "êtes-vous sur de vouloir supprimer ? \n Fermer comptabilité Kiwi_AE aprés validation  !", icon='warning')
	if result == 'yes':
		shutil.rmtree(racine_Devis      +  'Devis_PDF_' + ligne[42])#, ignore_errors=True)
		shutil.rmtree(racine_Devis_brut + 'Devis_brut_' + ligne[43])
	else:
		pass 
def del_compta_facture():
	result = messagebox.askquestion("Purge Facture_"+ ligne[44] , "êtes-vous sur de vouloir supprimer ? \n Fermer comptabilité Kiwi_AE aprés validation  !", icon='warning')
	if result == 'yes':
		shutil.rmtree(racine_Facture      +  'Facture_PDF_' + ligne[44])#, ignore_errors=True)
		shutil.rmtree(racine_Facture_brut + 'Facture_brut_' + ligne[45])
	else:
		pass 
def del_compta_avoir():
	result = messagebox.askquestion("Purge Avoir_"+ ligne[46] , "êtes-vous sur de vouloir supprimer ? \n Fermer comptabilité Kiwi_AE aprés validation  !", icon='warning')
	if result == 'yes':
		shutil.rmtree(racine_Avoir      +  'Avoir_PDF_' + ligne[46])#, ignore_errors=True)
		shutil.rmtree(racine_Avoir_brut + 'Avoir_brut_' + ligne[47])
	else:
		pass 
def del_compta_interVention():
	result = messagebox.askquestion("Purge Intervention_"+ ligne[50] , "êtes-vous sur de vouloir supprimer ? \n Fermer comptabilité Kiwi_AE aprés validation  !", icon='warning')
	if result == 'yes':
		shutil.rmtree(racine_Inter      + 'InterVention_PDF_' + ligne[50])
		shutil.rmtree(racine_Inter_brut + 'InterVention_brut_' + ligne[51])
	else:
		pass 
def del_compta_bank():
	result = messagebox.askquestion("Purge Livre de banque PDF sauf base comptabilité"+ ligne[50] , "êtes-vous sur de vouloir supprimer ? \n Fermer comptabilité Kiwi_AE aprés validation  !", icon='warning')
	if result == 'yes':
		shutil.rmtree(racine_banck + 'Recette_PDF_'+ ligne[50] )
		shutil.rmtree(racine_banck_ods + 'Recette_ods')
		shutil.rmtree(racine_banck_ods + 'Recette_db')
	else:
		pass
def del_compta_total():
	result = messagebox.askquestion("Purge sauf Tier planning", "êtes-vous sur de vouloir supprimer !!!\n Fermer comptabilité Kiwi_AE aprés validation !\n Il faut initialiser Tier puis livre de recette\n en ouvrant et fermant", icon='warning')
	if result == 'yes':
		shutil.rmtree(racine_Devis        + 'Devis_PDF_'        + ligne[42])
		shutil.rmtree(racine_Devis_brut   + 'Devis_brut_'       + ligne[43])
		shutil.rmtree(racine_Facture      + 'Facture_PDF_'      + ligne[44])
		shutil.rmtree(racine_Facture_brut + 'Facture_brut_'     + ligne[45])
		shutil.rmtree(racine_Avoir        + 'Avoir_PDF_'        + ligne[46])
		shutil.rmtree(racine_Avoir_brut   + 'Avoir_brut_'       + ligne[47])
		shutil.rmtree(racine_Inter        + 'InterVention_PDF_' + ligne[48])
		shutil.rmtree(racine_Inter_brut   + 'InterVention_brut_'+ ligne[49])
		shutil.rmtree(racine_recette)                     #     + ligne[50])
		shutil.rmtree(racine_stock)
		# Copier
		lisCopie            =r''+ racine_tier+'listTier_Copie.dat'
		thier   			=r''+ racine_tier+'listTier.dat'
		shutil.copy(lisCopie, thier)
		annCopie 			=r''+ racine_plann + 'Anniversaire_Copie.dat'
		anni    			=r''+ racine_plann + 'Anniversaire.dat'
		shutil.copy(annCopie,anni)
		plannCopie 			=r''+ racine_plann+'Plannig_Copie.dat' 
		plann       		=r''+ racine_plann+'Plannig.dat'
		shutil.copy(plannCopie,plann)
		plann_Kiwi_AE_Copie =r''+ racine_plann+'Planning_Kiwi_AE_Copie.dat' 
		plann_Kiwi_AE       =r''+ racine_plann+'Planning_Kiwi_AE.dat'
		shutil.copy(plann_Kiwi_AE_Copie,plann_Kiwi_AE)

	else:
		pass