#!/usr/bin/python3
# coding:utf-8
from Config  import *
import tkinter as tk
from tkinter import ttk
from tkinter.ttk import*
from tkinter import LabelFrame, Label, Tk
from tkcalendar import Calendar, DateEntry
import os,pwd
os.getlogin = lambda: pwd.getpwuid(os.getuid())[0]
import subprocess
import tkinter.font as font
from tkinter import messagebox
from tkinter.messagebox import *
from os import chdir
from contextlib import suppress
from tkinter import simpledialog
import sys
from PIL import Image, ImageTk
import platform
import locale
import time
import sqlite3

uname = platform.uname()
################ Appel 
from Sauve_toi import *
import WidgetsFac as WidgetsFac_fen
import WidgetsDev as WidgetsDev_fen
import WidgetsAvo as WidgetsAvo_fen
import Inter as Inter_fen
import Recette as Recette
import Tier as Tier
import Stock as Stock
import Planning_Kiwi_AE as Planning_Kiwi_AE
from lib.libAuxilaire import *
from lib.libExplore import *
from lib.DevFacAvo import *
import lib.DevFacAvo
import Alerte as Alerte
import random
import Sauve_toi  
from datetime import date,timedelta

def OpenConfig_Kiwi_AE(func,line):
	f =open(chem_note + 'Config_Kiwi_AE')#starter Graphe
	start_graph = f.readlines()[line]
	if start_graph == "0\n":
		pass
	else:
		func()
	f.close()
def boot_start():
	try:
		OpenConfig_Kiwi_AE(graphique,55)
	except:
		pass
	OpenConfig_Kiwi_AE(DossierFac_Pdf,56)
	OpenConfig_Kiwi_AE(DossierDev_Pdf,57)
	OpenConfig_Kiwi_AE(DossierAvo,58)
	OpenConfig_Kiwi_AE(Recette_Pdf,59)
	OpenConfig_Kiwi_AE(Planning,60)
	OpenConfig_Kiwi_AE(anniversaire,61)
	OpenConfig_Kiwi_AE(bien_venue,62)

def graphique():
	global CA,DEP,VTE,debit1,grah,Date_annee,info_1
	Date_annee = time.strftime('%Y')
	Date_annee_1 = int(Date_annee)-1
	Date_annee_2 = int(Date_annee)-2
	lire = dossier 
	def info_1():
		color_font = '#D3D7CF'
		infomation = LabelFrame(grah,bd=1,relief=RAISED,font=("Arial",10),bg=color_font)
		infomation.place(x=550,y=11,width=414,height=609)
		
		Label(infomation, text="Légende", font=("calibri",11,'bold'),bg=color_font).place(x=80,y=5)                                                 
		Label(infomation, text="Recettes  "+Date_annee, font=("calibri",11),bg=color_font).place(x=15,y=30 )                          
		Label(infomation, text=Date_annee_1, font=("calibri",11),bg=color_font).place(x=200,y=30)
		Label(infomation, text=Date_annee_2, font=("calibri",11),bg=color_font).place(x=300,y=30)
		Label(infomation, text="_____", font=("calibri",11,'bold'),fg='#204A87',bg=color_font).place(x=140,y=25,height=20) 
		Label(infomation, text="_____", font=("calibri",11,'bold'),fg='green',bg=color_font).place(x=240,y=25,height=20) 
		Label(infomation, text="_____", font=("calibri",11,'bold'),fg='grey',bg=color_font).place(x=340,y=25,height=20)
		Label(infomation, text="Dépences "+Date_annee, font=("calibri",11),bg=color_font).place(x=15,y=55)                          
		Label(infomation, text=Date_annee_1, font=("calibri",11),bg=color_font).place(x=200,y=55)                          
		Label(infomation, text=Date_annee_2, font=("calibri",11),bg=color_font).place(x=300,y=55)
		Label(infomation, text="_____", font=("calibri",11,'bold'),fg='red',bg=color_font).place(x=140,y=48,height=20) 
		Label(infomation, text="_____", font=("calibri",11,'bold'),fg='brown',bg=color_font).place(x=240,y=48,height=20) 
		Label(infomation, text="_____", font=("calibri",11,'bold'),fg='Yellow',bg=color_font).place(x=340,y=48,height=20)
		Label(infomation, text="Chiffres année "+ Date_annee+'         Service        Vente' , font=("calibri",11,'bold'),bg=color_font).place(x=20,y=75)
		total_vte = 0
		total_ser = 0
		total_dep2 = 0
		total_dep3 = 0
		for x in range(i):
			CA = Grah(x+1)
			CA_1 = Grah_1(x+1)
			DEP = debit(x+1)

			SER = info_service(x+1)
			VTE = info_vente(x+1)
			total_vte += VTE 
			total_ser += SER
			#total_dep2 += DEP2
			#total_dep3 += DEP3
			Label(infomation,text=mois[x], font=("calibri",12),bg=color_font).place(x=5,y=95+x*24)
			Label(infomation,text=str(CA), font=("calibri",11),fg='blue',bg=color_font).place(x=60,y=95+x*24)
			Label(infomation,text=str(DEP),font=("calibri",11),fg='red',bg=color_font).place(x=140,y=95+x*24)
			Label(infomation,text=str(SER),font=("calibri",11),fg='black',bg=color_font).place(x=240,y=95+x*24)
			Label(infomation,text=str(VTE),font=("calibri",11),fg='black',bg=color_font).place(x=340,y=95+x*24)

		valeur = somm1()
		valeur2 = somm2()
		valeur3 = somm3()
		total_ser_1 = service1()
		total_vte_1 = vente1()
		DEP1  = debit_1()
		DEP2 = debit2()
		total_ser_2 = service2()
		total_vte_2 = vente2()

		DEP3 = debit3()
		Label(infomation, text="________________________", font=("calibri",11,'bold'),bg=color_font).place(x=230,y=375,height=20)
		Label(infomation, text="Chiffres affaire", font=("calibri",12),bg=color_font).place(x=5,y=390)       
		Label(infomation, text= str(valeur), font=("calibri",12),fg='blue',bg=color_font).place(x=135,y=390)
		Label(infomation, text=total_vte, font=("calibri",12),bg=color_font).place(x=330,y=405)       
		Label(infomation, text=total_ser, font=("calibri",12),bg=color_font).place(x=230,y=405)       
		Label(infomation, text="Dépences", font=("calibri",12),bg=color_font).place(x=5,y=415)              
		Label(infomation, text=str(DEP1), font=("calibri",12),fg='red',bg=color_font).place(x=135,y=415)
		Label(infomation, text="Chiffres année "+ str(Date_annee_1)+'     Service    Vente', font=("calibri",12,'bold'),bg=color_font).place(x=20,y=440)  
		Label(infomation, text="Chiffres affaire" , font=("calibri",12),bg=color_font).place(x=5,y=465)         
		Label(infomation, text=str(valeur2), font=("calibri",12),fg='green',bg=color_font).place(x=135,y=465)         
		Label(infomation, text=total_ser_1, font=("calibri",12),bg=color_font).place(x=230,y=475)       
		Label(infomation, text=total_vte_1, font=("calibri",12),bg=color_font).place(x=330,y=475)
		Label(infomation, text="Dépences", font=("calibri",12),bg=color_font).place(x=5,y=490)             
		Label(infomation, text=str(DEP2), font=("calibri",12),fg='brown',bg=color_font).place(x=135,y=490)             
		Label(infomation, text="Chiffres année "+ str(Date_annee_2)+'     Service    Vente',font=("calibri",12,'bold'),bg=color_font).place(x=20,y=515)
		Label(infomation, text=total_ser_2, font=("calibri",12),bg=color_font).place(x=230,y=545)       
		Label(infomation, text=total_vte_2, font=("calibri",12),bg=color_font).place(x=330,y=545)
		Label(infomation, text="Chiffres affaire",font=("calibri",12),bg=color_font).place(x=5,y=540)         
		Label(infomation, text=str(valeur3), font=("calibri",12),fg='grey',bg=color_font).place(x=135,y=540)         
		Label(infomation, text="Dépences  ",font=("calibri",12),fg='black',bg=color_font).place(x=5,y=565)              
		Label(infomation, text=str(DEP3),fg='Yellow',font=("calibri",12),bg=color_font).place(x=135,y=565)	 
		#fer=Button(infomation,text =">>", command = lambda:info_1.destroy)
		#fer.place(x=275,y=570)
	##########################################
	##########################################
	
	def info(colone,date_annee,date = "%"):
		global vente1,total_vte
		conn = sqlite3.connect(lire)
		cur = conn.cursor()
		if date == "%":
			pass
		elif date < 10:
		    date = "0"+str(date)
		else:
		    date = str(date)
		vente1 = cur.execute("SELECT SUM("+colone+") FROM REGISTRE WHERE GDATE LIKE ?",("%" + "-" +date+ "-" +str(date_annee),))
		vente1 = vente1.fetchone()[0]
		if vente1 == None:
		    vente1 = 0
		conn.commit()
		conn.close()
		return vente1
	def info_vente(date):
		return info("VENTE",Date_annee,date)
	def info_service(date):
		return info('SERVICE',Date_annee,date)
	def service2():
		return info("SERVICE",Date_annee_2)
	def vente2():
		return info("VENTE",Date_annee_2)
	def service1():
		return info("SERVICE",Date_annee_1)
	def vente1():
		return info("VENTE",Date_annee_1)
	def debit3():
		return info("DEBIT",Date_annee_2)
	def debit2():
		return info("DEBIT",Date_annee_1)
	def debit_1():
		return info("DEBIT",Date_annee)
	def debit(date):
		return info("DEBIT",Date_annee,date)
	def somm3():
		return info("CREDIT",Date_annee_2)
	def somm2():
		return info("CREDIT",Date_annee_1)
	def somm1():
		return info("CREDIT",Date_annee)
	def Grah_2(date):
		return info("CREDIT",Date_annee_2,date)
	def Grah_1(date):
		return info("CREDIT",Date_annee_1,date)
	def Grah(date):
		return info("CREDIT",Date_annee,date)
		'''
		conn = sqlite3.connect(lire)
		cur = conn.cursor()
		if date < 10:
		    date = "0"+str(date)
		else:
		    date = str(date)
		value = cur.execute("SELECT SUM(CREDIT) FROM REGISTRE WHERE GDATE LIKE ?",("%" + "-" +date+ "-" +str(Date_annee),))
		value = value.fetchone()[0]
		if value == None:
		    value = 0
		conn.commit()
		conn.close()
		return value
		'''
	##########################################
	##########################################
	echelle =[0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,"!..."]
	mois =['Janv','Févr','Mars','Avril','Mai','Juin','Juillet','Aout','Sept','Oct','Nov','Déc']
	grah = LabelFrame(fenetre,bd=1,relief=RAISED,font=("Arial",10),bg=ligne[19])
	grah.place(x=115,relx = 0.0,rely = 1.0,anchor ='sw',width=970,height=660)

	width=1000
	height=660
	cnv = Canvas(grah, width=width, height=height,bg=ligne[19])
	cnv.pack()
	
	i = 12
	y = list()
	cote_bas = 625
	cote_ligne = 630
	ali =10
	cote_espace = 43
	cote_chiffre_0 = 10
	CA = 0                                                                                                 # Chiffre affaire Total Facture
	CA_1 = 0
	CA_2 = 0
	DEP = 0
	post_CA = 0
	post_DEP = 0

	for x in range(i):
		CA = Grah(x+1)
		CA_1 = Grah_1(x+1)
		CA_2 = Grah_2(x+1)
		DEP = debit(x+1)
		
		if CA > CA/2:
			CA   = CA/2
			if CA > 1800:
				CA = 1800
		else:
			pass
		if CA_1 > CA_1/2:
			CA_1   = CA_1/2
			if CA_1 > 1800:
				CA_1 = 1800
		else:
			pass
		if CA_2 > CA_2/2:
			CA_2   = CA_2/2
			if CA_2 > 1800:
				CA_2 = 1800
		else:
			pass
		if DEP > DEP/2:
			DEP   = DEP/2
			if DEP > 1800:
				DEP = 1800
		else:
			pass		
			
		if x != 0:
			cnv.create_line(65+(x-1)*cote_espace, post_CA/3+10, 65+x*cote_espace, CA/3+10,fill='#204A87',width=2)#,dash=1)   # Graph gauche ,bas,droite,mesure
			cnv.create_line(65+(x-1)*cote_espace, post_CA_1/3+10, 65+x*cote_espace, CA_1/3+10,fill='Green',width=2)#,dash=1)   # Graph gauche ,bas,droite,mesure
			cnv.create_line(65+(x-1)*cote_espace, post_CA_2/3+10, 65+x*cote_espace, CA_2/3+10,fill='grey',width=2)#,dash=1)   # Graph gauche ,bas,droite,mesure
			
			#cnv.create_line(69+(x-1)*cote_espace, post_DEP/3+10, 69+x*cote_espace, DEP/3+10, fill='#EF2929',width=3)   # Graph
			
		post_CA  = CA
		post_CA_1  = CA_1
		post_CA_2  = CA_2
		post_DEP = DEP

		for w in range(i):
			Label(grah, text=mois[w],bg=ligne[0],font=("calibri",9)).place(x=85+w*42,y=cote_bas)                # Mois 
	for u in range(19):
		Label(grah, text=echelle[u],bg=ligne[0],font=("calibri",11)).place(x=20,y=5+u*33)                       # Heures
	cnv.create_line(60,cote_chiffre_0,964,cote_chiffre_0,fill='black', width=0)									 # trait horizontale dessus 
	cnv.create_line(60,cote_ligne-10,60,10,fill='black', width=0)                                                # trait de Vertical
	cnv.create_line(549,cote_ligne-10,549,10,fill='black', width=0)                                              # trait de Vertical
	cnv.create_line(964,cote_ligne-10,964,10,fill='black', width=0)                                              # trait de Vertical
	cnv.create_line(60,cote_ligne-10,964,cote_ligne-10,fill='black', width=0) 									 # trait horizontale bas 
	info_1()   
	Button(grah,text ="Quitte", command = grah.destroy).place(x=896,y=623)
	
	def ipayer():
		es = LabelFrame(grah)
		es.place(x=4,y=4,width=314,height=309)
def display_shell(command):
    texte = ' '
    while texte != '':
        texte = command.stdout.readline()
        zoneTexte.insert(END, texte)
        zoneTexte.yview(END)
        zoneTexte.update()
def efface_carte():
	carte.destroy()
def meteo():
	global carte,Meteo,trouve_Meteo,affiche_wlocal,local
	print (ligne[48],ligne[48])
	local = ligne[48]
	trouve_Meteo = chem_parametre 
	wlocal = str(' wttr.in/'+local+'_0tqp_lang'+'='+'fr.png')
	try:
		Meteo = (trouve_Meteo +local+'_0tqp_lang=fr.png')
	except:
		pass
	try:
		os.remove(Meteo)
	except FileNotFoundError:
		pass
	Meteo = ('wget -P'+trouve_Meteo+ wlocal)#'  wttr.in/Niort_0tqp_lang=fr.png')
	#print (Meteo,'Meteo') #########################################################################
	os.system (Meteo)
	photo = PhotoImage(file= chem_parametre + local + '_0tqp_lang=fr.png')
	photo = photo.subsample(1,1)
	carte = Button(fenetre, image = photo,command=efface_carte)
	carte.image = photo
	carte.place(relx = 0.5,rely = 0.5, anchor = 'center')
def sett():
	#fenetre.deiconify()
	setBotun(fenetre)
def sett_bis():
	#fenetre.deiconify()
	devisfacture(fenetre)
def Alert():
	#fenetre.iconify()
	reset_menu()
	Alerte.main(sett,fenetre)
def sauvetoi():	
	#fenetre.iconify()
	reset_menu()
	Sauve_toi.Sauvetoi(sett,fenetre)	
def Agenda():
	#fenetre.iconify()
	reset_menu()
	Planning_Kiwi_AE.main(sett,fenetre)
def StockMain():
	#fenetre.iconify()
	reset_menu()
	Stock.main(sett,fenetre)	
def Saisirecette():
	#fenetre.iconify()
	reset_menu()
	Recette.main(sett,fenetre)
def TierMain():
	#fenetre.iconify()
	reset_menu()
	Tier.main(sett,fenetre)	
def	ouvrireWidgetsAvo():
	#fenetre.iconify()
	sett_fac()
	WidgetsAvo_fen.main(sett_bis,fenetre)
def ouvrireWidgetsDev():
	#fenetre.iconify()
	sett_fac()
	WidgetsDev_fen.main(sett_bis,fenetre)
def ouvrireWidgetsFac():
	#fenetre.iconify()
	sett_fac()
	WidgetsFac_fen.main(sett_bis,fenetre)
def	ouvrireWidgetsInt():
	#fenetre.iconify()
	sett_fac()
	Inter_fen.main(sett_bis,fenetre)
def a_propos():
	mon_message = messagebox.showinfo("Comptabilité Kiwi_AE:", "Comptabilité Tréfle pour\n Auto_entrepreuneure Auteur Roland et Evan \nseraytrefle@free.fr\nversion 0.5 \nversion d'essai ")
	menuHelp.add_command(label="A propos",font=(arial), command=a_propos)
def configuration():
	Guid()

def give_passwor():
	global password1,passwor
	passe = (chem_note + 'Pass_Kiwi_AE')
	f = open(passe, "r")
	passwor = f.readline().replace("\n","")
	#print(passwor)
	f.close()
	return passwor
def mot_passe ():
	login = ""
	password_ = give_passwor();
	while login != password_:
		login = simpledialog.askstring(title="Identifiant?",
						prompt="Mot de Passe Kiwi_AE?:",
						show="*",
						parent=fenetre)
		if login == None:
			quit()
def resize_image(event): 
	new_width = event.width 
	new_height = event.height 
	image = copy_of_image.resize((new_width, new_height)) 
	photo = ImageTk.PhotoImage(image) 
	label.config(image = photo) 
	label.image = photo 
fenetre = tk.Tk()
try:
	fenetre.configure(bg=ligne[0])
except :
	messagebox.showinfo("Information","Couleur blanc par defaut Probléme voire configuration")  
	fenetre.configure(bg='#FFFFFF')
	ligne[0]='#FFFFFF'
try:
	image = Image.open(chem_parametre +'Font_ecran/'+ligne[53]) 
	copy_of_image = image.copy() 
	photo = ImageTk.PhotoImage(image) 
	label = ttk.Label(fenetre,image = photo) 
	label.bind('<Configure>',resize_image) 
	label.pack(side = BOTTOM ,fill=BOTH, expand = YES)	
except:
	pass
try:
	largeur_win = int(ligne[1].split('x',1)[0])
	haut_win = int(ligne[1].split('x',1)[1].split('+',1)[0])
	window_height = haut_win
	window_width = largeur_win
	fenetre.geometry(ligne[1])
except:
	messagebox.showinfo("Information","Dimention par defaut\n Probléme voire configuration") 
	fenetre.geometry('790x650')
	ligne[1] = '790x450'	
Date_semaine = time.strftime(" %W") 
Date_jour = time.strftime("%A %d %B %Y")
fenetre.title("Kiwi_AE     "+' Bonjour Mr '+Nnom+ '   '+Date_jour+ '   Semaine '+ Date_semaine)
icone = PhotoImage(file= '/opt/Kiwi_AE/Icon/kiwi6.png')
fenetre.iconphoto(True, icone)
fenetre.bind('<Escape>',  lambda event:fenetre.destroy())
mot_passe ()
#def plein():
#	fenetre.attributes("-fullscreen", True)	
def legende():
	Barre_titre = LabelFrame(fenetre,fg='blue',bg=ligne[0])
	Barre_titre.place(relx=0.1,rely=0.0,relwidth=0.9,height=30) 
	photo = PhotoImage(file= chem_icon +'X.png')
	photo = photo.subsample(10,10)
	sortie_2 = Button(Barre_titre, image = photo,command=exit,bg='white')
	sortie_2.image = photo
	sortie_2.pack(side=RIGHT,fill=Y)
	#buton = tkinter.Button(Barre_titre, text='Plein ecran',font=("calibri",11),command=plein)
	#buton.pack(side=RIGHT)
	Label(Barre_titre, text='Semaine '+ Date_semaine,bg=ligne[0],font=("calibri",12)).pack(side=RIGHT,fill=Y,padx=4,ipadx=4)
	Label(Barre_titre, text= Date_jour,font=("calibri",12),bg=ligne[0]).pack(side=RIGHT,fill=Y,padx=4,ipadx=4)

def del_Poupmenu ():
	showMenu.destroyer()
def Poupmenu ():
	def showMenu(e):
		global showMenu
		pmenu.post(e.x_root, e.y_root)
	color_font = '#D3D7CF'#C6FAC6'
	color_polise = "black"
	pmenu =     tkinter.Menu(fenetre,font='Arial 14',relief=SUNKEN,bg=color_font,fg = color_polise, tearoff=0)
	menupopup = tkinter.Menu(pmenu,font = 'Arial 12',relief=SUNKEN,bg=color_font,fg = color_polise, tearoff=0)
	menu3 =     tkinter.Menu(pmenu,font = 'Arial 12',relief=SUNKEN,bg=color_font,fg = color_polise, tearoff=0)
	to_erase =  tkinter.Menu(pmenu,font = 'Arial 12',relief=SUNKEN,bg=color_font,fg = color_polise, tearoff=0)
	menu4 =     tkinter.Menu(pmenu,font = 'Arial 12',relief=SUNKEN,bg=color_font,fg = color_polise, tearoff=0)
	menudelcompta = tkinter.Menu(to_erase,font = 'Arial 12',relief=RAISED,bg='white',fg = color_polise, tearoff=0)
	
	to_erase.add_command(label="Effacer Dossier Facture",command=del_cadrefac)
	to_erase.add_command(label="Effacer Dossier Devis",command=del_cadredev)
	to_erase.add_command(label="Effacer Dossier Avoir ",command=del_avoir)
	to_erase.add_command(label="Effacer Recette mensuel",command=del_cadreRec)
	to_erase.add_command(label='Effacer tableau de bord',command=lambda: grah.destroy())
	to_erase.add_command(label='Effacer rappel rendez-vous',command=del_plann)
	to_erase.add_command(label='Effacer rappel annuelle',command=del_anniversaire)
	to_erase.add_cascade(label="Effacer Comptabilité",menu=menudelcompta)

	menudelcompta.add_command(label="Purge Devis année "+ ligne[42],command=del_compta_devis)
	menudelcompta.add_command(label="Purge Facture année "+ ligne[44],command=del_compta_facture)
	menudelcompta.add_command(label="Purge Avoir année "+ligne[46],command=del_compta_avoir)
	menudelcompta.add_command(label="Purge Intervention année "+ligne[48],command=del_compta_interVention)
	menudelcompta.add_command(label="Purge Banque année "+ligne[50],command=del_compta_bank)
	menudelcompta.add_command(label="Purge gestion Total",command=del_compta_total)

	menupopup.add_command(label="Recheche Facture",command=recheche_facture)
	menupopup.add_command(label="Recheche Devis",command=recheche_devis)
	menupopup.add_command(label="Recheche Avoir",command=recheche_avoir)
	menupopup.add_command(label="Recheche Intervention",command=recheche_Inter)
	menupopup.add_command(label="Recheche livre de Banque",command=demanderLienRec)
	
	menu3.add_command(label="Dossier Facture",command=DossierFac_Pdf)
	menu3.add_command(label="Dossier Devis",command=DossierDev_Pdf)
	menu3.add_command(label="Dossier Recettes",command=Recette_Pdf)
	menu3.add_command(label="Dossier Avoir",command=DossierAvo)
	menu3.add_command(label="Tableau de bord",command=graphique)
	menu3.add_command(label="Rendez_vous",command=Planning)
	menu3.add_command(label="Anniversaire",command=anniversaire)

	menu4.add_command(label="Fiche identité entreprise",command=Entete)
	menu4.add_command(label="Changer mon de passe",command=modifi_pass)   
	menu4.add_command(label="Réserve devis",command=devis_reserve)
	menu4.add_command(label="Pied de page facture Devis avoir",command=Pied)
	menu4.add_command(label="Pied de page livre de recette",command=pied_livre)
	#menu4.add_command(label="Effacer Comptabilité",command=del_compta)
	
	pmenu.add_cascade(label="Recheche",menu=menupopup)
	pmenu.add_cascade(label="Afficher les Dossiers",menu=menu3)
	pmenu.add_cascade(label='Effacer les Dossiers',menu=to_erase)
	pmenu.add_cascade(label="Divers utils",menu=menu4)
	pmenu.add_command(label="Calendrier",command=Wcal)
	pmenu.add_command(label="Carte de Visite",command=carte_visite)
	pmenu.add_command(label="Météo",command= meteo)
	#pmenu.add_command(label="Afficher pleine ecran",command= plein)
	#pmenu.add_command(label="Signalez un problème",command= rapporter_un_bug)
	pmenu.add_command(label="A propos des auteurs",command= contact_Kiwi_AE)
	pmenu.add_command(label="Sortie Popmenu",command=del_Poupmenu)
	pmenu.add_command(label="Sortie de Kiwi_AE", command=fenetre.destroy)
	fenetre.bind("<Button-3>", showMenu)
# 1 rang de Bouton 
def setBotun(fenetre):
	global  Table,back,config,bisness,planning,stock,Bank,client2,admin,label_setBotune,Barre_titre
	label_setBotune = LabelFrame(fenetre)
	label_setBotune.place(relx = 0.0,rely = 0.5,anchor ='w',y=5,x=5)  #relwidth=0.6,relheight=0.4
			
	photo = PhotoImage(file= chem_icon+'rendez_vous.png')
	photo = photo.subsample(5,5)
	Table = Button(label_setBotune,font=("bold",8, ), image = photo,command=Alert,bg='white')#, compound=BOTTOM)
	Table.image = photo
	Table.pack(anchor= N,fill=X)
	
	photo = PhotoImage(file= chem_icon+'Sauve_toi.png')
	photo = photo.subsample(5,5)
	back = Button(label_setBotune,font=("bold",8, ), image = photo,command=sauvetoi,bg='white')
	back.image = photo
	back.pack(fill=X)#side=LEFT,fill=X)
	
	photo = PhotoImage(file= chem_icon+'Kiwi_AE.gif')
	photo = photo.subsample(4,4)
	config = Button(label_setBotune,image = photo,command=configuration,bg='white')
	config.image = photo
	config.pack(fill=X)
	
	photo = PhotoImage(file=chem_icon + 'pdf.png')
	photo = photo.subsample(5,5)
	bisness = Button(label_setBotune,image = photo,command = setpdf,bg='white')
	bisness.image = photo
	bisness.pack(fill=X)
	
	photo = PhotoImage(file= chem_icon+'planning.png')
	photo = photo.subsample(5,5)
	planning = Button(label_setBotune,text='Planning',font=("calibri"), image = photo,command=Agenda,anchor=N,bg='white',compound=CENTER)
	planning.image = photo
	planning.pack(fill=X)
		
	photo = PhotoImage(file= chem_icon+'stock.png')
	photo = photo.subsample(5,5)
	stock = Button(label_setBotune,text= 'Gestion\n Article',font=("calibri"),anchor=N, image = photo,command=StockMain,bg='white',compound=CENTER)
	stock.image = photo
	stock.pack(fill=X)
	
	photo = PhotoImage(file= chem_parametre+'Bank/' + ligne[22])#Credit_Mutuel.png')
	photo = photo.subsample(5,5)
	Bank = Button(label_setBotune,text='Livre\n Recette',font=("calibri"),anchor=N, image = photo,command=Saisirecette,bg='white',compound=CENTER)
	Bank.image = photo
	Bank.pack(fill=X)
	
	photo = PhotoImage(file=chem_icon+ 'N_client2.png')
	photo = photo.subsample(5,5)
	client2 = Button(label_setBotune,text= 'Gestion\n Tiers',font=("calibri"),anchor=N,image = photo,command=TierMain,bg='white',compound=CENTER)
	client2.image = photo
	client2.pack(fill=X)
	
	photo = PhotoImage(file= chem_icon+'admin.png')
	photo = photo.subsample(5,5)
	admin = Button(label_setBotune,text= 'Devis\n Facture\nAvoir',font=("calibri"),anchor=N,image = photo,command=business,bg='white',compound=CENTER)
	admin.image = photo
	admin.pack(fill=X)

	photo = PhotoImage(file= chem_icon+'X.png')  
	photo = photo.subsample(4,4)
	admin = Button(label_setBotune,image = photo,command=exit,bg='white')
	admin.image = photo
	admin.pack(fill=X)

def devisfacture(fenetre):
	global ndevis,nfacture,sortie_2,reserve,pied,entete,aideges,Inter,nAvoir,label_devisfacture
	
	label_devisfacture = LabelFrame(fenetre)
	label_devisfacture.place(relx = 0.0,rely = 0.5,anchor ='w',y=5,x=5)
	
	aideges = Button(label_devisfacture,text='Pied de\npage livre\ndes recette',font=("calibri",11),command=pied_livre)
	aideges.pack(fill=X)
	
	reserve = Button(label_devisfacture,text= 'Réserve\nDevis',font=("calibri"), command=devis_reserve, compound=CENTER)
	reserve.pack(fill=X)
	
	entete = Button(label_devisfacture,text= 'Entéte\nPage',font=("calibri"),command=Entete, compound=CENTER)
	entete.pack(fill=X)

	pied = Button(label_devisfacture,text= 'Clause de \npénalité\n paiement',anchor=N,font=("calibri"),command=Pied, compound=CENTER)
	pied.pack(fill=X)
	
	photo = PhotoImage(file= chem_icon +'Inter.png')
	photo = photo.subsample(6,4)
	Inter = Button(label_devisfacture,text= 'SOS\nDépannage',font=("calibri"),image = photo,command=ouvrireWidgetsInt, compound=CENTER,bg='white')
	Inter.image = photo
	Inter.pack(fill=Y)
		
	photo = PhotoImage(file= chem_icon+'Client.png')
	photo = photo.subsample(9,9)
	nAvoir = Button(label_devisfacture,text= 'Avoir',font=("calibri"),fg='black', image = photo,command=ouvrireWidgetsAvo, compound=CENTER,bg='white')
	nAvoir.image = photo
	nAvoir.pack(fill=X)
	
	photo = PhotoImage(file=chem_icon+'devis.png')
	photo = photo.subsample(5,5)
	ndevis = Button(label_devisfacture,text= 'Devis',anchor=N,font=("calibri"),image = photo,command=ouvrireWidgetsDev, compound=CENTER,bg='white')
	ndevis.image = photo
	ndevis.pack(fill=X)
	
	photo = PhotoImage(file= chem_icon+'client02.png')
	photo = photo.subsample(5,5)
	nfacture = Button(label_devisfacture,text= 'Facture',anchor=N,font=("calibri"),image = photo,command=ouvrireWidgetsFac, compound=CENTER,bg='white')
	nfacture.image = photo
	nfacture.pack(fill=X)
	
	photo = PhotoImage(file= chem_icon+'sortie.png')
	photo = photo.subsample(4,4)
	sortie_2 = Button(label_devisfacture, image = photo,command=Delette,bg='white')
	sortie_2.image = photo
	sortie_2.pack(fill=X)
def pdf1():
	os.system('xdg-open '+ chem_gestion_Kiwi_AE +'Donnees_PDF/'+ligne[28]+'.pdf')
def pdf2():
	os.system('xdg-open '+ chem_gestion_Kiwi_AE +'Donnees_PDF/'+ligne[29]+'.pdf')
def pdf3():
	os.system('xdg-open '+ chem_gestion_Kiwi_AE +'Donnees_PDF/'+ligne[30]+'.pdf')
def pdf4():
	os.system('xdg-open '+ chem_gestion_Kiwi_AE +'Donnees_PDF/'+ligne[31]+'.pdf')	
def pdf5():
	os.system('xdg-open '+ chem_gestion_Kiwi_AE +'Donnees_PDF/'+ligne[32]+'.pdf')
def pdf6():
	os.system('xdg-open '+ chem_gestion_Kiwi_AE+'Donnees_PDF/'+ligne[33]+'.pdf')
def aide_menu():
	os.system('xdg-open '+ chem_parametre +'Aide_Kiwi_AE/menu_aide.pdf') # bon
def A_Aide_menu():
	os.system('xdg-open '+ chem_parametre +'Aide_menu.pdf')
def aide_ges():
	os.system('xdg-open '+ chem_parametre +'Aide_fac_dev.pdf')
def setpdf():
	global pdf1,pdf2,pdf3,pdf4,pdf5,pdf6,sortiePDF,ligne,sortie_2,aidemenu,button,bande,label_pdf
	wid = 100
	hei = 75
	i = [Table,config,back,bisness,planning,stock,Bank,client2,admin,label_setBotune]
	for i_ in i:
		i_.destroy()
	label_pdf = LabelFrame(fenetre)
	label_pdf.place(relx = 0.0,rely = 0.5,anchor ='w',y=5,x=5)

	photo = PhotoImage(file= chem_icon+'aide.png')
	photo = photo.subsample(4,4)
	aidemenu = Button(label_pdf,image = photo,command=aide_menu,bg='#FFFFFF')
	aidemenu.image = photo
	aidemenu.pack(fill=X)
	
	button = tkinter.Button(label_pdf, text='Banque PDF\nGestionnaire',font=("calibri"),command=recheche_pdf)
	button.pack(fill=X)
	
	photo = PhotoImage(file= chem_icon +'pdf.png')
	photo = photo.subsample(6,6)
	pdf6 = Button(label_pdf,text=ligne[33],font=("calibri"), image = photo,command=pdf6, compound=CENTER)
	pdf6.image = photo
	pdf6.pack(fill=X)
	
	photo = PhotoImage(file= chem_icon+'pdf.png')
	photo = photo.subsample(6,6)
	pdf5 = Button(label_pdf,text=ligne[32],font=("calibri"), image = photo,command=pdf5, compound=CENTER)
	pdf5.image = photo
	pdf5.pack(fill=X)
	
	photo = PhotoImage(file= chem_icon+'pdf.png')
	photo = photo.subsample(6,6)
	pdf4 = Button(label_pdf,text=ligne[31],font=("calibri"), image = photo,command=pdf4, compound=CENTER)
	pdf4.image = photo
	pdf4.pack(fill=X)
	
	photo = PhotoImage(file= chem_icon +'pdf.png')
	photo = photo.subsample(6,6)
	pdf3 = Button(label_pdf,text=ligne[30],font=("calibri"), image = photo,command=pdf3, compound=CENTER)
	pdf3.image = photo
	pdf3.pack(fill=X)
	
	photo = PhotoImage(file=chem_icon+'pdf.png')
	photo = photo.subsample(6,6)
	pdf2 = Button(label_pdf,text=ligne[29],font=("calibri"), image = photo,command=pdf2, compound=CENTER)
	pdf2.image = photo
	pdf2.pack(fill=X)

	photo = PhotoImage(file= chem_icon+'pdf.png')
	photo = photo.subsample(6,6)
	pdf1 = Button(label_pdf,text=ligne[28],font=("calibri"), image = photo,command=pdf1, compound=CENTER)
	pdf1.image = photo
	pdf1.pack(fill=X)

	photo = PhotoImage(file= chem_icon+'sortie.png')
	photo = photo.subsample(4,5)
	sortiePDF = Button(label_pdf, image = photo,command=Delette_pdf)
	sortiePDF.image = photo
	sortiePDF.pack(fill=X)
def business():
	i = [Table,back,config,bisness,planning,stock,Bank,client2,admin,label_setBotune]
	for i_ in i:
		i_.destroy()
	devisfacture(fenetre)
def Delette():
	i = [ndevis,nfacture,sortie_2,reserve,pied,entete,aideges,nAvoir,Inter,label_devisfacture]
	for i_ in i:
		i_.destroy()
	setBotun(fenetre)
def Delette_pdf():
	i = [pdf1,pdf2,pdf3,pdf4,pdf5,pdf6,sortiePDF,aidemenu,button,label_setBotune,label_pdf]
	for i_ in i:
		i_.destroy()
	setBotun(fenetre)
def sett_fac():
	i = [ndevis,nfacture,sortie_2,reserve,pied,entete,aideges,nAvoir,Inter,label_devisfacture]
	for i_ in i:
		i_.destroy()
def reset_menu():
	i = [Table,back,config,bisness,planning,stock,client2,Bank,admin,label_setBotune]
	for i_ in i:
		i_.destroy()
#legende()
Poupmenu()
boot_start()
setBotun(fenetre)
fenetre.mainloop()
