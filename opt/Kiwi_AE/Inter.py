#coding:utf-8
import tkinter
from tkinter import ttk
import subprocess
from lib.libAuxilaire import *
from lib.libExplore import *
import lib.DevFacAvo as DevFacAvo
import saveLoadTier

import lib_pdf.Inter_PDF as Inter_PDF
import lib.NumeroClass
import lib.class_nline as ClnL
from Config  import *
import lib.Inter_marge as genre_Inter_marge
entier_Int = lib.NumeroClass.trouveNumInter()
#colorer = '#E5E5E5'
#==================================================================================================================================================
#                                                                 class
#==================================================================================================================================================
class affai():
	def __init__(self,cord,text,frame):
		self.cord = cordonné()
		print(cord)
		self.cord.x_text , self.cord.y_text = cord[0]
		self.cord.x_saisi , self.cord.y_saisi = cord[1]
		self.label = Label(frame,tcoucouCestMoisext=text)
		self.entry = Entry(frame)
		self.label.place(x = self.cord.x_text, y = self.cord.y_text)
		self.entry.place(x = self.cord.x_saisi, y = self.cord.y_saisi)
	def get(self):
		return self.entry.get()
#gestion des cordonné
class cordonné():
	def __init__(self):
		self.x_text = 0
		self.y_text = 0
		self.x_saisi = 0
		self.y_saisi = 0
#==================================================================================================================================================
#                                                                 autre
#==================================================================================================================================================
#trouver non fichier
def Entier():
	global nom,rue,rue2,codepos,ville, objet,objet_suite, colone, netht,date
	global fen,frame,affai 
	lien = liste_inter_brut + str(entier_Int) 
	genre_Inter_marge.main(lien,affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date, entier_Int)
	forme_PDF()
#lire un PDF
def forme_PDF():
	global affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date, entier_Int,date
	global fen,frame
	lien =  liste_inter_brut+ str(entier_Int)                #str(NumeroFAC.trouveNumFac()) + ".dat"
	Inter_PDF.main(lien,affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date,entier_Int)
# Livre de Recette   
def jour_view():
	global date
	def print_sel(e):
		print(date.get_date())
	date = DateEntry(fen5, width=12, background='darkblue',locale='fr_FR',
					foreground='white',font =("Times",14), borderwidth=2)
	date.place(x=90,y=99)
	date.bind("<<DateEntrySelected>>", print_sel)
def NumeroInter():
	global entier_Int, labelnu
	label = tkinter.Label(fen5,text=" Intervention ",fg = "black",font = ("arial", 13, "italic")) 
	label.place(x=450,y=125)
	
	labelnu = tkinter.Label(fen5,text= entier_Int ,relief=SUNKEN,bg ="white", fg = "blue",font = ("arial", 13, "italic"), bd = 2) # Numero de Facture
	labelnu.place(x=560,y=125)
#Reset tout les Empty	
def reset():
	global  entier_Int
	affai.delete(0,END)
	nom.delete(0,END)
	rue.delete(0,END)
	rue2.delete(0,END)
	codepos.delete(0,END)
	ville.delete(0,END)
	objet.delete(0,END)
	objet_suite.delete(0,END)
	labelnu.config(text="")
	
	entier_Int = lib.NumeroClass.trouveNumInter()
	labelnu.config(text= entier_Int)   ##################################### erreure

def openFile_Inter():
	global affai,nom,rue,rue2,codepos,ville, objet,objet_suite,entier_Int,date,createFileBu,fen,supre_colone_select,frame
	entier_Int = genre_Inter.openf(demanderLien(Open = True),affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date,entier_Int,createFileBu,fen,frame,labelnu)#,ad_colone)
#                                                                 gestion graphique
#==================================================================================================================================================
#questionair de la Intervention
def setFenaitre(fen,sett_fac):
	global affai, nom,rue,rue2,codepos,ville, objet,objet_suite, entier_Int, ref,frame,labelnu
	global desig,qte,remi,uht,tht, netht,createFileBu,frame
	#Affaire
	DevFacAvo.labelfacdevavohaut(fen5)
	affai = tkinter.Entry(fen5,font = arial)
	affai.place(x=90, y=2,height=28,width=200)
	objet = tkinter.Entry(fen5,font = (arial))
	objet.place(x=90, y=31,height=30,width=200)
	objet_suite = tkinter.Entry(fen5,font = (arial))
	objet_suite.place(x=90, y=61,height=28,width=200)
	listecredit = nom_Tier()
	validate = (fen5.register(_validate), '%P')    
	nom = ttk.Combobox(fen5, values = listecredit,font = (arial), validatecommand=validate,validate='key')
	nom.bind('<<ComboboxSelected>>', select_Tier)
	nom.place(x=430, y=2,height=28,width=250)
	rue = tkinter.Entry(fen5,font = (arial))
	rue.place(x=430, y=31,height=28,width=250)
	rue2 = tkinter.Entry(fen5,font = (arial))
	rue2.place(x=430, y=60,height=28,width=250)
	codepos = tkinter.Entry(fen5,width=6,font = (arial))
	codepos.place(x=430, y=90,height=28,width=50)
	ville = tkinter.Entry(fen5,font =("Times",15)) 
	ville.place(x=480, y=90,height=28,width=200)
	
def nom_Tier():
	return list(saveLoadTier.load_to_list())
def _validate(s):
	nv = [ x for x in nom_Tier() if x.startswith(s) ]
	nom['values'] = nv
	return True
def select_Tier(x):
	global nom,rue,rue2,codepos,ville
	Tier = saveLoadTier.load_to_list()[nom.get()]
	"""
	{"nom" : "Duran Endree",
	"rue" : "6 rue des Charbon",
	"rue2" : "",
	"codePostal" : "79000",
	"ville" : "Niort",
	"mobil" : "0606010203",
	"fix" : "0906",
	"email" : "trefle@free.fr"}
	"""
	rue.delete(0,END)
	rue.insert(0, (str(Tier['rue'])))
	rue2.delete(0,END)
	rue2.insert(0, (str(Tier['rue2'])))
	codepos.delete(0,END)
	codepos.insert(0, (str(Tier['codePostal'])))
	ville.delete(0,END)
	ville.insert(0, (str(Tier['ville'])))
	#crées les bouton
def Voire_Inter_en_couros():
	#voire = os. startfile(liste_inter+'/'+ entier_Int+'_'+nom.get().replace(" ", "")+'.pdf')                                                                                                              
	voire = os.system('xdg-open '+ liste_inter + entier_Int+'_'+nom.get().replace(" ", "")+'.pdf')  
	try:
		f = open(voire)
	except:# (IOError, ValueError):
		pass#var = messagebox.showinfo("Erreure commande" , "          Cause possible!...                   Il faut d'abord faire une sauvegarde et aprés export en PDF!    à voire carte.png")
#=======================================================================
# FONCTION PRINCIPAL
#=======================================================================
def sortie_Int(sett_fac):
	def func():
		fen.destroy()
		sett_fac()
	return func
def main(sett_fac,fenètrePrimère,ouvrireFile = False):
	global affai, nom,rue,rue2,codepos,ville,objet,objet_suite, entier_Int,frame,labelnu, date
	global createFileBu,colone,fen,frame,line,fen5
	fen=tkinter.Toplevel(fenètrePrimère)
	x = fenètrePrimère.winfo_x()
	y = fenètrePrimère.winfo_y()
	try:
		fen.configure(bg='#FFFFFF')#ne4)
	except :
		messagebox.showinfo("Information","Couleur blanc par defaut\n Probléme voire configuration")  
		fen.configure(bg='#FFFFFF')
		ligne[3]='#FFFFFF'
	try:
		dx = 150
		dy = 50
		fen.geometry("%dx%d+%d+%d" % (692, 200, x + dx, y + dy))
	except:
		messagebox.showinfo("Information","Dimention par defaut\n Probléme voire configuration") 
		fen.geometry('692x600')
		ligne[4] = '692x600'
	fen.title(" Intervention")
	icone = PhotoImage(file=chem_icon+'Inter.png')
	fen.iconphoto(True, icone)
	#fen.protocol('WM_DELETE_WINDOW', lambda: None)
	fen.attributes('-topmost', 1)
	fen.wm_attributes('-type','splash')
	fen5 = LabelFrame(fen,relief=RAISED)#,bg= 'antiquewhite')
	fen5.place(x=1, y=2,height=160,width=690)

	bt2 = LabelFrame(fen,relief=RAISED,fg = "#FFFFFF")
	bt2.place(x=2, y=160,height=38,width=320)
	bouton2 = Button(bt2,text="sauvegarde ",bg ="green",fg = "#FFFFFF",width=9,font =("arial", 14, "bold"),command=Entier)
	bouton2.grid(row=1,sticky="nw")
	bouton3 = Button(bt2,text="export PDF",bg ="green",fg = "#FFFFFF",width=9,font =("arial", 14, "bold"),command=Voire_Inter_en_couros)
	bouton3.grid(row=1,column=1)
	bouton9 = Button(bt2,text='Sortie ',bg ="brown",fg = "#FFFFFF",width=6,font = ("Times", 14, "bold"),command=sortie_Int(sett_fac))
	bouton9.grid(row=1,column=2,ipadx=2,sticky="nw")
	#========================================================================================
	#		Crées le questionair de factur
	#========================================================================================
	setFenaitre(fen,sett_fac)
	############### Enplacement
	jour_view() # Date
	NumeroInter()
	if ouvrireFile:
		openFile()
	fen.mainloop()
if __name__ == '__main__':
	f = tk.Tk()
	main(f)
	f.mainloop()
