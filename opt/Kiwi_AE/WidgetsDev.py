# coding: utf-8
import tkinter
import PIL.Image
import sqlite3
import lib.DevFacAvo as DevFacAvo
from lib.libAuxilaire import *
from lib.libExplore import *
import lib.NumeroClass 
import lib.class_nline as ClnL
import lib.facture as genre_facture
import lib_pdf.devis_PDF as devis_PDF
from Config  import *
import saveLoadTier
import os,pwd
os.getlogin = lambda: pwd.getpwuid(os.getuid())[0]
entier_dev = lib.NumeroClass.trouveNumDev()
#==================================================================================================================================================
#                                                                 class
#==================================================================================================================================================
class affai():
	def __init__(self,cord,text,frame):
		self.cord = cordonné()
		print(cord)
		self.cord.x_text , self.cord.y_text = cord[0]
		self.cord.x_saisi , self.cord.y_saisi = cord[1]
		self.label = Label(frame,tcoucouCestMoisext=text)
		self.entry = Entry(frame)
		self.label.place(x = self.cord.x_text, y = self.cord.y_text)
		self.entry.place(x = self.cord.x_saisi, y = self.cord.y_saisi)
	def get(self):
		return self.entry.get()
#gestion des cordonné
class cordonné():
	def __init__(self):
		self.x_text = 0
		self.y_text = 0
		self.x_saisi = 0
		self.y_saisi = 0
#==================================================================================================================================================
#                                                                 autre
#==================================================================================================================================================
#trouver non fichier
def Entier():
	global affai, nom,rue,rue2,codepos,ville, objet,objet_suite, colone, netht, acompte,date,entier_dev
	global devis,frame
	lien = liste_devis_brut + str(entier_dev)
	try:
		genre_facture.main(lien,affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date, colone, acompte, netht,entier_dev,select)
	except:
		messagebox.showerror("Erreure","devis feuille protéger",parent=devis)
		pass 
	forme_PDF()
#lire un PDF
def forme_PDF():
	global affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date, entier_dev, colone, netht, acompte,date
	global devis,frame,ligne,close_check,pro_check,reserve_check,select
	lien = liste_devis + str(entier_dev)
	devis_PDF.main(lien,affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date,entier_dev,colone, netht,acompte,ligne,var1_banck,close_check,pro_check,reserve_check,select)
# Livre de Recette   
def jour_view():
	global date
	def print_sel(e):
		print(date.get_date())
	 
	date = DateEntry(fen5, width=12, background='darkblue',
					foreground='white',font =("Times",14), borderwidth=2)
	date.place(x=90,y=90)
	date.bind("<<DateEntrySelected>>", print_sel)
def NumeroDevis():
	global entier_dev, labelnu
	labelnu = tkinter.Label(fen5,text= entier_dev ,bg ='#F6F6D0',relief=RAISED, fg = "black",font = ("arial", 13,"italic")) # Numero de Facture
	labelnu.place(x=420,y=120,width=250) 
	label = tkinter.Label(fen5,text=" Devis N°",fg = "blue",font = ("arial",13), bd = 2) 
	label.place(x=290,y=120)
#ouivrire les text brut
def ouvrirtextE():
	global entier_dev, labelnu
	lien = (liste_facture_brut + entier_dev)# + ".dat") 
	ouvrirtexte.coucouCestMois(lien,entier_dev, labelnu)
def aide_facdevavo():
	try:
		devis.attributes('-topmost',False)
		os.system('xdg-open '+'/home/'+ Nnom +'/Gestion_Kiwi_AE/Parametres/Aide_Kiwi_AE/Guide_fac.pdf') 
	except:
		messagebox.showerror("Erreure","Pas de fichier ",parent=devis)		
		pass
#Reset tout les Empty		
def reset():
	global  entier_dev
	affai.delete(0,END)
	nom.delete(0,END)
	rue.delete(0,END)
	rue2.delete(0,END)
	codepos.delete(0,END)
	ville.delete(0,END)
	objet.delete(0,END)
	objet_suite.delete(0,END)
	labelnu.config(text="")
	netht.delete(0,END)
	sous_tt.config(text="")
	acompte.delete(0,END)
	Reset_colone()
	entier_dev = lib.NumeroClass.trouveNumDev()
	labelnu.config(text= entier_dev)   
def demanderLien_devis(Open = False):
	if not Open:
		return tkinter.filedialog.asksaveasfilename(parent=devis,filetypes =[("Dat Files","DEV**")]) 
	else:
		return tkinter.filedialog.askopenfilename(parent=devis,initialdir=racine_Devis_brut,
													title='Sélection Devis',
													filetypes =[("Dat Files","DEV**")])	
def openFile():
	global affai, nom,rue,rue2,codepos,ville, objet,objet_suite,colone,entier_dev,date,ref, netht, acompte,createFileBu,devis,supre_colone_select,frame,ligne
	entier_dev = genre_facture.openf(demanderLien_devis(Open = True),affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date,acompte,netht, entier_dev,colone, createFileBu,devis,supre_colone_select,frame,labelnu,ligne)#,ad_colone)
	NumeroDevis()
def total():
	global tht,acompte,netht
	global colone
	sous_tt_ = 0
	sous_tt_ = float(sous_tt_)
	for x in colone:
		sous_tt_ += x.calcul()
	val = acompte.get()
	if val == '':
		val = '0'
	nethtf= float(float(sous_tt_) - float(val))
	if nethtf <= 0:
		messagebox.showerror("Erreure","Nombre négatif sur\nstock ou modifier \nles Quantité",parent=devis) 
	else:
		netht.config(text = nethtf)
		sous_tt.config(text = str(sous_tt_))
#==================================================================================================================================================
#                                                                 gestion colone
#==================================================================================================================================================
#ajouté une colonne
def actualisationConteur():
	global Conpteur, colone
	Conpteur.config(text=len(colone))
def ad_colone():
	global colone,createFileBu,frame,ligne
	colone.append(ClnL.nligne((len(colone)),frame,supre_colone_select,up_ligne,down_ligne,copie_ligne))
	createFileBu.pack(pady=0+(len(colone)-1)*27)
	actualisationConteur()
#suprimer une colonne
def supre_colone_fin():
	global colone,createFileBu,frame
	supre_colone_select(len(colone)-1,frame)()
#reset tout les colonne
def Reset_colone():
	global colone,createFileBu,frame
	print (type(colone))
	for ligne in range(len(colone)):
		colone[0].delette()
		del colone[0]
	createFileBu.pack()
	ad_colone()
	actualisationConteur()
#suprimer un colonne selectioner
def supre_colone_select(ligne):
	global colone,createFileBu,frame
	def supre_colone():
		global colone,createFileBu,frame
		print (type(colone))
		if len(colone) <= 1:
			return True
		colone[ligne].delette() 
		del colone[ligne]
		t = 0
		for ligne_saisi in colone:
			ligne_saisi.replacer(t)
			t += 1
			createFileBu.pack(pady=0+(len(colone)-1)*27)
		actualisationConteur()
	return supre_colone
def up_ligne(ligne):
	global colone,createFileBu,frame
	def func():
		global colone,createFileBu,frame
		if ligne != 0:
			colone[ligne],colone[ligne-1] = colone[ligne-1],colone[ligne]
			t = 0
			for ligne_saisi in colone:
				ligne_saisi.replacer(t)
				t += 1
				createFileBu.pack(pady=0+(len(colone)-1)*27)
	return func	
def down_ligne(ligne):
	global colone ,createFileBu,frame
	def func():
		if ligne != len(colone)-1:
			colone[ligne],colone[ligne+1] = colone[ligne+1],colone[ligne]
			t = 0
			for ligne_saisi in colone:
				ligne_saisi.replacer(t)
				t += 1
				createFileBu.pack(pady=0+(len(colone)-1)*27)
	return func
def copie_ligne(ligne):
	global colone,createFileBu,frame,copier
	def func():
		global colone,createFileBu,frame,copier
		if ligne == len(colone)-1:
			colone.append(None)	
		tmp = colone[ligne+1]
		colone[ligne+1] = ClnL.nligne((len(colone)),frame,supre_colone_select,up_ligne,down_ligne,copie_ligne)
		colone[ligne+1].sette(*colone[ligne].give_element())
		for l in range(ligne+2,len(colone)):
			colone[l],tmp = tmp,colone[l]
		if tmp != None:
			colone.append(tmp)
		t = 0
		for ligne_saisi in colone:
			ligne_saisi.replacer(t)
			t += 1
			createFileBu.pack(pady=0+(len(colone)-1)*27)
	return func
#==================================================================================================================================================
#                                                                 gestion graphique
#==================================================================================================================================================
#questionair de la devis
def setFenaitre(devis,sett_bis):
	global affai, nom,rue,rue2,codepos,ville, objet,objet_suite, entier_dev, ref,frame,labelnu
	global desig,qte,remi,uht,tht, netht, acompte, createFileBu, colone,frame,nethtf,Conpteur,var1_banck,close_check,pro_check,reserve_check,select
	#Affaire
	#fram4 = Label(devis,bg ='white',relief=RAISED)
	#fram4.place(x=2, y=2,height=160,width=310)
	fen4 = LabelFrame(devis,text='Option impression devis',fg='blue',font=("Times",14,'bold'),relief=RAISED)
	fen4.place(x=10, y=1,height=150,width=240)
	#DevFacAvo.obtion_impression(fen4)
	var1 = IntVar()
	var1_banck = 0
	def ada():
		global var1_banck
		var1_banck = var1.get()
	Checkbutton(fen4, text=" Iban", variable=var1,relief=RAISED,font ='("Arial 11',anchor="w",width=16,command=ada).grid(row=0,column=0,padx=2,ipadx=25)
	
	var3 = IntVar()
	close_check= 0
	def adb():
		global close_check
		close_check = var3.get()
	Checkbutton(fen4, text=" Pénalité", variable=var3,relief=RAISED,font ='("Arial 11',anchor="w",width=16,command=adb).grid(row=1,column=0,padx=2,ipadx=25)
	
	var4 = IntVar()
	pro_check= 0
	def adc():
		global pro_check
		pro_check = var4.get()
	Checkbutton(fen4, text="Edition professionnelle", variable=var4,relief=RAISED,font ='("Arial 11',anchor="w",width=16,command=adc).grid(row=2,column=0,padx=2,ipadx=25)
	
	var2 = IntVar()
	reserve_check= 0
	def add():
		global reserve_check
		reserve_check = var2.get()
	Checkbutton(fen4, text=" Réserve Devis", variable=var2,relief=RAISED,font ='("Arial 11',anchor="w",width=16,command=add).grid(row=3,column=0,padx=2,ipadx=25)
	

	#Label(fen4,text='Date limite de paiement',relief=RAISED,font ='("Arial 11').grid(row=4,column=0,padx=2,pady=5,ipadx=20)
	
	Eche = IntVar()
	def action(event):
		global select
		select= Eche.get()
	Echeance = open(chem_note +'echeances',"r").readlines()
	for t in range(len(Echeance)):
		Echeance[t] = Echeance[t][:len(Echeance[t])-1]
	Eche = ttk.Combobox(fen4,values = Echeance,state='readonly',width=16,font=("calibri",11),textvariable=Echeance) #state="readonly"
	Eche.current(0)
	Eche.bind('<<ComboboxSelected>>',action)
	Eche.grid(row=5,column=0,padx=2,ipadx=30,sticky='n') 	
	select= Eche.get()

	#DevFacAvo.affiche_carte(fram4)	
	DevFacAvo.labelfacdevavohaut(fen5)
	affai = tkinter.Entry(fen5,font =("Times",14))
	affai.place(x=90, y=2,height=28,width=200)
	objet = tkinter.Entry(fen5,font =("Times",14))
	objet.place(x=90, y=31,height=30,width=200)
	objet_suite = tkinter.Entry(fen5,font =("Times",14))
	objet_suite.place(x=90, y=61,height=28,width=200)
	listecredit = nom_Tier()
	validate = (fen5.register(_validate), '%P')    
	nom = ttk.Combobox(fen5, values = listecredit,font =("Times",14), validatecommand=validate,validate='key')
	nom.bind('<<ComboboxSelected>>', select_Tier)
	nom.place(x=420, y=2,height=28,width=250) 
	rue = tkinter.Entry(fen5,font =("Times",14))
	rue.place(x=420, y=31,height=28,width=250)
	rue2 = tkinter.Entry(fen5,font =("Times",14))
	rue2.place(x=420, y=60,height=28,width=250)
	codepos = tkinter.Entry(fen5,width=6,font =("Times",14))
	codepos.place(x=420, y=90,height=28,width=50)
	ville = tkinter.Entry(fen5,font =("Times",14))
	ville.place(x=470, y=90,height=28,width=200)
	
	barre = Label(devis)#,bg =bar)#,relief=RAISED)
	barre.place(x=2, y=154,height=25,width=1018)
	DevFacAvo.bande(barre) 
	Conpteur= tkinter.Label(barre,text='Ligne'+"   ",fg = "green",bg= 'antiquewhite',width=7,relief = RAISED,font = ("Times", 12))
	Conpteur.place(x=0, y=0,width=55,height=22)
def nom_Tier():
	return list(saveLoadTier.load_to_list())
def _validate(s):
	nv = [ x for x in nom_Tier() if x.startswith(s) ]
	nom['values'] = nv
	return True
def select_Tier(x):
	global nom,rue,rue2,codepos,ville
	Tier = saveLoadTier.load_to_list()[nom.get()]
	"""
	{"nom" : "Duran Endree",
	"rue" : "6 rue des Charbon",
	"rue2" : "",
	"codePostal" : "79000",
	"ville" : "Niort",
	"mobil" : "0606010203",
	"fix" : "0906",
	"email" : "trefle@free.fr"}
	"""
	rue.delete(0,END)
	rue.insert(0, (str(Tier['rue'])))
	rue2.delete(0,END)
	rue2.insert(0, (str(Tier['rue2'])))
	codepos.delete(0,END)
	codepos.insert(0, (str(Tier['codePostal'])))
	ville.delete(0,END)
	ville.insert(0, (str(Tier['ville'])))
	#crées les bouton	
def Voire_devis_en_couros():
	try:
		devis.wm_attributes('-topmost',False)
		voire = os.system ('xdg-open '+ liste_devis  + entier_dev + '_' +nom.get().replace(" ", "")+'.pdf')   
		f = open(voire)
	except:
		messagebox.showerror("Erreure","Pas de saisie en cours ",parent=devis)
		#devis.wm_attributes('-topmost',True)
		pass
# Popmenu
def del_Poupmenu ():
	showMenu.destroyer()
def tk_popup( x, y, entry=""): 
	call('tk_popup', self._w, x, y, entry) 
def couper():
	global devis
	devis.clipboard_clear()
	a=devis.focus_get().index(ANCHOR)
	i=devis.focus_get().index(INSERT)
	if i<a:a,i=i,a
	t=devis.focus_get().get()
	s=t[a:i]
	t=t[0:a] + t[i:]
	devis.focus_get().delete(0,END)
	devis.focus_get().insert(0, t)
	devis.focus_get().icursor(a)
	devis.clipboard_append(s)
def copier():
	devis.clipboard_clear()
	devis.clipboard_append(devis.selection_get())
def coller():
	t=devis.selection_get(selection='CLIPBOARD')
	devis.focus_get().insert(INSERT,t)
def sortie_Dev(sett_bis):
	def func():
		devis.destroy()
		sett_bis()
	return func
def setBotun(devis,sett_bis):
	bt2 = Label(devis,bg =ligne[3],relief=RAISED)
	bt2.place(x=2, y=490,height=108,width=380)#width=380)
	bouton = Button(bt2,text="Total devis",bg ="green",fg = "#FFFFFF",width=16,font =("arial", 14),command=total)
	bouton.grid(sticky="nw")
	bouton2 = Button(bt2,text="sauvegarde devis",bg ="green",fg = "#FFFFFF",width=16,font =("arial", 14),command=Entier)
	bouton2.grid(row=1,sticky="nw")
	bouton3 = Button(bt2,text="export PDF",bg ="green",fg = "#FFFFFF",width=16,font =("arial", 14),command=Voire_devis_en_couros)
	bouton3.grid(row=2,sticky="nw")
	bouton4 = Button(bt2,text="Nouveau devis",width=16,font =("arial", 14),command=reset)
	bouton4.grid(row=0,column=1,sticky="nw")
	bouton5 = Button(bt2,text="Modifier un devis",width=16,font =("arial", 14),command=openFile)
	bouton5.grid(row=1,column=1,sticky="nw")
	bouton8 = Button(bt2,text='Aide',width=7,font = ("Times", 14),command=aide_facdevavo)
	bouton8.grid(row=2,column=1,sticky="nw")
	bouton9 = Button(bt2,text='Sortie',bg ="brown",fg = "#FFFFFF",width=6,font = ("Times", 14),command=sortie_Dev(sett_bis))
	bouton9.grid(row=2,column=1,ipadx=7,padx=92,sticky="nw")	
def Poupmenu ():
	def showMenu(e):
		pmenu.post(e.x_root, e.y_root)
	pmenu = tkinter.Menu(devis,font = 'Arial 10',relief=SUNKEN, fg = "#4D4D4D", tearoff=0)
	menu3 = tkinter.Menu(pmenu,font = 'Arial 10',relief=SUNKEN, tearoff=0)
	pmenu.add_command(label="Copier", command=copier)
	pmenu.add_command(label="Coller",command=coller)
	pmenu.add_command(label="Effacer",command=couper)
	pmenu.add_command(label="Exit", command=devis.destroy)

	devis.bind("<Button-3>", showMenu)
def resize_image(event): 
	new_width = event.width 
	new_height = event.height 
	image = copy_of_image.resize((new_width, new_height)) 
	photo = ImageTk.PhotoImage(image) 
	label.config(image = photo) 
	label.image = photo 

#=======================================================================
# FONCTION PRINCIPAL
#=======================================================================
def main(sett_bis,fenètrePrimère,ouvrireFile = False):
	global affai, nom,rue,rue2,codepos,ville,objet,objet_suite, entier_dev,ref,frame,labelnu, date,copy_of_image,label
	global desig, qte, remi,uht,tht,netht,sous_tt,acompte,createFileBu,colone,devis,frame,ligne,fen5
	devis = tkinter.Toplevel(fenètrePrimère)
	#entier_dev = lib.NumeroClass.trouveNumDev()
	try:
		devis.configure(bg=ligne[3])
	except :
		messagebox.showinfo("Information","Couleur blanc par defaut\n Probléme voire configuration",parent=devis)    
		devis.configure(bg='#FFFFFF')
		ligne[3]='#FFFFFF'
	try:
		image = Image.open(chem_parametre +'Font_ecran/'+ligne[53]) 
		copy_of_image = image.copy() 
		photo = ImageTk.PhotoImage(image) 
		label = ttk.Label(devis,image = photo) 
		label.bind('<Configure>',resize_image) 
		label.pack(side = BOTTOM ,fill=BOTH, expand = YES)	
	except:
		pass
	try:
		x = fenètrePrimère.winfo_x()
		y = fenètrePrimère.winfo_y()
		w = int(ligne[4].split('x',1)[0]) 
		h = int(ligne[4].split('x',1)[1].split('+',1)[0])
		if w != 1040:
			w=1040
		if h != 600:
			h=600
		dx = ligne[20] #80
		dy = ligne[21]
		devis.geometry("%dx%d+%d+%d" % (w, h, x + int(str(dx)), y + int(str(dy))))
		#devis.config(relief = RAISED,bd=1 )
	except:
		messagebox.showinfo("Information","Dimention par defaut\n Probléme voire configuration",parent=devis)  
		devis.geometry('1029x600')
		ligne[4] = '1029x600'
	fen5 = Label(devis,relief=RAISED)
	fen5.place(x=335, y=2,height=160,width=685)
	
	devis.title(" Devis")
	icone = PhotoImage(file='/opt/Kiwi_AE/Icon/Client.png')
	devis.iconphoto(True, icone)
	#devis.bind('<Escape>',  lambda event:devis.destroy())
	#devis.attributes('-topmost',True)
	devis.transient(fenètrePrimère)
	devis.wm_attributes('-type','splash')
	#devis.protocol('WM_DELETE_WINDOW', lambda: None)
	Poupmenu ()
	#========================================================================================
	#		Crées le questionair de factur
	#========================================================================================
	setFenaitre(devis,sett_bis)
	############### Enplacement 
	jour_view() # Date
	NumeroDevis()
	######################### boucle
	frameq = Label(devis,bg ="White")
	frameq.place(x=4, y=174, width=1030,height=300)
	frame = SimpleScrolledFrame(frameq )		
	frame.pack(fill=BOTH, expand=True)
	createFileBu=Button(frame,width=4,text='N/Ligne',font = ("Times",9),bg ="white", fg = "green",command = ad_colone)
	createFileBu.pack(side=LEFT)
	cadre_total = Label(devis,bg ='#FFFFFF')#lightgray')#,relief = RAISED)
	cadre_total.place(x=700, y=485,height=113,width=289)
	label=tkinter.Label(cadre_total,text="Sous total ",relief=RAISED, fg = "black",font=('families 14'),anchor=W).place(x=1,y=0,width=165,height=35)
	
	sous_tt = tkinter.Label(cadre_total,width=8,bg='#F6F6D0',font=('Helvetica 14'))
	sous_tt.place(x=168,y=1,width=115,height=35)

	label = tkinter.Label(cadre_total,text="Acompte ",relief=RAISED,font = ('families 14'),anchor=W).place(x=1,y=36,width=165,height=35)
	acompte = tkinter.Entry(cadre_total,justify=CENTER,width=8,font=('Helvetica 14'),relief=RAISED)
	acompte.place(x=168,y=37,width=115,height=35)
	acompte.insert(END,"0.0")
			
	label = tkinter.Label(cadre_total,text="PRIX GLOBAL",relief=RAISED,font=('Helvetica 14'),anchor=W).place(x=1,y=72,width=165,height=35)
	netht = tkinter.Label(cadre_total,justify=CENTER,width=8,bg ='#F6F6D0',font =('Helvetica 14'),relief=RAISED)
	netht.place(x=168,y=72,width=115,height=35)
	setBotun(devis,sett_bis)
	colone = [ClnL.nligne(0,frame,supre_colone_select,up_ligne,down_ligne,copie_ligne)]
	createFileBu.pack(pady=0 + (len(colone)-1)*27)   
	if ouvrireFile:
		openFile()
	devis.mainloop()
if __name__ == '__main__':
	f = Tk()
	main(f)
	f.mainloop()
