# coding: utf-8
import tkinter
import tkinter as tk
from tkinter import ttk
import sqlite3
import os
from lib.libAuxilaire import *
from lib.libExplore import *
import lib.DevFacAvo as DevFacAvo
import lib.NumeroClass
import lib.class_nline as ClnL
import lib.facture as genre_facture
import lib_pdf.avoir_PDF as avoir_PDF
import saveLoadTier
from Config  import *
entier_AVO = lib.NumeroClass.trouveNumAvo()

#==================================================================================================================================================
#                                                                 class
#==================================================================================================================================================
class affai():
	def __init__(self,cord,text,frame):
		self.cord = cordonné()
		print(cord)
		self.cord.x_text , self.cord.y_text = cord[0]
		self.cord.x_saisi , self.cord.y_saisi = cord[1]
		self.label = Label(frame,tcoucouCestMoisext=text)
		self.entry = Entry(frame)
		self.label.grid(x = self.cord.x_text, y = self.cord.y_text)
		self.entry.grid(x = self.cord.x_saisi, y = self.cord.y_saisi)
	def get(self):
		return self.entry.get()
#gestion des cordonné
class cordonné():
	def __init__(self):
		self.x_text = 0
		self.y_text = 0
		self.x_saisi = 0
		self.y_saisi = 0
#==================================================================================================================================================
#                                                                 autre
#==================================================================================================================================================
#trouver non fichier
def Entier():
	global affai, nom,rue,rue2,codepos,ville, objet,objet_suite, colone, netht, acompte,date
	global avoir,frame,affai 	
	lien = liste_avoir_brut + str(entier_AVO)
	try:
		genre_facture.main(lien,affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date, colone, acompte, netht,entier_AVO,select)
	except:
		messagebox.showerror("Erreure","Fichier passer en comptabilité\n soldé nom modifiable",parent=devis)
		pass 
	update_stock(Nnom)
	forme_PDF()
def update_stock(Nnom):
	global colone
	conn = sqlite3.connect(liste_stock)
	for line in colone:
		ref = line.ref.get()
		qt = int(float(line.qte.get()))
		#print(">>>",ref,end = "\n")
		cursor = conn.cursor()
		cursor.execute("UPDATE REGISTRE SET QTE = QTE-? WHERE REFERENCE LIKE ?;",(qt,ref))
		conn.commit()
	conn.close()

#lire un PDF
#lire un PDF
def forme_PDF():
	global affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date, entier_AVO, colone, netht, acompte,date
	global avoir,frame,var1_banck,pro_check,select
	lien = liste_avoir + ligne[44] + str(entier_AVO)# + ".dat"   #str(NumeroFAC.trouveNumFac()) + ".dat"
	avoir_PDF.main(lien,affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date,entier_AVO,colone, netht,acompte,ligne,var1_banck,close_check,pro_check,select)
# Livre de Recette   
def NumeroAvoir():
	global entier_AVO, labelnu
	labelnu = tkinter.Label(fen5,text= entier_AVO ,bg ='#F6F6D0',relief=RAISED,font = ("arial", 13)) # Numero de Facture
	labelnu.place(x=420,y=125,width=250)
	label = tkinter.Label(fen5,text=" Avoir N°", fg = "blue",font = ("arial", 12)) 
	label.place(x=290,y=125)
#ouivrire les text brut
def ouvrirtextE():
	global entier_AVO, labelnu
	lien = (liste_avoir_brut + entier_AVO)# + ".dat")
	ouvrirtexte.coucouCestMois(lien,entier_AVO, labelnu)
#Reset tout les Empty	
def reset():
	global  entier_AVO
	affai.delete(0,END)
	nom.delete(0,END)
	rue.delete(0,END)
	rue2.delete(0,END)
	codepos.delete(0,END)
	ville.delete(0,END)
	objet.delete(0,END)
	objet_suite.delete(0,END)
	labelnu.config(text="")
	netht.delete(0,END)
	sous_tt.config(text="")
	acompte.delete(0,END)
	Reset_colone()
	entier_AVO = lib.NumeroClass.trouveNumAvo()
	labelnu.config(text= entier_AVO)   ##################################### erreure

def demanderLien_avoir(Open = False):
	if not Open:
		return tkinter.filedialog.asksaveasfilename(parent=avoir,filetypes = [("Dat Files","AVO**")]) 
	else:
		return tkinter.filedialog.askopenfilename(parent=avoir,initialdir = racine_Avoir_brut,
												title = 'Sélection Avoir',
												filetypes = [("Dat Files","AVO**")])
def openFile_avoir():
	global affai,nom,rue,rue2,codepos,ville, objet,objet_suite,colone,entier_AVO,date,ref, netht, acompte,createFileBu,avoir,supre_colone_select,frame
	entier_AVO = genre_facture.openf( demanderLien_avoir(Open = True),affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date,acompte,netht, entier_AVO,colone, createFileBu,avoir,supre_colone_select,frame,labelnu,var1_banck)#,ad_colone)
	NumeroAvoir()
#calcul des totaux                                                        
def total():
	global tht,acompte,netht
	global colone
	sous_tt_ = 0
	sous_tt_ = float(sous_tt_)
	for x in colone:
		sous_tt_ += x.calcul()
	val = acompte.get()
	if val == '':
		val = '0'
	nethtf= float(float(sous_tt_) - float(val))
	if nethtf <= 0:
		messagebox.showerror("Erreure","Nombre négatif sur\nstock ou modifier \nles Quantité",parent=avoir) 	
	else:
		netht.config(text = nethtf)
		sous_tt.config(text = str(sous_tt_))	
#==================================================================================================================================================
#                                                                 gestion colone
#==================================================================================================================================================
#ajouté une colonne
def actualisationConteur():
	global Conpteur, colone
	Conpteur.config(text=len(colone))
def ad_colone():
	global colone,createFileBu,frame,ligne
	colone.append(ClnL.nligne((len(colone)),frame,supre_colone_select,up_ligne,down_ligne,copie_ligne))
	createFileBu.pack(pady=0+(len(colone)-1)*27)
	actualisationConteur()
#suprimer une colonne
def supre_colone_fin():
	global colone,createFileBu,frame
	supre_colone_select(len(colone)-1,frame)()
	actualisationConteur()
def aide_facdevavo():
	try:
		avoir.wm_attributes('-topmost',False)
		os.system('xdg-open '+'/home/'+ Nnom +'/Gestion_Kiwi_AE/Parametres/Aide_Kiwi_AE/Guide_fac.pdf') 
	except:
		messagebox.showerror("Erreure","Pas de fichier ",parent=avoir)	
		avoir.wm_attributes('-topmost', True)
		pass	
def Reset_colone():
	global colone,createFileBu,frame
	print (type(colone))
	for ligne in range(len(colone)):
		colone[0].delette()
		del colone[0]
	createFileBu.pack()
	ad_colone()
	actualisationConteur()
#suprimer un colonne selectioner
def supre_colone_select(ligne):
	global colone,createFileBu,frame
	def supre_colone():
		global colone,createFileBu,frame
		print (type(colone))
		if len(colone) <= 1:
			return True
		colone[ligne].delette() 
		del colone[ligne]
		t = 0
		for ligne_saisi in colone:
			ligne_saisi.replacer(t)
			t += 1
			createFileBu.pack(pady=0+(len(colone)-1)*27)
		actualisationConteur()
	return supre_colone
def up_ligne(ligne):
	global colone,createFileBu,frame
	def func():
		global colone,createFileBu,frame
		if ligne != 0:
			colone[ligne],colone[ligne-1] = colone[ligne-1],colone[ligne]
			t = 0
			for ligne_saisi in colone:
				ligne_saisi.replacer(t)
				t += 1
				createFileBu.pack(pady=0+(len(colone)-1)*27)
	return func	
def down_ligne(ligne):
	global colone ,createFileBu,frame
	def func():
		if ligne != len(colone)-1:
			colone[ligne],colone[ligne+1] = colone[ligne+1],colone[ligne]
			t = 0
			for ligne_saisi in colone:
				ligne_saisi.replacer(t)
				t += 1
				createFileBu.pack(pady=0+(len(colone)-1)*27)
	return func			
def copie_ligne(ligne):
	global colone,createFileBu,frame,copier
	def func():
		global colone,createFileBu,frame,copier,copie_ligne
		
		if ligne == len(colone)-1:
			colone.append(None)
			
		tmp = colone[ligne+1]
		colone[ligne+1] = ClnL.nligne((len(colone)),frame,supre_colone_select,up_ligne,down_ligne,copie_ligne)
		
		colone[ligne+1].sette(*colone[ligne].give_element())
		
		for l in range(ligne+2,len(colone)):
			colone[l],tmp = tmp,colone[l]
		
		if tmp != None:
			colone.append(tmp)
		
		t = 0
		for ligne_saisi in colone:
			ligne_saisi.replacer(t)
			t += 1
			createFileBu.pack(pady=0+(len(colone)-1)*27)
	
	return func
#==================================================================================================================================================
#                                                                 gestion graphique
#==================================================================================================================================================
#questionair de la avoir
def jour_view():
	global date
	def print_sel(e):
		print(date.get_date()) 
	date = DateEntry(fen5, width=10, background='darkblue',
					foreground='white', borderwidth=2,font = ("arial", 12))
	date.place(x=90,y=90)
	date.bind("<<DateEntrySelected>>", print_sel)
def setFenaitre(avoir,sett_bis):
	global affai, nom,rue,rue2,codepos,ville, objet,objet_suite, entier, ref,frame,labelnu
	global desig,qte,remi,uht,tht,netht,acompte,createFileBu,colone,Conpteur,var1_banck,pro_check,close_check,garantie_check,select
	
	fen4 = LabelFrame(avoir,text='Option impression avoir',fg='blue',font=("Times",14))
	fen4.place(x=10, y=5,height=140,width=240)
	#DevFacAvo.obtion_impression(fen4)
	
	var1 = IntVar()
	var1_banck = 0
	def ada():
		global var1_banck
		var1_banck = var1.get()
	Checkbutton(fen4, text=" Iban", variable=var1,relief=RAISED,font ='("Arial 11',anchor="w",width=16,command=ada).grid(row=0,column=0,padx=2,ipadx=25,pady=1)
	
	var3 = IntVar()
	close_check= 0
	def adb():
		global close_check
		close_check = var3.get()
	Checkbutton(fen4, text=" Pénalité", variable=var3,relief=RAISED,font ='("Arial 11',anchor="w",width=16,command=adb).grid(row=1,column=0,padx=2,ipadx=25,pady=1)
	
	var4 = IntVar()
	pro_check= 0
	def adc():
		global pro_check
		pro_check = var4.get()
	Checkbutton(fen4, text="Edition professionnelle", variable=var4,relief=RAISED,font ='("Arial 11',anchor="w",width=16,command=adc).grid(row=2,column=0,padx=2,ipadx=25,pady=1)
	
	Eche = IntVar()
	def action(event):
		global select
		select= Eche.get()
	Echeance = open(chem_note +'echeances',"r").readlines()
	for t in range(len(Echeance)):
		Echeance[t] = Echeance[t][:len(Echeance[t])-1]
	Eche = ttk.Combobox(fen4,values = Echeance,state='readonly',width=25,font=("calibri",10),textvariable=Echeance) #state="readonly"
	Eche.current(0)
	Eche.bind('<<ComboboxSelected>>',action)
	Eche.grid(row=4,column=0,padx=2,pady=2,ipadx=1,sticky='n') 	
	select= Eche.get()
	
	#DevFacAvo.affiche_carte(fram4)
	#Affaire
	DevFacAvo.labelfacdevavohaut(fen5)
	affai = tkinter.Entry(fen5,font = arial)
	affai.place(x=90, y=2,height=28,width=200)
	objet = tkinter.Entry(fen5,font = (arial))
	objet.place(x=90, y=31,height=30,width=200)
	objet_suite = tkinter.Entry(fen5,font = (arial))
	objet_suite.place(x=90, y=61,height=28,width=200)
	listecredit = nom_Tier()
	validate = (fen5.register(_validate), '%P')    
	nom = ttk.Combobox(fen5, values = listecredit,font = (arial), validatecommand=validate,validate='key')
	nom.bind('<<ComboboxSelected>>', select_Tier)
	nom.place(x=420, y=2,height=28,width=250)
	rue = tkinter.Entry(fen5,font = (arial))
	rue.place(x=420, y=31,height=28,width=250)
	rue2 = tkinter.Entry(fen5,font = (arial))
	rue2.place(x=420, y=60,height=28,width=250)
	codepos = tkinter.Entry(fen5,width=6,font = (arial))
	codepos.place(x=420, y=90,height=28,width=50)
	ville = tkinter.Entry(fen5,font =("Times",15)) 
	ville.place(x=470, y=90,height=28,width=200)
	barre = Label(avoir)
	barre.place(x=2, y=154,height=25,width=1018)
	DevFacAvo.bande(barre) 
	Conpteur= tkinter.Label(barre,text='Ligne'+"   ",fg = "green",bg= 'antiquewhite',width=7,relief = RAISED,font = ("Times", 12, "bold"))
	Conpteur.place(x=0, y=0,width=55,height=22)
def nom_Tier():
	return list(saveLoadTier.load_to_list())
def _validate(s):
	nv = [ x for x in nom_Tier() if x.startswith(s) ]
	nom['values'] = nv
	return True
def select_Tier(x):
	global nom,rue,rue2,codepos,ville
	Tier = saveLoadTier.load_to_list()[nom.get()]
	"""
	{"nom" : "Duran Endree",
	"rue" : "6 rue des Charbon",
	"rue2" : "",
	"codePostal" : "79000",
	"ville" : "Niort",
	"mobil" : "0606010203",
	"fix" : "0906",
	"email" : "trefle@free.fr"}
	"""
	rue.delete(0,END)
	rue.insert(0, (str(Tier['rue'])))
	rue2.delete(0,END)
	rue2.insert(0, (str(Tier['rue2'])))
	codepos.delete(0,END)
	codepos.insert(0, (str(Tier['codePostal'])))
	ville.delete(0,END)
	ville.insert(0, (str(Tier['ville'])))
	#crées les bouton
def Voire_avoir_en_couros():
	try:
		avoir.wm_attributes('-topmost',False)
		voire = os.system('xdg-open '+liste_avoir+ entier_AVO+'_'+nom.get().replace(" ", "")+'.pdf')    
		f = open(voire)
	except:
		messagebox.showerror("Erreure","Pas de saisie en cours ",parent=avoir)
		pass
	
def sortie_Avo(sett_bis):
	def func():
		avoir.destroy()
		sett_bis()
	return func
def setBotun(avoir,sett_bis):
	bt2 = Label(avoir,bg =ligne[3],relief=RAISED)
	bt2.place(x=2, y=490,height=108,width=380)
	bouton = Button(bt2,text="Total avoir",bg ="green",fg = "#FFFFFF",width=16,font =("arial", 14),command=total)
	bouton.grid(sticky="nw")
	bouton2 = Button(bt2,text="sauvegarde avoir",bg ="green",fg = "#FFFFFF",width=16,font =("arial", 14),command=Entier)
	bouton2.grid(row=1,sticky="nw")
	bouton3 = Button(bt2,text="export PDF",bg ="green",fg = "#FFFFFF",width=16,font =("arial", 14),command=Voire_avoir_en_couros)
	bouton3.grid(row=2,sticky="nw")
	bouton4 = Button(bt2,text="Nouveau avoir",width=16,font =("arial", 14),command=reset)
	bouton4.grid(row=0,column=1,sticky="nw")
	bouton5 = Button(bt2,text="Modifier un avoir",width=16,font =("arial", 14),command=openFile_avoir)
	bouton5.grid(row=1,column=1,sticky="nw")
	bouton8 = Button(bt2,text='Aide',width=7,font = ("arial", 14),command=aide_facdevavo)
	bouton8.grid(row=2,column=1,sticky="nw")
	bouton9 = Button(bt2,text='Sortie',bg ="brown",fg = "#FFFFFF",width=6,font = ("arial", 14),command=sortie_Avo(sett_bis))
	bouton9.grid(row=2,column=1,ipadx=7,padx=92,sticky="nw")
	
def del_Poupmenu ():
	showMenu.destroyer()
def tk_popup( x, y, entry=""): 
	call('tk_popup', self._w, x, y, entry) 
def couper():
	global avoir
	avoir.clipboard_clear()
	a=avoir.focus_get().index(ANCHOR)
	i=avoir.focus_get().index(INSERT)
	if i<a:a,i=i,a
	t=avoir.focus_get().get()
	s=t[a:i]
	t=t[0:a] + t[i:]
	avoir.focus_get().delete(0,END)
	avoir.focus_get().insert(0, t)
	avoir.focus_get().icursor(a)
	avoir.clipboard_append(s)
def copier():
	avoir.clipboard_clear()
	avoir.clipboard_append(avoir.selection_get())
def coller():
	t=avoir.selection_get(selection='CLIPBOARD')
	avoir.focus_get().insert(INSERT,t)
def Poupmenu ():
	def showMenu(e):
		pmenu.post(e.x_root, e.y_root)
	pmenu = tkinter.Menu(avoir, fg = "black",font = ("Arial",10), tearoff=0)
	pmenu.add_command(label="Copier", command=copier)
	pmenu.add_command(label="Coller",command=coller)
	pmenu.add_command(label="Effacer",command=couper)
	pmenu.add_command(label="Exit", command=avoir.destroy)
	avoir.bind("<Button-3>", showMenu)
def Saisie():
	avoir = SaisieClass()
def resize_image(event): 
	new_width = event.width 
	new_height = event.height 
	image = copy_of_image.resize((new_width, new_height)) 
	photo = ImageTk.PhotoImage(image) 
	label.config(image = photo) 
	label.image = photo 
#=======================================================================
# FONCTION PRINCIPAL
#=======================================================================
def main(sett_bis,fenètrePrimère,ouvrireFile = False):
	global affai, nom,rue,rue2,codepos,ville,objet,objet_suite, entier_AVO,ref,frame,labelnu, date,barre,fen2,copy_of_image,label
	global desig, qte, remi,uht,tht,netht,sous_tt,acompte,createFileBu,colone,avoir,frame,ligne,Totot,fen5,var1_banck,pro_check,select
	avoir = tkinter.Toplevel(fenètrePrimère)
	entier_AVO = lib.NumeroClass.trouveNumAvo()
	try:
		avoir.configure(bg=ligne[3])
	except :
		messagebox.showinfo("Information","Couleur blanc par defaut\n Probléme voire configuration",parent=avoir) 
		avoir.configure(bg='#FFFFFF')
		ligne[3]='#FFFFFF'
	try:
		image = Image.open(chem_parametre +'Font_ecran/'+ligne[53]) 
		copy_of_image = image.copy() 
		photo = ImageTk.PhotoImage(image) 
		label = ttk.Label(avoir,image = photo) 
		label.bind('<Configure>',resize_image) 
		label.pack(side = BOTTOM ,fill=BOTH, expand = YES)	
	except:
		pass
	try:
		x = fenètrePrimère.winfo_x()
		y = fenètrePrimère.winfo_y()
		w = int(ligne[4].split('x',1)[0]) 
		h = int(ligne[4].split('x',1)[1].split('+',1)[0])
		if w != 1040:
			w=1040
		if h != 600:
			h=600
		dx = ligne[20] #80
		dy = ligne[21]
		avoir.geometry("%dx%d+%d+%d" % (w, h, x + int(str(dx)), y + int(str(dy))))
	except:
		messagebox.showinfo("Information","Dimention par defaut\n Probléme voire configuration",parent=avoir)  
		avoir.geometry('1029x600')
		ligne[4] = '1029x600'	
	#avoir.bind('<Escape>',lambda event:avoir.destroy())
	#avoir.protocol('WM_DELETE_WINDOW', lambda: None)
	#avoir.wm_attributes('-topmost', True)
	avoir.transient(fenètrePrimère)
	avoir.wm_attributes('-type','splash')
	#avoir.overrideredirect(True)
	fen5 = Label(avoir,relief=RAISED)
	fen5.place(x=335, y=2,height=160,width=685)
	##########################
	Poupmenu ()
	#========================================================================================
	#		Crées le questionair de factur
	#========================================================================================
	setFenaitre(avoir,sett_bis)
	############### Enplacement
	jour_view() # Date
	NumeroAvoir()
	######################### boucle
	frameq = Label(avoir,bg ="White")
	frameq.place(x=4, y=174,width=1030, height=300)
	frame = SimpleScrolledFrame(frameq )		
	frame.pack(fill=BOTH, expand=True)

	createFileBu=Button(frame,width=4,text='N/Ligne',font = ("Times",9),bg ="white", fg = "green",command = ad_colone)
	createFileBu.pack(side=LEFT)
	###################################### Boutton
	cadre_total = Label(avoir)#lightgray')#,relief = RAISED)
	cadre_total.place(x=700, y=485,height=113,width=289)
	label=tkinter.Label(cadre_total,text="Sous total ",font=('Helvetica 14'),relief=RAISED,anchor=W).place(x=1,y=1,width=165,height=35)
	sous_tt = tkinter.Label(cadre_total,width=8,bg ='#F6F6D0',relief=RAISED,font=('Helvetica 14'))
	sous_tt.place(x=168,y=1,width=115,height=35)

	Label(cadre_total,text="Acompte ",font = ('Helvetica 14'),relief=RAISED,anchor=W).place(x=1,y=37,width=165,height=35)
	acompte = tkinter.Entry(cadre_total,justify=CENTER,width=8,font=('Helvetica 14'),relief=RAISED)
	acompte.place(x=168,y=37,width=115,height=35)
	acompte.insert(END,"0.0")
			
	Label(cadre_total,text="PRIX GLOBAL",font=('Helvetica 14'),relief=RAISED,anchor=W).place(x=1,y=74,width=165,height=35)
	netht = tkinter.Label(cadre_total,justify=CENTER,width=8,bg ='#F6F6D0',relief=RAISED,font =('Helvetica 14'))
	netht.place(x=168,y=74,width=115,height=35)
	setBotun(avoir,sett_bis)
	colone = [ClnL.nligne(0,frame,supre_colone_select,up_ligne,down_ligne,copie_ligne)]
	createFileBu.pack(pady=0 + (len(colone)-1)*27)   
	if ouvrireFile:
		openFile()
	avoir.mainloop()
if __name__ == '__main__':
	f = Tk()
	main(f)
	f.mainloop()
