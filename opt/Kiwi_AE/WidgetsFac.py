# coding: utf-8
import tkinter
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkinter.filedialog import askopenfilename
from tkinter import Tk, Button, filedialog, LabelFrame
from tkinter import Tk, Toplevel, Button
import subprocess

from PIL import Image
import sqlite3
from lib.libAuxilaire import *
from lib.libExplore import *
import lib.DevFacAvo as DevFacAvo
import lib.NumeroClass
import lib.class_nline as ClnL
import lib.facture as genre_facture
import lib_pdf.facture_PDF as facture_PDF
from Config  import *
import saveLoadTier
entier = lib.NumeroClass.trouveNumFac()

#==================================================================================================================================================
#                                                                 class
#==================================================================================================================================================
class affai():
	def __init__(self,cord,text,frame):
		self.cord = cordonné()
		print(cord)
		self.cord.x_text , self.cord.y_text = cord[0]
		self.cord.x_saisi , self.cord.y_saisi = cord[1]
		self.label = Label(frame,tcoucouCestMoisext=text)
		self.entry = Entry(frame)
		self.label.place(x = self.cord.x_text, y = self.cord.y_text)
		self.entry.place(x = self.cord.x_saisi, y = self.cord.y_saisi)
	def get(self):
		return self.entry.get()
#gestion des cordonné
class cordonné():
	def __init__(self):
		self.x_text = 0
		self.y_text = 0
		self.x_saisi = 0
		self.y_saisi = 0
#==================================================================================================================================================
#                                                                 autre
#==================================================================================================================================================
#trouver non fichier
def Entier():
	global affai, nom,rue,rue2,codepos,ville, objet,objet_suite, colone, netht,acompte,date,entier
	global factur,frame,var1_banck,close_check,pro_check,select
	lien = (liste_facture_brut + str(entier))# + ".dat")
	try:
		genre_facture.main(lien,affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date, colone, acompte, netht,entier,select)
	except:	
		messagebox.showerror("Erreure","Facture passer en comptabilité\n soldé nom modifiable",parent=factur)
		pass
	update_stock(Nnom)
	forme_PDF()
def update_stock(Nnom):
	global colone,qt
	conn = sqlite3.connect(liste_stock)
	for line in colone:
		ref = line.ref.get()
		qt = int(float(line.qte.get()))
		cursor = conn.cursor()
		cursor.execute("UPDATE REGISTRE SET QTE = QTE-? WHERE REFERENCE LIKE ?;",(qt,ref))
		conn.commit()
	conn.close()
#lire un PDF
def forme_PDF():
	global affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date, entier, colone, netht, acompte,date
	global ffacturen,frame,var1_banck,close_check,pro_check,select,bt3
	lien = (liste_facture)+'/'+ str(entier)# 
	facture_PDF.main(lien,affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date,entier,colone, netht, acompte,var1_banck,close_check,pro_check,select)
def jour_view():
	global date
	def print_sel(e):
		print(date.get_date())
	date = DateEntry(fen5, width=10, background='darkblue',
					foreground='white',font = ("arial", 13), borderwidth=2)
	date.place(x=90,y=93)
	date.bind("<<DateEntrySelected>>", print_sel)
def NumeroFacture():
	global entier, labelnu
	labelnu = tkinter.Label(fen5,text= entier ,relief=SUNKEN,bg ='#F6F6D0', fg = "black",font = ("arial", 13), bd = 2) # Numero de Facture
	labelnu.place(x=430,y=120,width=250) 
	label = tkinter.Label(fen5,text="Facture N°",fg = "blue",font = ("arial", 13, "italic"))
	label.place(x=300,y=120)
def NumeroDevis():
	global entier_dev, labelnu
	label_av = tkinter.Label(fen5,text=" Devis N° ",fg = "blue",font = ("arial", 13),bd = 2)
	label_av.place(x=2,y=120)
	labelnu = tkinter.Label(fen5,text= entier_dev ,bg ="white", fg = "black",font = ("arial", 12, "italic"), bd = 2) # Numero de Facture
	labelnu.place(x=90,y=120,width=200) 
def NumeroAvoir():
	global entier_AVO,labelnu,label_av
	label_av=tkinter.Label(fen5,text="Avoir N° ",fg = "blue",font = ("arial", 13, "italic"), bd = 2)
	label_av.place(x=2,y=120)
	labelnu = tkinter.Label(fen5,text= entier_AVO,relief=SUNKEN ,bg ="white", fg = "black",font = ("arial", 13, "italic"), bd = 2) # Numero de Facture
	labelnu.place(x=90,y=120,width=200) 
def openFile_facture():
	def demanderLien(Open = False):
		if not Open:
			return tkinter.filedialog.asksaveasfilename(parent=factur,filetypes =[("Dat Files","FAC**")])
		else:
			return tkinter.filedialog.askopenfilename(parent=factur,initialdir=racine_Facture_brut,
														title='Sélection Facture',
														filetypes =[("Dat Files","FAC**")]) 
	global affai,nom,rue,rue2,codepos,ville, objet,objet_suite,colone,entier,date,ref, netht, acompte,createFileBu,factur,supre_colone_select,frame,ligne
	entier = genre_facture.openf(demanderLien(Open = True),affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date,acompte,netht, entier,colone, createFileBu,factur,supre_colone_select,frame,labelnu,ligne)#,ad_colone)
	NumeroFacture()
def openFile_devis():
	def demanderLien_devis(Open = False):
		if not Open:
			return tkinter.filedialog.asksaveasfilename(parent=factur,filetypes = [("Dat Files","DEV**")]) 
		else:
			return tkinter.filedialog.askopenfilename(parent=factur,initialdir = racine_Devis_brut,
														title='Sélection Devis',
														filetypes =[("Dat Files","DEV**")])
	global affai,nom,rue,rue2,codepos,ville, objet,objet_suite,colone,entier,entier_dev,date,ref, netht, acompte,createFileBu,factur,supre_colone_select,frame,labelnu,ligne
	entier_dev = genre_facture.openf(demanderLien_devis(Open = True),affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date,acompte,netht, entier,colone, createFileBu,factur,supre_colone_select,frame,labelnu,ligne)#,ad_colone)
	NumeroDevis()
def openFile_avoir():
	def demanderLien_avoir(Open = False):
		if not Open:
			return tkinter.filedialog.asksaveasfilename(parent=factur,filetypes =[("Dat Files","AVO**")]) 
		else:
			return tkinter.filedialog.askopenfilename(parent=factur,initialdir=racine_Avoir_brut,
														title='Sélection Avoir',
														filetypes =[("Dat Files","AVO**")])
	global affai,nom,rue,rue2,codepos,ville, objet,objet_suite,colone,entier,entier_AVO,date,ref, netht, acompte,createFileBu,factur,supre_colone_select,frame,labelnu,ligne
	entier_AVO = genre_facture.openf(demanderLien_avoir(Open = True),affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date,acompte,netht, entier,colone, createFileBu,factur,supre_colone_select,frame,labelnu,ligne)#,ad_colone)
	NumeroAvoir()
def aide_facdevavo():
	try:
		factur.wm_attributes('-topmost',False)
		os.system('xdg-open '+ chem_aide +'Guide_fac.pdf')
	except:
		messagebox.showerror("Erreure","Pas de fichier ",parent=factur)		
		pass 
#Reset tout les Empty	
def reset():
	global  entier,entier_dev,labelnu,label_av
	affai.delete(0,END)
	nom.delete(0,END)
	rue.delete(0,END)
	rue2.delete(0,END)
	codepos.delete(0,END)
	ville.delete(0,END)
	objet.delete(0,END)
	objet_suite.delete(0,END)
	labelnu.config(text="")
	netht.delete(0,END)
	sous_tt.config(text="")
	acompte.delete(0,END)
	#labelnu.config(text="")
	label_av.delete(0,END)
	Reset_colone()
	entier = lib.NumeroClass.trouveNumFac()
	labelnu.config(text= entier)  
#calcul des totaux                              
def total():
	global tht,acompte,netht
	global colone,sous_tt
	sous_tt_ = 0
	sous_tt_ = float(sous_tt_)
	for x in colone:
		sous_tt_ += x.calcul()
	val = acompte.get()
	if val == '':
		val = '0'
	nethtf= float(float(sous_tt_) - float(val))
	if nethtf <= 0:
		messagebox.showerror("Erreure","Nombre négatif sur\nstock ou modifier \nles Quantité",parent=factur) 
	else:
		netht.config(text = nethtf)
		sous_tt.config(text = str(sous_tt_))
#==================================================================================================================================================
#                                                                 gestion colone
#==================================================================================================================================================
#ajouté une colonne
def actualisationConteur():
	global Conpteur, colone
	Conpteur.config(text=len(colone))
def ad_colone():
	global colone,createFileBu,frame,ligne
	colone.append(ClnL.nligne((len(colone)),frame,supre_colone_select,up_ligne,down_ligne,copie_ligne))
	createFileBu.pack(pady=0+(len(colone)-1)*27)
	actualisationConteur()
#suprimer une colonne
def supre_colone_fin():
	global colone,createFileBu,frame
	supre_colone_select(len(colone)-1,frame)()
	actualisationConteur()
#reset tout les colonne
def Reset_colone():
	global colone,createFileBu,frame
	for ligne in range(len(colone)):
		colone[0].delette()
		del colone[0]
	createFileBu.pack()
	ad_colone()
	actualisationConteur()
#suprimer un colonne selectioner
def supre_colone_select(ligne):
	global colone,createFileBu,frame
	def supre_colone():
		global colone,createFileBu,frame
		if len(colone) <= 1:
			return True
		colone[ligne].delette() 
		del colone[ligne]
		t = 0
		for ligne_saisi in colone:
			ligne_saisi.replacer(t)
			t += 1
			createFileBu.pack(pady=0+(len(colone)-1)*27)
		actualisationConteur()
	return supre_colone
def  basup_ligne(ligne):
	global colone,createFileBu,frame
	if ligne != 0:
		colone[ligne],colone[ligne-1] = colone[ligne-1],colone[ligne]
		t = 0
		for ligne_saisi in colone:
			ligne_saisi.replacer(t)
			t += 1
			createFileBu.pack(pady=0+(len(colone)-1)*27)
def up_ligne(ligne):
	global colone,createFileBu,frame
	def func():
		global colone,createFileBu,frame
		if ligne != 0:
			colone[ligne],colone[ligne-1] = colone[ligne-1],colone[ligne]
			t = 0
			for ligne_saisi in colone:
				ligne_saisi.replacer(t)
				t += 1
				createFileBu.pack(pady=0+(len(colone)-1)*27)
	return func
def down_ligne(ligne):
	global colone ,createFileBu,frame
	def func():
		if ligne != len(colone)-1:
			colone[ligne],colone[ligne+1] = colone[ligne+1],colone[ligne]
			t = 0
			for ligne_saisi in colone:
				ligne_saisi.replacer(t)
				t += 1
				createFileBu.pack(pady=0+(len(colone)-1)*27)
	return func
def copie_ligne(ligne):
	global colone,createFileBu,frame,copier
	def func():
		global colone,createFileBu,frame,copier
		if ligne == len(colone)-1:
			colone.append(None)
		tmp = colone[ligne+1]
		colone[ligne+1] = ClnL.nligne((len(colone)),frame,supre_colone_select,up_ligne,down_ligne,copie_ligne)
		colone[ligne+1].sette(*colone[ligne].give_element())
		for l in range(ligne+2,len(colone)):
			colone[l],tmp = tmp,colone[l]
		if tmp != None:
			colone.append(tmp)
		t = 0
		for ligne_saisi in colone:
			ligne_saisi.replacer(t)
			t += 1
			createFileBu.pack(pady=0+(len(colone)-1)*27)
		print(ligne,"dupliced")
		print(colone)
	return func
def texte():
	global colone,createFileBu,frame
	pass
#==================================================================================================================================================
#                                                                 gestion graphique
#==================================================================================================================================================
#questionair de la facture
def setFenaitre(factur,sett_bis):
	global affai, nom,rue,rue2,codepos,ville, objet,objet_suite, entier, ref,frame,labelnu,on_closing,fenetre,label_av
	global desig,qte,remi,uht,tht, netht, acompte,sous_tt, createFileBu, colone,frame,exef,Conpteur,var1_banck,close_check,pro_check,select
	fen4 = LabelFrame(factur,text='Option impression facture',fg='blue',font=("Times",14))
	fen4.place(x=10, y=1,height=140,width=240)
	#DevFacAvo.obtion_impression(fen4)
	var1 = IntVar()
	var1_banck = 0
	def ada():
		global var1_banck
		var1_banck = var1.get()
	Checkbutton(fen4, text=" Iban", variable=var1,relief=RAISED,font ='("Arial 11',anchor="w",width=16,command=ada).grid(row=0,column=0,padx=2,ipadx=25,ipady=1)
	var3 = IntVar()
	close_check= 0
	def adb():
		global close_check
		close_check = var3.get()
	Checkbutton(fen4, text=" Pénalité", variable=var3,relief=RAISED,font ='("Arial 11',anchor="w",width=16,command=adb).grid(row=1,column=0,padx=2,ipadx=25,ipady=1)
	
	var4 = IntVar()
	pro_check= 0
	def adc():
		global pro_check
		pro_check = var4.get()
	Checkbutton(fen4, text="Edition professionnelle", variable=var4,relief=RAISED,font ='("Arial 11',anchor="w",width=16,command=adc).grid(row=3,column=0,padx=2,ipadx=25,ipady=1)
	
	Eche = IntVar()
	def action(event):
		global select
		select= Eche.get()
	Echeance = open(chem_note + 'echeances',"r").readlines()
	for t in range(len(Echeance)):
		Echeance[t] = Echeance[t][:len(Echeance[t])-1]
	Eche = ttk.Combobox(fen4,values = Echeance,state='readonly',width=25,font=("calibri",10),textvariable=Echeance) #state="readonly"
	Eche.current(0)
	Eche.bind('<<ComboboxSelected>>',action)
	Eche.grid(row=4,column=0,padx=2,pady=2,ipadx=1,sticky='n')	
	select= Eche.get()

	#DevFacAvo.affiche_carte(fram4)	
	DevFacAvo.labelfacdevavohaut(fen5)
	
	affai = tkinter.Entry(fen5,font =("Times",14))
	affai.place(x=90, y=2,height=28,width=200)
	objet = tkinter.Entry(fen5,font =("Times",14))
	objet.place(x=90, y=31,height=30,width=200)
	objet_suite = tkinter.Entry(fen5,font = (arial))
	objet_suite.place(x=90, y=61,height=28,width=200)
	listecredit = nom_Tier()
	validate = (factur.register(_validate), '%P')    
	nom = ttk.Combobox(fen5, values = listecredit,font=("Times",14),validatecommand=validate,validate='key')
	nom.bind('<<ComboboxSelected>>', select_Tier)
	nom.place(x=430, y=2,height=28,width=250) 
	rue = tkinter.Entry(fen5,font = ("Times",14))
	rue.place(x=430, y=31,height=28,width=250)
	rue2 = tkinter.Entry(fen5,font =("Times",14))
	rue2.place(x=430, y=60,height=28,width=250)
	codepos = tkinter.Entry(fen5,width=5,font = ("Times",14))
	codepos.place(x=430, y=90,height=28,width=50)
	ville = tkinter.Entry(fen5,font = ("Times",14))
	ville.place(x=480, y=90,height=28,width=200)
	barre = Label(factur)
	barre.place(x=2, y=154,height=25,width=1018)
	DevFacAvo.bande(barre)
	Conpteur= tkinter.Label(barre,text='Ligne'+"   ",fg = "green",bg= 'antiquewhite',relief = RAISED,width=7,font = ("Times", 11))
	Conpteur.place(x=0, y=0,width=55,height=22)
def nom_Tier():
	return list(saveLoadTier.load_to_list())
def _validate(s):
	nv = [ x for x in nom_Tier() if x.startswith(s) ]
	nom['values'] = nv
	return True
def select_Tier(x):
	global nom,rue,rue2,codepos,ville
	Tier = saveLoadTier.load_to_list()[nom.get()]
	"""
	{"nom" : "Duran Endree",
	"rue" : "6 rue des Charbon",
	"rue2" : "",
	"codePostal" : "79000",
	"ville" : "Niort",
	"mobil" : "0606010203",
	"fix" : "0906",
	"email" : "trefle@free.fr"}
	"""
	rue.delete(0,END)
	rue.insert(0, (str(Tier['rue'])))
	rue2.delete(0,END)
	rue2.insert(0, (str(Tier['rue2'])))
	codepos.delete(0,END)
	codepos.insert(0, (str(Tier['codePostal'])))
	ville.delete(0,END)
	ville.insert(0, (str(Tier['ville'])))
	#crées les bouton
def Voire_facture_en_couros():
	voire = os.popen('xdg-open '+liste_facture       + entier+'_'+nom.get().replace(" ", "")+'.pdf')
def sortie_Fac(sett_bis):
	def func():
		factur.destroy()
		sett_bis()
	return func
def set_Boutun(factur,sett_bis):
	bt2 = Label(factur,bg =ligne[2])#,relief=RAISED)
	bt2.place(x=2, y=490,height=118,width=587)
	bouton = Button(bt2,text="Total Facture",bg ="green",fg = "#FFFFFF",width=16,font =("arial", 14),command=total)
	bouton.grid(sticky="nw")
	bouton2 = Button(bt2,text="sauvegarde Facture",bg ="green",fg = "#FFFFFF",width=16,font =("arial", 14),command=Entier)
	bouton2.grid(row=1,column=0,sticky="nw")
	bouton3 = Button(bt2,text="export PDF",bg ="green",fg = "#FFFFFF",width=16,font =("arial", 14),command=Voire_facture_en_couros)
	bouton3.grid(row=2,column=0,sticky="nw")
	bouton4 = Button(bt2,text="Nouvelle Facture",fg = "#000000",width=17,font =("arial", 14),command=reset)
	bouton4.grid(row=0,column=1,ipadx=3,sticky="nw")
	bouton5 = Button(bt2,text="Modifier une Facture",fg = "#000000",width=17,font =("arial", 14),command=openFile_facture)
	bouton5.grid(row=1,column=1,ipadx=3,sticky="nw")
	bouton6 = Button(bt2,text='Exporter Devis',fg = "#000000",width=17,font=("arial", 14),command=openFile_devis)
	bouton6.grid(row=2,column=1,ipadx=3,sticky="nw")
	bouton7 = Button(bt2,text='Exporter Avoir',fg = "#000000",width=16,font = ("arial",14),command=openFile_avoir)
	bouton7.grid(row=0,column=2,ipady=0,sticky="nw")
	bouton8 = Button(bt2,text='Aide',fg = "#000000",width=16,font = ("arial", 14),command=aide_facdevavo)
	bouton8.grid(row=1,column=2,sticky="nw")
	bouton9 = Button(bt2,text='Sortie Facture',bg ="brown",fg = "#FFFFFF",width=16,font = ("arial", 14, "bold"),command=sortie_Fac(sett_bis))
	bouton9.grid(row=2,column=2,sticky="nw")
def del_Poupmenu ():
	showMenu.destroyer()
#def tk_popup( x, y, entry=""): 
	call('tk_popup', self._w, x, y, entry) 
def copier():
	factur.clipboard_clear()
	factur.clipboard_append(factur.selection_get())
def coller():
	t=factur.selection_get(selection='CLIPBOARD')
	factur.focus_get().insert(INSERT,t)
def Poupmenu ():
	global factur
	def showMenu(e):
		pmenu.post(e.x_root, e.y_root)	
	pmenu = tkinter.Menu(factur, fg = "black",font = ("Arial",13), tearoff=0)
	pmenu.add_command(label="Copier", command=copier)
	pmenu.add_command(label="Coller",command=coller)
	pmenu.add_command(label="Exit", command=factur.destroy)
	factur.bind("<Button-3>", showMenu)
def resize_image(event): 
	new_width = event.width 
	new_height = event.height 
	image = copy_of_image.resize((new_width, new_height)) 
	photo = ImageTk.PhotoImage(image) 
	label.config(image = photo) 
	label.image = photo 
def fenetre_affiche():
	pass
#=======================================================================
# FONCTION PRINCIPAL
#=======================================================================
def main(sett_bis,fenètrePrimère,ouvrireFile = False):
	global affai, nom,rue,rue2,codepos,ville,objet,objet_suite, entier,ref,frame,labelnu, date,saisie,fen2,bt3,var1_banck,close_check,pro_check,select,fenetre
	global desig, qte, remi,uht,tht,netht,acompte,sous_tt,createFileBu,colone,factur,frame,ligne,fen5,cadre_total,copy_of_image,image,label
	factur = tkinter.Toplevel(fenètrePrimère)
	factur.title(" Menu facture")
	icone = PhotoImage(file='/opt/Kiwi_AE/Icon/Client.png')
	factur.iconphoto(True, icone)
	#factur.bind('<Escape>',  lambda event:factur.destroy())
	entier = lib.NumeroClass.trouveNumFac()
	
	try:
		factur.configure(bg=ligne[2])
	except :
		messagebox.showinfo("Information","Couleur blanc par defaut Probléme voire configuration",parent=factur)  
		factur.configure(bg='#FFFFFF')
		ligne[2]='#FFFFFF'	
	try:
		image = Image.open(chem_parametre +'Font_ecran/'+ligne[53]) 
		copy_of_image = image.copy() 
		photo = ImageTk.PhotoImage(image) 
		label = ttk.Label(factur,image = photo) 
		label.bind('<Configure>',resize_image) 
		label.pack(side = BOTTOM ,fill=BOTH, expand = YES)	
	except:
		pass
	try:
		x = fenètrePrimère.winfo_x()
		y = fenètrePrimère.winfo_y()
		w = int(ligne[4].split('x',1)[0]) 
		h = int(ligne[4].split('x',1)[1].split('+',1)[0])
		if w != 1040:
			w=1040
		if h != 600:
			h=600
		dx = ligne[20] #80
		dy = ligne[21]
		factur.geometry("%dx%d+%d+%d" % (w, h, x + int(str(dx)), y + int(str(dy))))
	except:
		messagebox.showinfo("Information","Dimention par defaut\n Probléme voire configuration",parent=factur)  
		factur.geometry('1050x600')
		ligne[4] = '1050x600'
		factur.geometry(ligne[4])
	factur = factur
	#factur.protocol('WM_DELETE_WINDOW', lambda: None)
	#factur.wm_attributes("-topmost",True)
	factur.transient(fenètrePrimère)
	factur.wm_attributes('-type','splash')
	fen5 = Label(factur)#,relief=RAISED)
	fen5.place(x=335, y=2,height=160,width=685)
	Poupmenu()
	#========================================================================================
	#		Crées le questionair de factur
	#========================================================================================
	setFenaitre(factur,sett_bis)
	######## Combobox
	jour_view() # Date
	NumeroFacture()
	######################### boucle
	frameq = Label(factur,bg ="White")
	frameq.place(x=4, y=174, width=1030, height=300)
	frame = SimpleScrolledFrame(frameq )		
	frame.pack(fill=BOTH, expand=True) # fill window
	createFileBu=Button(frame,width=4,text='N/Ligne',font = ("Times",9),bg ="white", fg = "green",command = ad_colone)
	createFileBu.pack(side=LEFT)	
	###################################### Boutton
	cadre_total = Label(factur,bg =ligne[2])#lightgray')#,relief = RAISED)
	cadre_total.place(x=700, y=485,height=113,width=289)
	
	label=tkinter.Label(cadre_total,text="Sous total HT ",relief='raise', fg = "black",font=('arial 14'),anchor=W).place(x=1,y=1,width=165,height=32)
	sous_tt = tkinter.Label(cadre_total,width=8,bg ='#F6F6D0',font=('arial 14'),relief=RAISED)
	sous_tt.place(x=168,y=1,width=115,height=32)
	#print (type(sous_tt),'insert(END,')

	label = tkinter.Label(cadre_total,text="Acompte ",relief='raise', fg = "black",font = ('arial 14'),anchor=W).place(x=1,y=34,width=165,height=32)
	acompte = tkinter.Entry(cadre_total,justify=CENTER,relief='raise',width=8,bg ='#ffffff',font =('arial 14'))
	acompte.place(x=168,y=34,width=115,height=32)
	acompte.insert(END,"0.0")
			
	label = tkinter.Label(cadre_total,text="TOTAL HT",relief='raise',font=('arial 14'),anchor=W).place(x=1,y=68,width=165,height=32)
	netht = tkinter.Label(cadre_total,justify=CENTER,width=8,relief='raise',bg ='#F6F6D0',font =('arial 14'))
	netht.place(x=168,y=68,width=115,height=32)
	#file_message()
	################## Boutton de command
	set_Boutun(factur,sett_bis)
	colone = [ClnL.nligne(0,frame,supre_colone_select,up_ligne,down_ligne,copie_ligne)]
	createFileBu.pack(pady=0 +(len(colone)-1)*27)   
	#actualisationConteur()
	if ouvrireFile:
		openFile()
if __name__ == '__main__':
	f = Tk()
	main(f)
	f.mainloop()
