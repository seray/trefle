from tkinter import *
from math import *
import tkinter
import lireFacture
import class_nline as ClnL
import agenda as agenda
import os
from tkinter import messagebox 
from pathlib import Path

# Control fichier present
path = FILE_NAME 
if not os.path.exists(path) :
	messagebox.showinfo(title = "Probléme du Fichier", message = "Fichier /Agenda.dat'at\n absent ou erronée")
def lectur_date(donner):
	jour = int(donner["date"][0:2])
	mois = int(donner["date"][3:5])
	anné = int(donner["date"][6:])
	return (anné ,mois ,jour)
def recet_date(select,date,sauvegard):
	list_factur = date
	for x in select:
		if x in list_factur:
			list_factur = list_factur[x]
		else:
			return 0
	return aplatir(list_factur,sauvegard)
def aplatir(dictionaire,sauvegard):
	sortie = 0
	if type(dictionaire) == type({}):
		for key in dictionaire:
			sortie += aplatir(dictionaire[key],sauvegard)
	else:
		sortie += float(sauvegard[dictionaire]["Samedi"])
	return sortie
def clearSpecialCaracter(string):
	string=string.replace("é","e")
	string=string.replace("ä","a")
	string=string.replace("-",' ')
	string=string.replace("/","_")
	string=string.replace(">","_")
	string=string.replace('<','_')
	string=string.replace(":",";")
	return string
def save(colone):
	fileName = FILE_NAME
	print(fileName)
	data = open(fileName,"w")
	# ======== Tierh
	data.write("#zone_a_imprimer#\n")
	data.write("#end#\n")
	data.write("#zone_de_donner#\n")
	data.write("colone~[")
	
	for x in colone:
		date =       x.date.get()
		Lundi =     clearSpecialCaracter.x.Lundi.get()
		Mardi =     clearSpecialCaracter.x.Mardi.get()
		Mercredi =  clearSpecialCaracter.x.Mercredi.get()
		Jeudi =     clearSpecialCaracter.x.Jeudi.get()
		Vendredi =  clearSpecialCaracter.x.Vendredi.get()
		Samedi =    clearSpecialCaracter.x.Samedi.get()
						
		data.write('{')
		data.write('"date" : "'       + str(date)    + '",')
		
		data.write('"Lundi" : "'     + str(Lundi)  + '",')
		data.write('"Mardi" : "'        + str(Mardi)     + '",')
		data.write('"Mercredi" : "'     + str(Mercredi)  + '",')
		data.write('"Jeudi" : "'     + str(Jeudi)  + '",')
		data.write('"Vendredi" : "'     + str(Vendredi)  + '",')
		data.write('"Samedi" : "'      + str(Samedi)   + '",')
		data.write('},')
	data.write(']~\n')
	data.write("#end#\n")
	data = open(fileName,"r") # 
	print(data.read())
	data.close()
	
def load(colone,supre_colone_select,frame):
	text,a_apliqué = lireFacture.lireDat(FILE_NAME)
	for ligne in range(len(colone)):
		colone[len(colone)-1].delette()
		del colone[len(colone)-1]
	date = []
	for x in a_apliqué["colone"]:
		date.append(x["date"])
	for x in range(len(a_apliqué['colone'])):
		colone.append(ClnL.nligneRecette(len(colone),frame,supre_colone_select))
		colone[len(colone)-1].set(
			a_apliqué['colone'][x]['date'],
			a_apliqué['colone'][x]['Lundi'],
			a_apliqué['colone'][x]['Mardi'],
			a_apliqué['colone'][x]['Mercredi'],
			a_apliqué['colone'][x]['Jeudi'],
			a_apliqué['colone'][x]['Vendredi'],
			a_apliqué['colone'][x]['Samedi'])
def date_action():
	text,a_apliqué = lireFacture.lireDat(FILE_NAME)
	f = a_apliqué["colone"]
	arbre = {}
	t = 0
	for x in f:
		date = lectur_date(x)
		branche = arbre
		for element in date:
			if not(element in branche):
				branche[element] = {}
			branche = branche[element]
		branche[t] = t
		t += 1
	ans = int("20"+time.strftime("%y"))
	mois = int(time.strftime("%m"))
	reset_mois = recet_date([ans],arbre,f)
	reset_anne = recet_date([ans,mois],arbre,f)
	print(reset_anne,reset_mois,[ans,mois])
	return reset_anne,reset_mois
def set_leval_solde(labeldate):
	reset_anne,reset_mois = date_action()
	labeldate.set_solde(reset_mois,reset_anne)
	#print (reset_mois,' ',reset_anne)
def load_to_list():
	text,a_apliqué = lireFacture.lireDat(FILE_NAME)
	sortie = {}
	"""
	{"Date" : "Lundi",
	"Mardi" : "Mercredi",
	"Jeudi" : "Samedi",
	"""
	for Recette in a_apliqué['colone']:
		sortie[Recette["date"]] = Recette
	return sortie
		
#	app.mainloop()
