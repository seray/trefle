# coding: utf-8$
#from tkinter import *
from tkinter import *
import tkinter as tk
from tkinter import ttk
import sqlite3
import lib.fpdf
from random import choice
import os
import os, sys, stat
import locale
import time
from datetime import date
from PIL import Image
import csv
import Tier as Tier
import saveLoadTier
import lib_pdf.MoisPDF
from lib.libAuxilaire import *
from lib_pdf.MoisPDF import *
from Config  import *

Date_annee = time.strftime('%Y')
day_mois   = time.strftime('%m')
date_info_annee = time.strftime('%y')
date_info_mois = time.strftime('%b')
date_vue = time.strftime("%m-%Y")

#function to define database
def Database():
    global conn, cursor
    #print ('#creating contact database')
    conn = sqlite3.connect(dossier)
    cursor = conn.cursor()
    cursor.execute(
        "CREATE TABLE IF NOT EXISTS REGISTRE (RID INTEGER NOT NULL  PRIMARY KEY AUTOINCREMENT,GDATE DATETIME,FNAME TEXT, IDENTIFIANT TEXT,GENRE TEXT,NOTE TEXT, NATURE TEXT,BANK TEXT, LIBELLE TEXT, DEBIT REAL,CREDIT REAL,SERVICE REAL,VENTE REAL)")
def acount(bank):
    return acount_column("CREDIT-DEBIT",date_filter = False,bank = bank)
def acount_annee(bank):
    return acount_column("CREDIT-DEBIT",date_filter = True,bank = bank)
def acount_debit_mois():
    return acount_column("DEBIT",date_filter = True)
def acount_credit_mois():
    return acount_column("CREDIT",date_filter = True)
def acount_service_mois():
    return acount_column("SERVICE",date_filter = True)
def acount_vente_mois():
    return acount_column("VENTE",date_filter = True)
def acount_debit():
    return acount_column("DEBIT")
def acount_credit():
    return acount_column("CREDIT")
def acount_service():
    return acount_column("SERVICE")
def acount_vente():
    return acount_column("VENTE")
def acount_column(colone,date_filter = False,bank = None):
    list_colone = ["DEBIT","CREDIT","SERVICE","VENTE","CREDIT-DEBIT"]
    if not colone in list_colone:
        raise ValueError
    conn = sqlite3.connect(dossier)
    cur = conn.cursor()
    argument = []
    if date_filter:
        sql_text = "SELECT  SUM("+ colone +") FROM REGISTRE WHERE GDATE LIKE ?"
        argument = ["%" + "-" +day_mois+ "-" + Date_annee,]    
    else:
        sql_text = "SELECT SUM("+colone+") FROM REGISTRE"
        argument = []
    if bank != None:
        bank = bank.replace("\n","")
        if not date_filter:
            sql_text += " WHERE BANK = ?"
        else:
            sql_text += " AND BANK = ?"
        argument.append(bank)
    value = cur.execute(sql_text,(*argument,))
    value = value.fetchone()[0]
    conn.commit()
    conn.close()
    return value
def Button_Banck1():
    Database()
    tree.delete(*tree.get_children())
    b1 = ligne[23] .replace("\n","")
    cursor=conn.execute("SELECT * FROM REGISTRE WHERE BANK LIKE ?", (b1,))
    fetch = cursor.fetchall()
    for data in fetch:
        tree.insert('', 'end', values=(data))
    cursor.close()
    conn.close()
def Button_Banck2():
    Database()
    tree.delete(*tree.get_children())
    b2 = bank_2.replace("\n","")
    cursor=conn.execute("SELECT * FROM REGISTRE WHERE BANK LIKE ?", (b2,))
    fetch = cursor.fetchall()
    for data in fetch:
        tree.insert('', 'end', values=(data))
    cursor.close()
    conn.close()
def print_acount(func):
    global label_bank1,label_bank2
    global bank_1,bank_2,Menu_saisie 
    def run(*args,**kwargs):
        global label_bank1,label_bank2
        global label_debit_annee
        global bank_1,bank_2,val_1
        global tree,Menu_saisie
        #print (ligne[22],'ligne[22]')   # Credit_Mutuel.png   
        #print (ligne[23],'ligne[23]')   # Credit_Mutuel
        #print (ligne[24],'ligne[24]')   # 111.11
        
        #print (ligne[25],'ligne[25]')   # especes.png
        #print (ligne[26],'ligne[26]')   # caisse
        #print (ligne[27],'ligne[27]')   # 222.22
        func(*args,**kwargs)
        val_1 = acount(ligne[23])                                      #  print (ligne[22],'ligne[22]')   # Credit_Mutuel
        if val_1 != None:
            val_1 = round(val_1,2)+float(ligne[24])                    #  print (ligne[23],'ligne[23]')   # 111.11
            label_bank1.config(text = str(val_1)+" €",anchor=CENTER)
            if val_1 > 0:
                label_bank1.config(fg='black')
            else:
                label_bank1.config(fg='red')
                
        val = acount_annee(ligne[23])            #  bon print (ligne[23],'ligne[23]')   # Credit_Mutuel
        if val != None:
            val = round(val,2)
            label_bank1_annee.config(text = str(val)+" €",anchor=CENTER)
            if val > 0:
                label_bank1_annee.config(fg='black')
            else:
                label_bank1_annee.config(fg='red')
        
        val_2 = acount(ligne[26])                                       #  print (ligne[25],'ligne[25]')   # caisse
        if val_2 != None:
            val_2 = round(val_2,2)+float(ligne[27])                     #  print (ligne[26],'ligne[26]')   # 222.22
            label_bank2.config(text = str(val_2)+" €",anchor=CENTER)
            if val_2 > 0:
                label_bank2.config(fg='black')
            else:
                label_bank2.config(fg='red')
        
        val = acount_annee(ligne[26])                                   #  print (ligne[25],'ligne[25]')   # caisse
        if val != None:
            val = round(val,2)
            label_bank2_annee.config(text = str(val)+" €",anchor=CENTER)
            if val > 0:
                label_bank2_annee.config(fg='black')
            else:
                label_bank2_annee.config(fg='red')
        
        val = acount_debit() # label_credit_annee()
        if val != None:
            val = round(val,2)
            label_debit_annee.config(text = str(val)+"€")
            if val > 0:
                label_debit_annee.config(fg='black')
            else:
                label_debit_annee.config(fg='red')

        val = acount_debit_mois()
        if val != None:
            val = round(val,2)
            label_debit_mois.config(text = str(val)+"€")
            if val > 0:
                label_debit_mois.config(fg='black')
            else:
                label_debit_mois.config(fg='red')

        val = acount_credit_mois()
        if val != None:
            val = round(val,2)
            label_credit_mois.config(text = str(val)+"€")
            if val > 0:
                label_credit_mois.config(fg='black')
            else:
                label_credit_mois.config(fg='red')

        val = acount_credit() # label_credit_annee()
        if val != None:
            val = round(val,2)
            label_credit_annee.config(text = str(val)+"€")
            if val > 0:
                label_credit_annee.config(fg='black')
            else:
                label_credit_annee.config(fg='red')
        
        val = acount_service_mois() 
        if val != None:
            val = round(val,2)
            label_service_mois.config(text = str(val)+"€")
            if val > 0:
                label_service_mois.config(fg='black')
            else:
                label_service_mois.config(fg='red')
        ######
        val = acount_service() 
        if val != None:
            val = round(val,2)
            label_service_annee.config(text = str(val)+"€")
            if val > 0:
                label_service_annee.config(fg='black')
            else:
                label_service_annee.config(fg='red')
        
        val = acount_vente() 
        if val != None:
            val = round(val,2)
            label_vente_annee.config(text = str(val)+"€")
            if val > 0:
                label_vente_annee.config(fg='black')
            else:
                label_vente_annee.config(fg='red')

        val = acount_vente_mois() 
        if val != None:
            val = round(val,2)
            label_vente_mois.config(text = str(val)+"€")
            if val > 0:
                label_vente_mois.config(fg='black')
            else:
                label_vente_mois.config(fg='red')
        
    return run
def style_combo():
    dropdown_width = 200
    style = ttk.Style()
    style.configure('TCombobox', postoffset=(0, 0, dropdown_width, 0))
    style.theme_use('clam')
    style.configure("TCombobox", fieldbackground= "white", background= "white")
def set_value_combobox():
        global country,fac_saisie
        fac_saisie = []
        Database()
        conn = sqlite3.connect(dossier)
        cursor=conn.execute("SELECT IDENTIFIANT FROM REGISTRE;")
        fetch = cursor.fetchall()
        for data in fetch:
            fac_saisie.append(data[0])
        cursor.close()
        conn.close()
'''
def update_combobox(func):  #_validate(s):
    global fname_Combobox
    def r(*arg):
        global fname_Combobox
        func(*arg)
        nv = list(saveLoadTier.load_to_list())
        fname_Combobox['values'] = nv  
    return r
'''
def write_to_pdf():
    memoire_date()
    def voire_date ():
        global titre,fetch,texte
        Database()  
        tree.delete(*tree.get_children())
        cursor=conn.execute("SELECT *FROM REGISTRE WHERE GDATE LIKE ?",("%" + "-" +day_Value.get()+ "-" + annee_Value.get(),))
        fetch = cursor.fetchall()
        for data in fetch:
            tree.insert('', 'end', values=(data))
        cursor.close()
        conn.close()
        titre = day_Value.get()+ "_" + annee_Value.get()
        
        texte = ""
        for rows in fetch:
            texte += 'N°{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}'.format(rows[0],rows[1],rows[2],rows[3],rows[4],rows[5],rows[6],rows[7],rows[8],rows[9],rows[10],rows[11],rows[12])
            texte += "\n"
        lib_pdf.MoisPDF.main(titre,*fetch)     
        
        subprocess.call ([os.system('xdg-open '+ racine_banck+'Banque_PDF_'+ligne[46]+'/'+ titre + '.pdf')]) 
    btn_date = Button(fen2, text="Sélection Date",font=('verdana', 10), command= voire_date)
    btn_date.place(x=10,y=32,height=25,width=180)
def write_to_ods():
    global fetch,fichier_ods
    try:
        file = open(r''+ '/home/'+ Nnom +'/' + fichier_ods+'.csv','w')
        text = ""
        for rows in fetch:
            text += 'N°{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}'.format(rows[0],rows[1],rows[2],rows[3],rows[4],rows[5],rows[6],rows[7],rows[8],rows[9],rows[10],rows[11],rows[12])
            text += "\n"
        file.write(text)
        file=open(r''+ '/home/'+ Nnom +'/' + fichier_ods + '.csv')
        file.close()
        Label(fen2,text='/home/'+ Nnom +'/fichier.csv', font=('verdana', 10 )).place(x=30,y=59)
         
    except:
        fen_screen.attributes('-topmost',  False) 
        messagebox.showerror("showerror", "il faut selectionnais DATE ! ou\n Nom Prénom tier\n Identifiant, Genre, Note, Nature\n Banque, Libellé, Débit, Crédit\n Service, Vente",parent=fen_screen) 
        fen_screen.attributes('-topmost', True) 
########################## Boutton Action Bas ######################
####################################################################
def calculVente(*args):
    global gdate,fname,identifiant,genre,note,nature,debit,bank,credit,vente,service
    if not isInt_str(credit.get()):
        vente.config(text = "Erreure")
        return None
    if not isInt_str(service.get()):
        vente.config(text = "Erreure")
        return None
    #try:    
    #    if vente != 0:
    vente.config(text = str(float(credit.get())-float(service.get())))#round(acount(bank_3),2))
    return float(credit.get())-float(service.get())           # Calcul de vente
    #except ValueError:
        #vente.insert(text ="Erreure")
    #return float(credit.get())-float(service.get())           # Calcul de vente
def isInt_str(v):
    v = str(v).strip()
    return v=='0' or (v if v.find('..') > -1 else v.lstrip('-+').rstrip('0').rstrip('.')).isdigit()                 # Calcul de vente
@print_acount
def Database():
    global conn, cursor
    conn = sqlite3.connect(dossier)
    cursor = conn.cursor()
    #creation REGISTRE table
    cursor.execute(
        "CREATE TABLE IF NOT EXISTS REGISTRE (RID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,GDATE TEXT,FNAME TEXT, IDENTIFIANT TEXT,GENRE TEXT,NOTE TEXT, NATURE TEXT,BANK TEXT, LIBELLE TEXT, DEBIT REAL,CREDIT REAL,SERVICE REAL,VENTE REAL)")                   # Départ de la base                    # Départ de la base pour boutton de Haut
@print_acount
def DisplayData():
    Database()
    tree.delete(*tree.get_children())
    cursor=conn.execute("SELECT * FROM REGISTRE")
    cursor=conn.execute("SELECT * FROM REGISTRE  ORDER BY  (substr(GDATE, 7, 4) || '-' || substr(GDATE, 4, 2) || '-' || substr(GDATE, 1, 2))ASC;")   
    fetch = cursor.fetchall()
    for data in fetch:
        tree.insert('', 'end', values=(data))
        tree.bind("<Double-1>",OnDoubleClick)
    cursor.close()
    conn.close()                # Départ de la base 
@print_acount
def Reset():
    global jour,mise_jour
    # Effacer les données actuelles de la table
    tree.delete(*tree.get_children())
    try:
        jour.destroy()
    except:
        pass
    DisplayData()
    SEARCH.set("")
    label_debit['state']   = NORMAL
    label_credit['state']  = NORMAL
    label_service['state'] = NORMAL
    #gdate.set('')
    fname.set("")
    identifiant.set("")
    genre.set("")
    note.set('')
    nature.set("")
    bank.set('')
    libelle.set("")
    debit.set('')
    credit.set('')
    service.set('')
    #vente.set('')
    Couleur()
@print_acount
def ExitApp():
    if not tree.selection():
        messagebox.showinfo("Information","Sélectionnez la ligne à supprimer",parent=fen_screen) 
    else:
        result = messagebox.askquestion("Confirmer un choix","Effacer la ligne définitivement",icon="warning",parent=fen_screen)
        if result == 'yes':
            Yes_yes()
        else:
            pass
    #couleur()
def Yes_yes():
    curItem = tree.focus()
    contents = (tree.item(curItem))
    selecteditem = contents['values']
    tree.delete(curItem)
    cursor=conn.execute("DELETE FROM REGISTRE WHERE RID = %d" % selecteditem[0])
    conn.commit()
    cursor.close()
    conn.close()
    fen2.destroy()
    couleur()
def Delete():
    Database()
    ExitApp()
def thier():
    Tier.main()
def nom_Tier():
    return list(saveLoadTier.load_to_list())
def _validate(s):  #_validate(s):
    global nom
    dic_liste = liste_tier
    nv = [ x for x in dic_liste() if x.startswith(s) ]
    nom['values'] = nv
    return True
@print_acount
def up():
    global fen2
    if not tree.selection():
        messagebox.showerror("Information","Sélectionner une ligne",parent=fen_screen) 
    else:
        rows = tree.selection()
        for row in rows:
            tree.move(row, tree.parent(row), tree.index(row)-1)          
    couleur()
def down():
    if not tree.selection():
        messagebox.showerror("Information","Sélectionner une ligne",parent=fen_screen) 
    else:
        rows = tree.selection()
        for row in reversed(rows):
            tree.move(row, tree.parent(row), tree.index(row)+1)
    couleur()
@print_acount
def trier_Mdate():
    Database()
    conn = sqlite3.connect(dossier)
    tree.delete(*tree.get_children())
    cursor=conn.execute("SELECT * FROM REGISTRE  ORDER BY  (substr(GDATE, 7, 4) || '-' || substr(GDATE, 4, 2) || '-' || substr(GDATE, 1, 2))DESC;")
    fetch = cursor.fetchall()
    for data in fetch:
        tree.insert('', 'end', values=(data))
    cursor.close()
    conn.close()
    couleur()  
def trier_Bdate():
    Database()
    conn = sqlite3.connect(dossier)
    tree.delete(*tree.get_children())
    cursor=conn.execute("SELECT * FROM REGISTRE  ORDER BY  (substr(GDATE, 7, 4) || '-' || substr(GDATE, 4, 2) || '-' || substr(GDATE, 1, 2))ASC;")
    fetch = cursor.fetchall()
    for data in fetch:
        tree.insert('', 'end', values=(data))
    cursor.close()
    conn.close()
    Couleur()
@print_acount  
def OnDoubleClick(self):
    global ajour,Menu_saisie,jour,mise_jour
    curItem = tree.focus()
    contents = (tree.item(curItem))
    selecteditem = contents['values']
    #Définir des valeurs dans les champs
    #gdate.set      (selecteditem[1])
    fname.set      (selecteditem[2])
    identifiant.set(selecteditem[3])
    genre.set      (selecteditem[4])
    note.set       (selecteditem[5])
    nature.set     (selecteditem[6])
    bank.set       (selecteditem[7])
    libelle.set    (selecteditem[8])
    debit.set      (selecteditem[9])
    credit.set     (selecteditem[10])
    service.set    (selecteditem[11])
    #vente.set      (selecteditem[11])         # Double Click pour modification
@print_acount
def register():
    fig_facture()
    Database()
    gdate1       = gdate.get().replace("/","-")
    fname1       = fname.get()
    identifiant1 = identifiant.get()
    genre1       = genre.get()
    note1        = note.get()
    nature1      = nature.get()
    bank1        = bank.get()
    libelle1     = libelle.get()
    debit1       = debit.get()
    credit1      = credit.get()
    service1     = service.get()
    vente1       =calculVente()
    if gdate1=='' or fname1=='' or identifiant1=='' or genre1=='' or note1=='' or nature1==''or bank1=='' or libelle1=='' or debit1==''or credit1==''or service1==''or vente1=='':
        messagebox.showerror("Information","Remplir le champ vide!!!",parent=fen_screen) 
    else:
        #  Exécuter l'ordre
        conn.execute('INSERT INTO REGISTRE (GDATE,FNAME,IDENTIFIANT,GENRE,NOTE,NATURE,BANK,LIBELLE,DEBIT,CREDIT,SERVICE,VENTE) \
              VALUES (?,?,?,?,?,?,?,?,?,?,?,?)',(gdate1,fname1,identifiant1,genre1,note1,nature1,bank1,libelle1,debit1,credit1,service1,vente1));
        conn.commit()                 
        messagebox.showinfo("Ok", "Mis à jour avec succés",parent=fen_screen)
        fetch = cursor.fetchall()
        for data in fetch:
            tree.insert('', 'end', values=(data))
        DisplayData()
        conn.close()              # Mettre a jour
    couleur()
@print_acount
def Update():
    fig_facture()
    Database()
    gdate1       = gdate.get()
    fname1       = fname.get()
    identifiant1 = identifiant.get()
    genre1       = genre.get()
    note1        = note.get()
    nature1      = nature.get()
    bank1        = bank.get()
    libelle1     = libelle.get()
    debit1       = debit.get()
    credit1      = credit.get()
    service1     = service.get()
    vente1       = calculVente()
    if gdate1=='' or fname1==''or identifiant1=='' or genre1=='' or note1=='' or nature1==''or bank1=='' or libelle1=='' or debit1=='' or credit1=='' or service1=='' or vente1=='':
        messagebox.showerror("Erreure", "Remplir le champ vide!!!",parent=fen_screen)
    else:
        curItem = tree.focus()
        contents = (tree.item(curItem))
        selecteditem = contents['values']
        conn.execute('UPDATE REGISTRE SET GDATE=?,FNAME=?,IDENTIFIANT=?,GENRE=?,NOTE=?,NATURE=?,BANK=?,LIBELLE=?,DEBIT=?,CREDIT=?,SERVICE=?,VENTE=? WHERE RID = ?',(gdate1,fname1,identifiant1,genre1,note1,nature1,bank1,libelle1,debit1,credit1,service1,vente1,selecteditem[0]))
        conn.commit()                                                                                                                                           
        messagebox.showinfo("Ok", "Mis à jour avec succés",parent=fen_screen)
        Reset()
        DisplayData()
        conn.close()                # Sauvegarde     
    Couleur()
def Couleur():
    colors = ["#FFFFFF", "#E0FAE0"]
    t = 0
    for child in tree.get_children():
        tree.item(child, tags=(colors[t]))
        t += 1
        t %= len(colors)
    for color in colors:
        tree.tag_configure(color, background=color)
def aide_livre():
    subprocess.call ([os.system('xdg-open '+ chem_aide + 'Article.pdf')])
    #except :
    #    messagebox.showerror("Erreure","Pas de fichier ",parent=fen_screen)     
########################## Boutton barre ###########################
####################################################################
def Mymessagebox():
    global fen2
    fen2 = LabelFrame(fen_screen,bd=1,relief=RAISED)
    fen2.place(x=2,y=0,width=250,height=120)
def memoire_date():
    global fen2,entries ,day_entry1, day_entry2,printdate,day_Value,annee_Value,print_recheche_date
    Mymessagebox()
    crearor = tkinter.Label(fen2,text = "Date:",width=15,anchor=W,font = 'Arial 12').place(x=2,y=5,width=50)
    closeButton = tk.Button(fen2,bg="cyan", text="Sortie", width=5,fg='grey', command=fen2.destroy).place(x=10,y=85,height=25)
    def SizeDay(*args):
        value = day_Value.get()
        if len(value) > 2: day_Value.set(value[:2])
        val = annee_Value.get()
        if len(val) > 4: annee_Value.set(val[:4])
        print_recheche_date =  "-" +day_Value.get()+ "-" + annee_Value.get()
        #validfiltre = lambda ligne : ligne.day_Value.get()#[-len(gdate):] == gdate
        #print ('print_recheche_date',print_recheche_date)   
    day_Value = StringVar()
    day_Value.trace('w', SizeDay)
    annee_Value = StringVar()
    annee_Value.trace('w', SizeDay)
    day_entry1=Entry(fen2,fg="black",font = 'Arial 12',width=2, textvariable=day_Value)
    day_entry1.place(x=45, y=5,height=25) 
    day_entry2=Entry(fen2,fg="black",font = 'Arial 12',width=4, textvariable=annee_Value)
    day_entry2.place(x=73, y=5,height=25) 
    entries = [day_entry1, day_entry2]
    day_entry1.bind('<KeyRelease>', lambda e: _check(0,2))
    day_entry2.bind('<KeyRelease>', lambda e: _check(1,2)) 
def _check(index, size):
    entry = entries[index]
    next_index = index + 1
    next_entry = entries[next_index] if next_index < len(entries) else None
    data = entry.get()
    if len(data) >= size and next_entry:
        next_entry.focus()
def give_date():
    global day_entry1,day_entry2,entries,day_Value,annee_Value
    #print ('print_recheche_date',print_recheche_date)
    return titre[int(day_entry2.get())]
##################### fin de recheche Date
def Edition_fname():
    memoire_date()
    def RechecheRecord():
        global fetch,fichier_ods
        Database()
        if RECHERCHER.get() != "":
            tree.delete(*tree.get_children())
            cursor=conn.execute("SELECT * FROM REGISTRE WHERE FNAME LIKE ? AND GDATE LIKE ?",('%' + str(RECHERCHER.get()) + '%',"%" + "-" +day_Value.get()+ "-" + annee_Value.get(),))
            fetch = cursor.fetchall()
            cursor.close()
            for data in fetch:
                tree.insert('', 'end', values=(data))
            conn.close()
            fichier_ods = RECHERCHER.get()
        
    btn_recheche = Button(fen2, text="Sélection Nom",font=('verdana', 10), command=RechecheRecord)#,bg=pale)
    btn_recheche.place(x=10,y=32,height=25,width=180)
    listecredit = nom_Tier()   
    validate = (fen2.register(_validate), '%P') 
    recheche = ttk.Combobox(fen2,width=14,values = listecredit,font=('arial', 12),textvariable=RECHERCHER, validatecommand=validate,validate='key')
    recheche.place(x=10,y=59,width=180,height=25)
    couleur()       
def Edition_identifiant():
    memoire_date()
    def voire_identifiant():
        global fetch,fichier_ods
        Database()    
        tree.delete(*tree.get_children())
        cursor=conn.execute("SELECT * FROM REGISTRE WHERE IDENTIFIANT LIKE ? AND GDATE LIKE ?",('%' + str(SEARCH.get()) + '%',"%" + "-" +day_Value.get()+ "-" + annee_Value.get(),))
        fetch = cursor.fetchall()
        for data in fetch:
            tree.insert('', 'end', values=(data))
        cursor.close()
        conn.close()
        fichier_ods = SEARCH.get()
    btn_Identifiant = Button(fen2, text="Sélection Identifiant",font=('verdana', 10), command=voire_identifiant)
    btn_Identifiant.place(x=10,y=32,height=25,width=180)    
    search = Entry(fen2, textvariable=SEARCH, font=('verdana', 10) ,width=10)
    search.place(x=10,y=59,width=180,height=25)             # Boutton barre             # Boutton barre
    #return edit_column("IDENTIFIANT")
    couleur()
def Edition_genre():
    memoire_date()
    def voire_genre():
        global fetch,fichier_ods
        Database()
        if AFFICHE_GENRE.get() != "":
            tree.delete(*tree.get_children())
            cursor=conn.execute("SELECT * FROM REGISTRE WHERE GENRE LIKE ? AND GDATE LIKE ?",('%' + str(AFFICHE_GENRE.get()) + '%',"%" + "-" +day_Value.get()+ "-" + annee_Value.get(),))
            fetch = cursor.fetchall()
            for data in fetch:
                tree.insert('', 'end', values=(data))
            cursor.close()
            conn.close()
            fichier_ods = AFFICHE_GENRE.get()
    btn_genre = Button(fen2,text="Sélection Genre",font=('verdana', 11),width=12,bg=ligne[10],command=voire_genre)#,bg=pale)
    btn_genre.place(x=10,y=32,height=25,width=130)
    Genre = open(chem_note + 'genre',"r").readlines()
    for t in range(len(Genre)):
        Genre[t] = Genre[t][:len(Genre[t])-1]
    genre = ttk.Combobox(fen2,values = Genre,state="readonly",font=("calibri",10),textvariable=AFFICHE_GENRE)
    genre.place(x=10,y=59,width=130,height=25)           
    #return edit_column("GENRE")
    couleur()
def Edition_note():
    memoire_date()
    def voire_note():
        global fetch,fichier_ods
        Database()
        if AFFICHE_NOTE.get() != "":
            tree.delete(*tree.get_children())
            cursor=conn.execute("SELECT * FROM REGISTRE WHERE NOTE LIKE ? AND GDATE LIKE ?",('%' + str(AFFICHE_NOTE.get()) + '%',"%" + "-" +day_Value.get()+ "-" + annee_Value.get(),))
            fetch = cursor.fetchall()
            for data in fetch:
                tree.insert('', 'end', values=(data))
            cursor.close()
            conn.close()
            fichier_ods = AFFICHE_NOTE.get()
    btn_note = Button(fen2,text="Sélection Note",font=('verdana', 11),width=12,bg=ligne[10],command=voire_note)#,bg=pale)
    btn_note.place(x=10,y=32,height=25,width=130)
    ttk.Combobox(fen2,values =[
                'Réglés',
                'Nom réglés',
                'Acompte'],
                font=("calibri",10),state="readonly",width=9,textvariable=AFFICHE_NOTE).place(x=10,y=59,width=130,height=25)              
    couleur()
def Affiche_nature():
    memoire_date()
    def voire_nature():
        global fetch,fichier_ods
        Database()
        if AFFICHE_REGLEMENT.get() != "":
            tree.delete(*tree.get_children())
            cursor=conn.execute("SELECT * FROM REGISTRE WHERE NATURE LIKE ? AND GDATE LIKE ?",('%' + str(AFFICHE_REGLEMENT.get()) + '%',"%" + "-" +day_Value.get()+ "-" + annee_Value.get(),))
            fetch = cursor.fetchall()
            for data in fetch:
                tree.insert('', 'end', values=(data))
            cursor.close()
            conn.close()
            fichier_ods = AFFICHE_REGLEMENT.get() 

    Affiche_nature = Button(fen2, text="Sélection Réglement",font=('verdana', 10), command=voire_nature)
    Affiche_nature.place(x=10,y=32,height=25,width=180)
    ListPosibilite = open(chem_note +'tools_bank',"r").readlines()
    for t in range(len(ListPosibilite)):
        ListPosibilite[t] = ListPosibilite[t][:len(ListPosibilite[t])-1]
    natures = ttk.Combobox(fen2,values = ListPosibilite,state="readonly",font=("calibri",10),textvariable=AFFICHE_REGLEMENT)
    natures.place(x=10,y=59,width=180,height=25)                  # Boutton barre
    #return edit_column("NATURE")
    couleur()
def Edition_banck():
    memoire_date()
    def RececheBank():
        global fetch,fichier_ods
        Database()
        #checking recheche text is empty or not
        if AFFICHE_BANQUE.get() != "":
            tree.delete(*tree.get_children())
            cursor=conn.execute("SELECT * FROM REGISTRE WHERE BANK LIKE ? AND GDATE LIKE ?",('%' + str(AFFICHE_BANQUE.get()) + '%',"%" + "-" +day_Value.get()+ "-" + annee_Value.get(),))
            fetch = cursor.fetchall()
            for data in fetch:
                tree.insert('', 'end', values=(data))
            cursor.close()
            conn.close()
            fichier_ods = AFFICHE_BANQUE.get()         
    btn_bank = ttk.Combobox(fen2,width=14,values=[bank_1,bank_2],font=("calibri",10),textvariable=AFFICHE_BANQUE)
    btn_bank.place(x=5,y=59,width=180,height=25)
    #
    btn_bank = Button(fen2, text="Sélection Banque",font=('verdana', 10),width=11, command=RececheBank,bg='#BFBFBF')
    btn_bank.place(x=5,y=32,height=25,width=180)
    couleur()
def Edition_libelle():
    memoire_date()
    def voire_libelle(): 
        global fetch,fichier_ods
        Database()
        if AFFICHE_LIBELLE.get() != "":
            tree.delete(*tree.get_children())
            
            cursor=conn.execute("SELECT * FROM REGISTRE WHERE LIBELLE LIKE ? AND GDATE LIKE ?",('%' + str(AFFICHE_LIBELLE.get()) + '%',"%" + "-" +day_Value.get()+ "-" + annee_Value.get(),))
            fetch = cursor.fetchall()
            for data in fetch:
                tree.insert('', 'end', values=(data))
            cursor.close()
            conn.close() 
            fichier_ods = AFFICHE_LIBELLE.get()

    btn_libelle = Button(fen2, text="Sélection Libélle",font=('verdana', 10), command=voire_libelle)
    btn_libelle.place(x=10,y=32,height=25,width=180)
    ListPosibilite = open(chem_note +'Nature',"r").readlines()
    for t in range(len(ListPosibilite)):
        ListPosibilite[t] = ListPosibilite[t][:len(ListPosibilite[t])-1]
    btn_libelle = ttk.Combobox(fen2,values = ListPosibilite,state="readonly",width=19,font=('arial', 10),textvariable=AFFICHE_LIBELLE)
    btn_libelle.place(x=10,y=59,width=180,height=25)                 # Boutton barre
    #return edit_column("LIBELLE")
    couleur()
def Edition_date():
    memoire_date()
    def voire_date ():
        global fetch,fichier_ods
        Database()   
        tree.delete(*tree.get_children())
        cursor=conn.execute("SELECT * FROM REGISTRE WHERE GDATE LIKE ?",("%" + "-" +day_Value.get()+ "-" + annee_Value.get(),))
        fetch = cursor.fetchall()
        for data in fetch:
            tree.insert('', 'end', values=(data))
        cursor.close()
        conn.close()
        fichier_ods = day_Value.get()+ "-" + annee_Value.get()
    btn_date = Button(fen2, text="Sélection Date",font=('verdana', 10), command= voire_date)
    btn_date.place(x=10,y=32,height=25,width=180)                        
    couleur()
def Edition_Debit():
    memoire_date()
    def voire_debit():
        global fetch,fichier_ods
        Database()
        tree.delete(*tree.get_children())
        cursor=conn.execute("SELECT * FROM REGISTRE WHERE DEBIT > 0 AND GDATE LIKE ?",("%" + "-" +day_Value.get()+ "-" + annee_Value.get(),))
        fetch = cursor.fetchall()
        for data in fetch:
            tree.insert('', 'end', values=(data))
        cursor.close()
        conn.close()
        fichier_ods = 'DEBIT_'+day_Value.get()+ "-" + annee_Value.get()
    btn_debit = Button(fen2, text="Sélection Débit",font=('verdana', 10), command=voire_debit)
    btn_debit.place(x=10,y=32,height=25,width=180)  
    couleur()
def Edition_Credit():    
    memoire_date()
    def voire_credit():
        global fetch,fichier_ods
        Database()
        tree.delete(*tree.get_children())
        cursor=conn.execute("SELECT * FROM REGISTRE WHERE CREDIT > 0 AND GDATE LIKE ?",("%" + "-" +day_Value.get()+ "-" + annee_Value.get(),))
        fetch = cursor.fetchall()
        for data in fetch:
            tree.insert('', 'end', values=(data))
        cursor.close()
        conn.close()
        fichier_ods = 'CREDIT_'+ day_Value.get()+ "-" + annee_Value.get()

    btn_credit = Button(fen2, text="Sélection Crédit",font=('verdana', 10), command=voire_credit)
    btn_credit.place(x=10,y=32,height=25,width=180)              # Boutton barre  
    #return edit_column("CREDIT") 
    couleur()
def Edition_Service():    
    memoire_date()
    def voire_servie():
        global fetch,fichier_ods
        Database()
        tree.delete(*tree.get_children())
        cursor=conn.execute("SELECT * FROM REGISTRE WHERE GDATE LIKE ? AND SERVICE  > 0" ,("%" + "-" +day_Value.get()+ "-" + annee_Value.get(),))
        fetch = cursor.fetchall()
        for data in fetch:
            tree.insert('', 'end', values=(data))
        cursor.close()
        conn.close()
        fichier_ods= 'SERVICE_'+day_Value.get()+ "-" + annee_Value.get()
    btn_Service = Button(fen2, text="Sélection Service",font=('verdana', 10), command=voire_servie)
    btn_Service.place(x=10,y=32,height=25,width=180)             # Boutton barre 
    #return edit_column("SERVICE")
    couleur()
def Edition_Vente():    
    memoire_date()
    def voire_vente():
        global fetch,fichier_ods
        Database()
        tree.delete(*tree.get_children())
        cursor=conn.execute("SELECT * FROM REGISTRE WHERE GDATE LIKE ? AND VENTE > 0" ,("%" + "-" +day_Value.get()+ "-" + annee_Value.get(),))
        fetch = cursor.fetchall()
        for data in fetch:
            tree.insert('', 'end', values=(data))
        cursor.close()
        conn.close()
        fichier_ods = 'VENTE_'+day_Value.get()+ "-" + annee_Value.get()
    btn_Vente = Button(fen2, text="Sélection Vente",font=('verdana', 10), command=voire_vente)
    btn_Vente.place(x=10,y=32,height=25,width=180)               # Boutton barre 
    couleur()
######### Menu #########
    global fetch,Acompte
    Database()
    print ('#def RechecheRecord():1111')
    if Acompte.get() != "":
        print ('#def RechecheRecord():2222')
        tree.delete(*tree.get_children())
        cursor=conn.execute("SELECT * FROM REGISTRE WHERE NOTE LIKE ?", ('%' + str(Acompte.get()) + '%',))
        fetch = cursor.fetchall()
        for data in fetch:
            tree.insert('', 'end', values=(data))
        cursor.close()
        conn.close()   
        #print (data)
    couleur()    
def fig_facture():
    global note,List_fac
    try:
        if note.get() == "Réglés":
            os.chmod( liste_facture_brut + List_fac.get(), stat.S_IREAD)
    except:
        pass    
@print_acount
def Menu_saisie(sett):#metre_a_jour
    global tree,ajour,hat,mise_jour,police,country,fac_saisie,List_fac
    global Tier,affiche_saisie,genre,note,nature,gdate,bank,debit,vente,credit,service,fname,Menu_saisie,label_credit,label_service,label_debit 
    curItem = tree.focus()
    contents =(tree.item(curItem))
    selecteditem = contents['values']
    color01 = ligne[10]#('#15244C')
    color03 = 'black'
    bg_color = 'ivory4'
    date = time.strftime("%d-%m-%Y")
    gdate=Entry(affiche_saisie,width=9,font=("calibri",10),textvariable=gdate)
    gdate.grid(row=0,column=0,padx=2,ipadx=5,pady=5)
    gdate.insert(0,date)

    # *** Nom prénom tier
    def mouse_click(event):
        global nom,list_fac
        Tier.main(affiche_saisie)
    Label(affiche_saisie, text="Nom Prénom & Tier",font=("calibri",10),fg=color03).grid(row=0,column=1,padx=10,pady=5)  
    listecredit = nom_Tier()   
    validate = (affiche_saisie.register(_validate),'%P') 
    fname_Combobox = ttk.Combobox(affiche_saisie, values = listecredit,font=("calibri",10),width=25,textvariable=fname, validatecommand=validate,validate='key')
    fname_Combobox.grid(row=0,column=2,padx=0,pady=5)    
    set_value_combobox()  
    
    #callbackFunc()
    Label(affiche_saisie, text=" Identifiant N°",font=("calibri",10),fg=color03).grid(row=0,column=3,padx=10,ipady=5,sticky='w')   
    selected_item = StringVar()
    List_fac = os.listdir(liste_facture_brut)
    for t in range(len(List_fac)):
        t = len(List_fac)-1-t
        List_fac[t] = List_fac[t][:len(List_fac[t])]
        if List_fac[t] in fac_saisie:
            del List_fac[t]
    List_fac = ttk.Combobox(affiche_saisie,values = List_fac,width=17,font=("calibri",10),textvariable=identifiant)
    List_fac.grid(row=0,column=4,padx=2,pady=5,sticky='w') 
    
    nature_label = Label(affiche_saisie, text="Type Réglement ",font=("calibri",10),fg=color03).grid(row=1,column=0,columnspan=2,padx=2,sticky='w') 
    ListPosibilite = open(chem_note +'tools_bank',"r").readlines()
    for t in range(len(ListPosibilite)):
        ListPosibilite[t] = ListPosibilite[t][:len(ListPosibilite[t])-1]
    nature = ttk.Combobox(affiche_saisie,values = ListPosibilite,width=15,state="readonly",font=("calibri",10),textvariable=AFFICHE_REGLEMENT)
    nature.grid(row=1,column=1,columnspan=4,padx=35,sticky='w')  
    
    bank_label=Label(affiche_saisie, text="Banque",font=("calibri",10),fg=color03).grid(row=1,column=2,columnspan=3,padx=38,sticky='w')  
    bank = ttk.Combobox(affiche_saisie,width=16,values=[ligne[23],ligne[26]],font=("calibri",10),textvariable=bank)
    bank.grid(row=1,column=2,columnspan=4,padx=100,sticky='w')   
    
    bank_libelle=Label(affiche_saisie, text="Libellé ",font=("calibri",10),fg=color03).grid(row=1,column=2,columnspan=4,padx=260,sticky='w')
    ListPosibilite = open(chem_note +'Nature',"r").readlines()
    for t in range(len(ListPosibilite)):
        ListPosibilite[t] = ListPosibilite[t][:len(ListPosibilite[t])-1]
    List_Libelle = ttk.Combobox(affiche_saisie,values = ListPosibilite,state="readonly",width=20,font=("calibri",10),textvariable=libelle)
    List_Libelle.grid(row=1,column=3,columnspan=5,padx=90,sticky='w')
    ##############
    Label(affiche_saisie, text="Débit",font=("calibri",10),fg=color03).grid(row=0,column=5,pady=5,padx=5,sticky=W)
    def entry_changed(event = None):
        try:
            if float(label_debit.get()) > 0:
                label_credit.delete(0, 'end')
                label_credit.insert(END,"0.00")
                label_credit['state'] = DISABLED
                label_service.delete(0, 'end')
                label_service.insert(END,"0.00")
                label_service['state']= DISABLED
            else:
                label_credit['state'] = NORMAL
                label_service['state']= NORMAL
        except:
            label_credit['state'] = NORMAL
            label_service['state']= NORMAL

    label_debit= Entry(affiche_saisie, font=("calibri",10),disabledforeground="#BFBFBF",disabledbackground='#F6F6D0',width=9,textvariable=debit)
    label_debit.grid(row=0,column=6,pady=10,padx=5,sticky=W)
    label_debit.bind('<Any-KeyPress>', entry_changed)
    Label(affiche_saisie, text="Crédit",font=("calibri",10),fg=color03).grid(row=1,column=5,padx=5,sticky=W)
    def entry_chanded_debit(event =None):
        try:
            if float(label_credit.get()) > 0:
                label_debit.delete(0,'end')
                label_debit.insert(END,"0.00")
                label_debit['state'] = DISABLED  
            else:
                label_debit['state'] = NORMAL
                label_debit['state'] = NORMAL
        except:
            label_debit['state'] = NORMAL       
    label_credit=Entry(affiche_saisie,font=("calibri",10),width=9,disabledforeground="#BFBFBF",disabledbackground='#F6F6D0',textvariable=credit)
    label_credit.grid(row=1,column=6,padx=5,sticky=W)
    label_credit.bind('<Any-KeyPress>', entry_chanded_debit)
    credit.trace("w", calculVente)
    
    Label(affiche_saisie, text="Note ",font=("calibri",10),fg=color03).grid(row=2,column=0,padx=5,pady=10,sticky=SW)   
    ListNote = ['Réglés','Nom réglés','Acompte']
    for t in range(len(ListNote)):
        ListNote[t] = ListNote[t][:len(ListNote[t])]
    note = ttk.Combobox(affiche_saisie,values = ListNote,state="readonly",font=("calibri",10),width=9,textvariable=AFFICHE_NOTE)
    note.grid(row=2,column=0,columnspan=2,padx=48,pady=10,sticky=SW)

    Label(affiche_saisie, text="Genre ", font=("calibri",10),fg=color03).grid(row=2,column=1,columnspan=3,padx=60,pady=10,sticky='sw')    
    Genre = open(chem_note +'genre',"r").readlines()
    for t in range(len(Genre)):
        Genre[t] = Genre[t][:len(Genre[t])-1]
    genre = ttk.Combobox(affiche_saisie,width=13,values = Genre,state="readonly",font=("calibri",10),textvariable=AFFICHE_GENRE)
    genre.grid(row=2,column=1,columnspan=3,padx=110,pady=10,sticky=SW) 

    Label(affiche_saisie, text="Service ",font=("calibri",10),fg=color03).grid(row=2,column=5,padx=5,sticky=W)
    label_service=Entry(affiche_saisie,font=("calibri",10),disabledforeground="#BFBFBF",disabledbackground='#F6F6D0',width=9,textvariable=service)
    label_service.grid(row=2,column=6,padx=5,sticky=W)
    service.trace("w", calculVente)
    
    Label(affiche_saisie, text="Vente ",font=("calibri",10),fg=color03).grid(row=3,column=5,padx=5,sticky=NW)
    vente=Label(affiche_saisie,bg='#F6F6D0',width=8,fg="black",font=("calibri",10,"bold"))
    vente.grid(row=3,column=6,padx=5,sticky=NW)
    
    color02='#BFBFBF'
    #hat = lat-45
    haut = 40 
    police = ("arial", 10,'bold')
    photo = PhotoImage(file= chem_icon+'Réinitialiser.png')
    photo = photo.subsample(5,5)
    move_up_button = Button(affiche_saisie,text='Réinitialiser',height=37,width=59,image = photo,anchor=E,relief=RAISED,command= Reset,bg=color02,compound=LEFT,font = police)
    move_up_button.image = photo
    move_up_button.grid(row=3,column=0,columnspan=2,padx=2,sticky=NW)
    
    photo = PhotoImage(file= chem_icon+'oeil.png')
    photo = photo.subsample(6,6)
    oeil = Button(affiche_saisie,text= 'Voire \ntout',image = photo,anchor=E,width=61,height=37,relief=RAISED,command=DisplayData, compound=LEFT,font = police)
    oeil.image = photo
    oeil.grid(row=3,column=0,columnspan=3,padx=90,sticky=NW)
    
    photo = PhotoImage(file=chem_icon+'haut.png')
    photo = photo.subsample(6,6)
    move_up_button = Button(affiche_saisie,text='Ligne',height=37,width=22,image = photo,anchor=E,command= up,bg='white',compound=CENTER,font = police)
    move_up_button.image = photo
    move_up_button.grid(row=3,column=1,columnspan=3,padx=88,sticky=NW)
    
    photo = PhotoImage(file=chem_icon+'bas.png')
    photo = photo.subsample(6,6)
    move_down_button = Button(affiche_saisie,text='Ligne',height=37,width=22,image = photo,anchor=E,command= down,bg='white',compound=CENTER,font = police)
    move_down_button.image = photo
    move_down_button.grid(row=3,column=1,columnspan=2,padx=137,sticky='nw')
    
    photo = PhotoImage(file=chem_icon+'haut.png')
    photo = photo.subsample(6,6)
    bth_date = Button(affiche_saisie,text='Date\nDébut',height=37,width=22,image = photo,anchor=E,command= trier_Mdate,bg='white',compound=CENTER,font = police)
    bth_date.image = photo
    bth_date.grid(row=3,column=2,columnspan=4,padx=34,sticky=NW)
    
    photo = PhotoImage(file=chem_icon+'bas.png')
    photo = photo.subsample(6,6)
    btb_date = Button(affiche_saisie,text='Date\n Fin',height=37,width=22,image = photo,anchor=E,command= trier_Bdate,compound=CENTER,bg='white',font = police)
    btb_date.image = photo
    btb_date.grid(row=3,column=2,columnspan=4,padx=84,sticky=NW)
    
    photo = PhotoImage(file=chem_icon+'Save.png')
    photo = photo.subsample(5,5)
    ba_save = Button(affiche_saisie,height=45,width=45,image = photo,command=register)
    ba_save.image = photo
    ba_save.grid(row=3,column=2,columnspan=4,padx=135,sticky=NW)
    
    photo = PhotoImage(file=chem_icon+'Mise_jour.png')
    photo = photo.subsample(5,5)
    ba_save = Button(affiche_saisie,height=45,width=45,image = photo,command=Update)
    ba_save.image = photo
    ba_save.grid(row=3,column=2,columnspan=4,padx=187,sticky=NW)
    
    photo = PhotoImage(file=chem_icon+'pdf.png')
    photo = photo.subsample(6,6)
    pdf = Button(affiche_saisie,height=45,width=45,image = photo,command=lambda:write_to_pdf(),font = police)
    pdf.image = photo
    pdf.grid(row=3,column=2,columnspan=4,padx=238,sticky=NW)
    ###
    photo = PhotoImage(file=chem_icon+'calc.png')
    photo = photo.subsample(5,5)
    calc = Button(affiche_saisie,height=45,width=45,image = photo,command=lambda:write_to_ods(),font = police)
    calc.image = photo
    calc.grid(row=3,column=3,columnspan=6,padx=68,sticky=NW)#grid(row=3,column=4,pady=10,sticky=NW)
    
    photo = PhotoImage(file=chem_icon+'poubelle.png')
    photo = photo.subsample(5,5)
    move_up_button = Button(affiche_saisie,height=45,width=45,image = photo,command= Delete,bg='white')
    move_up_button.image = photo
    move_up_button.grid(row=3,column=4,columnspan=6,padx=3,sticky=NW)
    
    photo = PhotoImage(file=chem_icon+'aide.png')
    photo = photo.subsample(5,5)
    Ba = Button(affiche_saisie,bg='#FFFFFF',width=45,height=45,image = photo,command=aide_livre)
    Ba.image = photo
    Ba.grid(row=3,column=4,columnspan=6,padx=51,sticky=NW)

    def sortie_4():
        sett()
        fen_screen.destroy()
    photo = PhotoImage(file=chem_icon+'sortie.png')
    photo = photo.subsample(5,5)
    Ba = Button(affiche_saisie,height=45,width=45,image = photo,command=sortie_4)
    Ba.image = photo
    Ba.grid(row=3,column=4,columnspan=6,padx=104,sticky=NW)  
    
@print_acount       
def Tableau(): 
    global bank_1,bank_2,label_bank1,label_bank1_annee,label_bank2,label_bank2_annee,label_credit_annee,label_service_annee
    global label_bank3_annee,label_debit_annee,label_debit_mois,label_credit_mois,label_service_mois
    global label_vente_annee,label_vente_mois,tree
    color04='#F6F6D0'
    block_Banck01 = LabelFrame(fen_screen,bd=1,relief=RAISED,text='Gestion BANQUE',font=("Arial",12,'bold'),fg='green',bg=ligne[10])
    block_Banck01.grid(row=0,column=0,padx=1,sticky=W+N)
    block_Banck01_2 = LabelFrame(fen_screen,bd=1,relief=RAISED,height=2,text='Soldes',font=("Arial",11),bg=ligne[10])
    block_Banck01_2.grid(row=0,column=1,sticky=W+N)
    block_Banck01_3 = LabelFrame(fen_screen,bd=1,relief=RAISED,text='C A '+Date_annee,font=("Arial",11),bg=ligne[10])
    block_Banck01_3.grid(row=0,column=2,padx=1,sticky=W+N)
    block_info_1 = LabelFrame(fen_screen,bd=1,relief=RAISED,text='Info',font=("Arial",11),bg=ligne[10])
    block_info_1.grid(row=0,column=3,padx=1,sticky=W+N)
    block_info_2 = LabelFrame(fen_screen,bd=1,relief=RAISED,text=' Année  '+Date_annee,font=("Arial",11),bg=ligne[10])
    block_info_2.grid(row=0,column=4,padx=1,sticky=W+N)
    block_info_3 = LabelFrame(fen_screen,bd=1,relief=RAISED,text=' Mois '+date_info_mois+' '+date_info_annee,font=("Arial",11),bg=ligne[10])
    block_info_3.grid(row=0,column=5,padx=1,sticky=W+N)
    block_info_1Suite = LabelFrame(fen_screen,bd=1,relief=RAISED,text='Info',font=("Arial",11),bg=ligne[10])
    block_info_1Suite.grid(row=0,column=6,padx=2,sticky=W+N)
    block_info_2Suite = LabelFrame(fen_screen,bd=1,relief=RAISED,text=' Année  '+Date_annee,font=("Arial",11),bg=ligne[10])
    block_info_2Suite.grid(row=0,column=7,padx=1,sticky=W+N)
    block_info_3Suite = LabelFrame(fen_screen,bd=1,relief=RAISED,text='Mois '+date_info_mois+' '+date_info_annee,font=("Arial",11),bg=ligne[10])
    block_info_3Suite.grid(row=0,column=8,padx=1,sticky=E+N)
    #
    photo = PhotoImage(file= '/home/'+Nnom+'/Gestion_Kiwi_AE/Parametres/Bank/' + ligne[22])
    photo = photo.subsample(7,7)
    bank_1 = '\n'.join(bank_1.split())  
    label_bank1b = Button(block_Banck01,text=bank_1,image = photo,relief=RAISED,height=26,anchor=CENTER,font=("Arial", 11),command= Button_Banck1,compound=LEFT)
    label_bank1b.pack(side=TOP, padx=0,ipadx=10, fill=X)
    label_bank1b.image = photo

    label_bank1 = Label(block_Banck01_2,bg=color04,relief=RAISED,height=2,anchor=W,font=("Arial", 11))
    label_bank1.pack(side=TOP,padx=1,ipadx=10,fill=X)
    label_bank1_annee = Label(block_Banck01_3,bg=color04,relief=RAISED,height=2,anchor=W,font=("Arial", 11))
    label_bank1_annee.pack(side=TOP,padx=1,ipadx=10,fill=X)

    bank_2 = ' \n'.join(bank_2.split())  
    photo = PhotoImage(file= '/home/'+Nnom+'/Gestion_Kiwi_AE/Parametres/Bank/' + ligne[25])
    photo = photo.subsample(7,7)  
    label_bank2b = Button(block_Banck01,text=bank_2,image = photo,relief=RAISED,height=26,anchor=CENTER,font=("Arial", 11),command= Button_Banck2,compound=LEFT)
    label_bank2b.pack(side=TOP, padx=0,ipadx=10,fill=X)
    label_bank2b.image = photo

    label_bank2 = Label(block_Banck01_2,bg=color04,relief=RAISED,height=2,anchor=W,font=("Arial", 11))
    label_bank2.pack(side=TOP,pady=0,padx=1,ipadx=10,fill=X)
    label_bank2_annee = Label(block_Banck01_3,bg=color04,relief=RAISED,height=2,anchor=W,font=("Arial", 11))
    label_bank2_annee.pack(side=TOP,pady=0,padx=2,ipadx=10,fill=X)
    ## Boutton
    label_debit = Label(block_info_1,text='Débit',relief=RAISED,height=2,anchor=CENTER,font=("Arial", 11),compound=LEFT)
    label_debit.pack(side=TOP,padx=2,ipadx=10,fill=X)
    label_debit_annee = Label(block_info_2,bg=color04,relief=RAISED,height=2,anchor=CENTER,font=("Arial", 11))
    label_debit_annee.pack(side=TOP,padx=2,ipadx=10,fill=X)
    label_debit_mois = Label(block_info_3,bg=color04,relief=RAISED,height=2,anchor=CENTER,font=("Arial", 11))
    label_debit_mois.pack(side=TOP,padx=2,ipadx=10,fill=X)
    
    label_credit = Label(block_info_1,text='Crédit',relief=RAISED,height=2,anchor=CENTER,font=("Arial", 11),compound=LEFT)
    label_credit.pack(side=TOP, padx=2,ipadx=10,fill=X)
    label_credit_annee = Label(block_info_2,bg=color04,relief=RAISED,height=2,anchor=CENTER,font=("Arial",11))
    label_credit_annee.pack(side=TOP,padx=2,ipadx=10,fill=X)
    label_credit_mois = Label(block_info_3,bg=color04,relief=RAISED,height=2,anchor=CENTER,font=("Arial",11))
    label_credit_mois.pack(side=TOP,padx=2,ipadx=10,fill=X)
    credit.trace("w", calculVente)

    label_service = Label(block_info_1Suite,text='Service',relief=RAISED,height=2,anchor=CENTER,width=7, font=("Arial", 11),compound=LEFT)
    label_service.pack(side=TOP, padx=2,ipadx=10,fill=X)
    label_service_annee = Label(block_info_2Suite,bg=color04,relief=RAISED,height=2,anchor=CENTER,font=("Arial", 11))
    label_service_annee.pack(side=TOP,ipadx=10,padx=2,fill=X)
    label_service_mois = Label(block_info_3Suite,bg=color04,relief=RAISED,height=2,anchor=CENTER,font=("Arial", 11))
    label_service_mois.pack(side=TOP,padx=2,ipadx=10,fill=X)
    service.trace("w", calculVente)
    
    label_vente = Label(block_info_1Suite,text='Vente',relief=RAISED,height=2,anchor=CENTER,font=("Arial", 11),compound=LEFT)
    label_vente.pack(side=TOP, padx=2,ipadx=10,fill=X)
    label_vente_annee = Label(block_info_2Suite,bg=color04,relief=RAISED,height=2,anchor=CENTER,font=("Arial", 11))
    label_vente_annee.pack(side=TOP, padx=2,ipadx=10,fill=X)
    label_vente_mois = Label(block_info_3Suite,bg=color04,relief=RAISED,height=2,anchor=CENTER,font=("Arial", 11))
    label_vente_mois.pack(side=TOP, padx=2,ipadx=10,fill=X)
@print_acount
def main(sett,fenètrePrimère = None):
    global tree
    global SEARCH
    global RECHERCHER,DATEDATE,AFFICHE_BANQUE,AFFICHE_REGLEMENT,AFFICHE_GENRE,AFFICHE_NOTE,NOTE,RESULTAT,AFFICHE_LIBELLE,AFFICHE_DEBIT,AFFICHE_CREDIT
    global bank_1,bank_2,AFFICHE_BANK01,Acompte
    global genre_label,nom_label,nature_label,bank_label,bank_libell,nom
    global gdate,fname,identifiant,gdate,genre,note,nature,bank,libelle,debit,credit,service,vente
    global label_bank2,choix_imprim,bth_date
    global label_debit_annee,label_credit_annee,label_service_annee,label_vente_annee,hat,Menu_saisie
    global fname_Combobox,fen_screen,Affiche_donnee,fen2,block_saisie,lat,affiche_saisie,Ba
    global label_bank1_annee,label_bank2_annee,label_debit_mois,label_credit_mois,label_service_mois,label_vente_mois
    #f = open('/home/'+Nnom+'/.Config_Trefle', 'r')
    fen_screen=tkinter.Toplevel(fenètrePrimère)
    try:
        fen_screen.configure(bg=ligne[10])
    except :
        messagebox.showinfo("Information","Couleur blanc par defaut\n Probléme voire configuration",parent=fen_screen)  
        fen_screen.configure(bg=ligne[10])
        ligne[10]='#FFFFFF'
    try:
        x = fenètrePrimère.winfo_x()
        y = fenètrePrimère.winfo_y()
        w = int(str(ligne[11]).split('x',1)[0])
        h = int(ligne[11].split('x',1)[1].split('+',1)[0])
        dx = ligne[20]
        dy = ligne[21]
        fen_screen.geometry("%dx%d+%d+%d" % (w, h, x + int(str(dx)), y + int(str(dy))))
    except:
        messagebox.showinfo("Information","Dimention par defaut\n Probléme voire configuration",parent=fen_screen) 
        fen_screen.geometry('1050x400')
        ligne[11] = '1050x400'
        w=1200
        y=1000 
        h=400   
    #fen_screen.protocol('WM_DELETE_WINDOW', lambda: None)
    #fen_screen.attributes('-topmost', True)
    fen_screen.transient(fenètrePrimère)
    fen_screen.wm_attributes('-type','splash')
    
    bank_1            = ligne[23] 
    bank_2            = ligne[26] 
    Affiche_Acompte   = StringVar()
    NOTE              = StringVar()
    SEARCH            = StringVar()
    RECHERCHER        = StringVar()
    DATEDATE          = StringVar()
    AFFICHE_BANQUE    = StringVar()
    AFFICHE_REGLEMENT = StringVar()
    AFFICHE_GENRE     = StringVar()
    AFFICHE_NOTE      = StringVar()
    AFFICHE_LIBELLE   = StringVar()
    AFFICHE_DEBIT     = StringVar()
    AFFICHE_CREDIT    = StringVar()
    AFFICHE_BANK01    = StringVar()
    RESULTAT          = StringVar()
    Acompte           = StringVar()
    gdate             = StringVar()
    fname             = StringVar()
    identifiant       = StringVar()
    genre             = StringVar()
    note              = StringVar()
    nature            = StringVar()
    bank              = StringVar()
    libelle           = StringVar()
    debit             = StringVar()
    credit            = StringVar()
    service           = StringVar()
    vente             = StringVar()
    
    bordure_left = 5
    haut_screen = 0
    affiche_sqlite = Frame(fen_screen,bg=ligne[10])
    affiche_sqlite.place(x=bordure_left,y=109,width=w-10,height=h-284)
    
    affiche_saisie = Frame(fen_screen)
    affiche_saisie.place(x=bordure_left,y=h-175,width=w-10,height=170)
    
    color02='#BFBFBF'
    
    cal_label = Label(fen_screen, text="Date",relief=RAISED,font=("calibri",10),bg=color02,fg="white") 
    #cal_label.place(x=608,y=1,width=90,height=25)
    cal_label = ttk.Combobox(fen_screen,values=["2019","2020","2021","2022","2023"],font=("calibri",10))#,textvariable=AFFICHE_DATE)
    #cal_label.bind('<<ComboboxSelected>>', lambda e: cal_label.config(text = cal_label.get(),bg='#15244C',fg='white'))
    #cal_label.place(x=344,y=30,width=90,height=28)
    cal_label.current(2)
    
    #bar barre de défilement
    scrollbarx = Scrollbar(affiche_sqlite, orient=HORIZONTAL)
    scrollbary = Scrollbar(affiche_sqlite, orient=VERTICAL)
    tree = ttk.Treeview(affiche_sqlite,columns=("Student N°",'Date',"Fname","Identifiant", "Genre","Note","Nature",'Banque',"Libellé",'Débit','Crédit','Service','Vente'), 
                                selectmode="extended", height=100, yscrollcommand=scrollbary.set, xscrollcommand=scrollbarx.set) 
    scrollbary.config(command=tree.yview)
    scrollbary.pack(side=RIGHT, fill=Y)
    scrollbarx.config(command=tree.xview)
    scrollbarx.pack(side=BOTTOM, fill=X)
    #  Définition des en-têtes des colonnes
    
    style = ttk.Style(fen_screen)
    style.theme_use('default')
    style.configure("Treeview.Heading",
        background="#D3D3D3",
        foreground="blue",
        rowheight=25,
        relief= RAISED,
        bd=2,
        font=('calibri', 10),                     # police bande
        fieldbackground="#D3D3D3")
    style.configure(".", font=('Helvetica', 10))  # police Treeview
    tree.heading('Student N°'  ,text=  "N°"                                                     ,anchor=W)
    tree.heading('Date'        ,text=' Date'             ,command =lambda: Edition_date()       ,anchor=W)
    tree.heading('Fname'       ,text=" Nom prénom & Tier",command =lambda: Edition_fname()      ,anchor=W)
    tree.heading('Identifiant' ,text=" Identifiant"      ,command =lambda: Edition_identifiant(),anchor=W)   
    tree.heading('Genre',       text=" Genre"            ,command =lambda: Edition_genre()      ,anchor=W)
    tree.heading('Note',        text=" Note"             ,command =lambda: Edition_note()       ,anchor=W)
    tree.heading('Nature'      ,text=" Nature"           ,command =lambda: Affiche_nature()     ,anchor=W)
    tree.heading('Banque'      ,text=' Banque'           ,command =lambda: Edition_banck()      ,anchor=W)
    tree.heading('Libellé'     ,text=" Libellé"          ,command =lambda: Edition_libelle()    ,anchor=W)
    tree.heading('Débit'       ,text=' Débit'            ,command =lambda: Edition_Debit()      ,anchor=W)
    tree.heading('Crédit'      ,text=' Crédit'           ,command =lambda: Edition_Credit()     ,anchor=W)
    tree.heading('Service'     ,text=' Service'          ,command =lambda: Edition_Service()    ,anchor=W)
    tree.heading('Vente'       ,text=' Vente'            ,command =lambda: Edition_Vente()      ,anchor=W)
    # Réglage de la largeur des colonnes
    tree.column('#0', stretch=NO, minwidth=0, width=0)
    tree.column('#1', stretch=NO, minwidth=10, width=0)
    tree.column('#2', stretch=NO, minwidth=0, width=100)   # Date
    tree.column('#3', stretch=NO, minwidth=0, width=100)
    tree.column('#4', stretch=NO, minwidth=0, width=160)  # Identitifianr
    tree.column('#5', stretch=NO, minwidth=0, width=100)  # Genre
    tree.column('#6', stretch=NO, minwidth=0, width=90)
    tree.column('#7', stretch=NO, minwidth=0, width=120)
    tree.column('#8', stretch=NO, minwidth=0, width=120)
    tree.column('#9', stretch=NO, minwidth=0, width=190)  # Libelle
    tree.column('#10',stretch=NO, minwidth=0, width=80)  # credit
    tree.column('#11',stretch=NO, minwidth=0, width=80)
    tree.column('#12',stretch=NO, minwidth=0, width=80)
    tree.column('#13',stretch=NO, minwidth=0, width=80)
    #tree.column('#13',stretch=NO, minwidth=0, width=90) 
    tree['show'] = 'headings'
    tree.pack()
    
    #style_combo()
    try:
        Tableau()
    except :
        pass
    DisplayData()
    Menu_saisie(sett) #metre_a_jour)
    Couleur()
    fen_screen.mainloop()       
if __name__=='__main__':
    main()