#!/usr/bin/env python
# -*- coding: utf8 -*-
#latin-1
from PIL import Image
import lib.lireFacture
import lib.class_nline as ClnL
from lib.fpdf import FPDF
import re
import sys
import os
from lib.libAuxilaire import *
from Config  import *
class PDF(FPDF):
	def header(self):
		try:
			# Logo
			self.image('/home/' + Nnom + '/'+ligne[54],130, 0, 60)
			#self.cell(30, 10, 'Title', 1, 0, 'C')
		except:
			pass
    # Page footer
	def footer(self):
		# Position at 1 cm 
		self.set_y(-10)
		
		self.set_font('Arial', 'I', 7)
		with open(chem_note +'Pied_page' , "r") as filin:
			
			self.cell(0,10,filin.readline(),0,0,'C')
			self.set_y(-10)
			self.cell(0,5,filin.readline(),0,0,'C')
		self.set_y(-10)
		self.cell(0, 4, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'R')
	
def adapte(nobre_d_esspace_a_vide,variable):
	variable = racourcire(variable,nobre_d_esspace_a_vide)
	lenVar = len(str(variable))
	nonbre_d_espace = nobre_d_esspace_a_vide - lenVar
	if nonbre_d_espace <= 0:
		return ''
	return nonbre_d_espace * ' '
	
		
def positif(val):
	if val < 0:
		val = val * -1
	return val
	
def racourcire(ref,l):
	return ref[:l]#ref[:positif(len(ref))-2-l] 

def main(fileName,affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date,entier_Int):
	global ligne		     
		
	# ======== Tierh
	affai = affai.get()       
	nom =nom.get()     
	rue =rue.get()
	rue2 =rue2.get()
	codepos =codepos.get()
	ville =ville.get()
	objet = objet.get()
	objet_suite = objet_suite.get()
	date = date.get()
	
	####################################################################
	pdf = PDF()
	pdf.alias_nb_pages()
	pdf.add_page()
		
	def openf(filename,affai, nom,rue,rue2,codepos,ville, objet,objet_suite,entier_Int,createFileBu,fen,framejour):
			pdf.ln(0)
	
	try:
		pass
	except FileNotFoundError:
		print('Fichier introuvable.')
	except IOError:
		print('Erreur d\'entrée/sortie.')
	myfile = open(chem_note +'Entete', 'r')

	pdf.set_font('Times', '', 10)	
	line1= (myfile.readline())
	line2= (myfile.readline())
	line3= (myfile.readline())
	line4= (myfile.readline())
	line5= (myfile.readline())
	####################################################################
	pdf.set_draw_color(207, 207, 207),pdf.ln(0)
	pdf.cell(0,0,line1),pdf.ln(0)
	pdf.cell(0,8,line2),pdf.ln(0)
	pdf.set_text_color(1,1,1)
	pdf.cell(0,16,line3),pdf.ln(0)
	pdf.cell(0,24,line4),pdf.ln(0)
	pdf.cell(0,32,line5),pdf.ln(0)
	######################################################################## pdf.cell(120,5,'',0,0,'C', True)
	pdf.ln(30)
	pdf.set_font('Arial', '', 10)
	pdf.set_text_color(1,1,1) 
	pdf.set_fill_color(250,250,250)
	pdf.cell(13,4,'Affaire!',0,0,'D',True),pdf.cell(30,4,affai,0,0,'D',True),pdf.cell(70),pdf.cell(75,4,nom,0,0,'D',True)
	pdf.ln(4)
	pdf.cell(13,4,'Objet',0,0,'D'   ,True),pdf.cell(30,4,objet,0,0,'D',True) ,pdf.cell(70),pdf.cell(75,4,rue,0,0,'D',True)
	pdf.ln(5)
	pdf.cell(53,4,objet_suite,0,0,'D',True),pdf.cell(60)                                  ,pdf.cell(75,4,rue2,0,0,'D',True)
	pdf.ln()
	pdf.cell(113),pdf.cell(12,4,codepos,0,0,'D',True),pdf.cell(63,4,ville,0,0,'D',True)
	pdf.ln(10)
	pdf.set_font('Arial', '', 11)
	pdf.cell(30,2,'Intervention N°'),pdf.cell(15,2, entier_Int),pdf.cell(20,),pdf.cell(10,2,'Date'),pdf.cell(10,2 ,str(date))
	pdf.set_font('Arial', '', 10)
	pdf.set_text_color(1,1,1) 
	pdf.set_fill_color(255,255,255)
	pdf.rect(123, 39, 80, 18, 'D') #  Gauche -Hauteur -Largeur -Epaisseur
	#################################
	pdf.ln(5)
	pdf.rect(10,68,192,25, "D")     #  Gauche -Hauteur -Largeur -Epaisseur 
	pdf.set_font('Arial', '', 10)
	pdf.set_text_color(10, 10, 10) 

	pdf.cell(20,5,'Réference',1,0,'L',1),pdf.cell(110,5,'Designation',1,0,'L',1),pdf.cell(9,5,'%',1,0,'L',1),pdf.cell(10,5,'QTE',1,0,'L',1),pdf.cell(19,5,'P.U HT',1,0,'L',1),pdf.cell(24,5,'TOTAL HT',1,0,'L',1)
	pdf.ln(5)
	pdf.cell(20,5,'',1,0,'L'),pdf.cell(110,5,ligne[34],1,0,'L'),pdf.cell(9,5,'',1,0,'L'),pdf.cell(10,5,'',1,0,'L'),pdf.cell(19,5,ligne[35],1,0,'L'),pdf.cell(24,5,'',1,0,'L')
	pdf.ln(5)
	pdf.cell(20,5,'',1,0,'L'),pdf.cell(110,5,ligne[36],1,0,'L'),pdf.cell(9,5,'',1,0,'L'),pdf.cell(10,5,'',1,0,'L'),pdf.cell(19,5,ligne[37],1,0,'L'),pdf.cell(24,5,'',1,0,'L')
	pdf.ln(5)
	pdf.cell(20,5,'',1,0,'L'),pdf.cell(110,5,ligne[38],1,0,'L'),pdf.cell(9,5,'',1,0,'L'),pdf.cell(10,5,'',1,0,'L'),pdf.cell(19,5,ligne[39],1,0,'L'),pdf.cell(24,5,'',1,0,'L')
	pdf.ln(5)
	
	compteur = 0
	while compteur <23:
		pdf.cell(20,5,'',1,0,'L'),pdf.cell(110,5,'',1,0,'L'),pdf.cell(9,5,'',1,0,'L'),pdf.cell(10,5,'',1,0,'L'),pdf.cell(19,5,'',1,0,'L'),pdf.cell(24,5,'',1,0,'L')
		pdf.ln(5)
		compteur +=1
	
	pdf.set_font('Arial', '', 11)
	
	pdf.set_fill_color(280, 280, 280),	pdf.set_text_color(0, 0, 255)
	pdf.cell(130),pdf.cell(38,5,'Sous total !',1,0,'L') ,pdf.cell(24,5,str(),1,0,'L'),pdf.ln(5)
	pdf.cell(130),pdf.cell(38,5,'Acompte!',1,0,'L')     ,pdf.cell(24,5,str(),1,0,'L'),pdf.ln(5)
	pdf.cell(130),pdf.cell(38,5,'Total Net HT!',1,0,'L'),pdf.cell(24,5,str(),1,0,'L')
	
	pdf.ln(15),pdf.set_text_color(10, 10, 10) ,pdf.set_font('Times', '', 10)
	pdf.cell(123,5,'Mode de Réglement Chéque []  Espece []',0,0,'L'),pdf.ln(5)
	pdf.cell(123,5,' TVA non applicable, art. 293 B du CGI',0,0,'L')
	pdf.ln(15)
	pdf.line(1, 287, 209, 287)
	pdf.cell(0,5,'Conditions générales',0,0,'L')
	pdf.ln(5)
	monfile = open(chem_note +'Condition_generale', 'r')
		
	pdf.set_font('Times', '', 8)	
	line= (monfile.readline())
	pdf.multi_cell(0,3,str(line),1,0,'L'),pdf.ln(0)
	monfile.close()
	nom = ''.join(nom.split()) # Enlever les espace	
	pdf.output(liste_inter + entier_Int+'_'+ nom +'.pdf', 'F')



