#!/usr/bin/env python
# -*- coding: utf8 -*-
#latin-1
import  unicodedata
from PIL import Image
import lib.lireFacture
import lib.class_nline as ClnL
from lib.fpdf import FPDF
import re
import sys
import os
from lib.libAuxilaire import *
from Config  import * 
Nnom=os.getlogin()
class PDF(FPDF):
	def header(self):
		try:
		# Logo carte
			self.image('/home/' + Nnom + '/'+ligne[54],130, 0, 60)
		except:
			pass
    # Page footer
	def footer(self):
		# Position at 1 cm 
		self.set_y(-10)
		self.set_font('Arial', 'I', 7)
		try:
			with open(chem_note +'Pied_page' , "r") as filin:
				
				self.cell(0,10,filin.readline(),0,0,'C')
				self.set_y(-10)
				self.cell(0,5,filin.readline(),0,0,'C')
				self.set_y(-10)
				self.cell(0, 4, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'R')
		except:
			pass
	
def adapte(nobre_d_esspace_a_vide,variable):
	variable = racourcire(variable,nobre_d_esspace_a_vide)
	lenVar = len(str(variable))
	nonbre_d_espace = nobre_d_esspace_a_vide - lenVar
	if nonbre_d_espace <= 0:
		return ''
	return nonbre_d_espace * ' '
	
		
def positif(val):
	if val < 0:
		val = val * -1
	return val
	
def racourcire(ref,l):
	return ref[:l]#ref[:positif(len(ref))-2-l] 

def main(fileName,affai, nom,rue,rue2,codepos,ville, objet,objet_suite,date,entier, colone,netht,acompte,var1_banck,close_check,pro_check,select):
	affai = affai.get()       
	nom 		= nom.get()     
	rue 		= rue.get()
	rue2 		= rue2.get()
	codepos     = codepos.get()
	ville 		= ville.get()
	objet 		= objet.get()
	objet_suite = objet_suite.get()
	date 		= date.get()
	acompte 	= acompte.get()
	netht 		= netht.cget('text') 
	#totaltht = totaltht.get()
	#
	pdf = PDF()
	pdf.alias_nb_pages()
	pdf.add_page()
		
	def openf(filename,affai, nom,rue,rue2,codepos,ville,objet,objet_suite,entier, colone,netht,acompte,createFileBu,fen,supre_colone_select,framejour,ligne,var1_banck,close_check,pro_check,select):
		pdf.ln(0)
	try:
		mon_fichier = open(chem_note +'Entete', 'r')
	except FileNotFoundError:
		print('Fichier introuvable.')
	'''
	except IOError:
		print('Erreur d\'entrée/sortie.')
	mon_fichier = open(ligne[36]+"/Parametres/Entete", 'r')
	'''
	pdf.set_font('Times', '', 10)	
	line1= (mon_fichier.readline())
	line2= (mon_fichier.readline())
	line3= (mon_fichier.readline())
	line4= (mon_fichier.readline())
	line5= (mon_fichier.readline())
	#
	pdf.set_draw_color(207, 207, 207),pdf.ln(0)
	pdf.cell(0,0,line1),pdf.ln(0)
	pdf.cell(0,8,line2),pdf.ln(0)
	pdf.set_text_color(1,1,1)
	pdf.cell(0,16,line3),pdf.ln(0)
	pdf.cell(0,24,line4),pdf.ln(0)
	pdf.cell(0,32,line5),pdf.ln(0)
	# pdf.cell(120,5,'',0,0,'C', True)
	pdf.ln(1*35)
	pdf.set_font('Arial', '', 10)
	pdf.set_text_color(1,1,1) 
	pdf.set_fill_color(250,250,250)
		
	pdf.cell(13,4,'Affaire!',0,0,'D',True),pdf.cell(87,4,affai,0,0,'D',True),pdf.cell(20),pdf.cell(65,4,nom,0,0,'D',True)
	pdf.ln(4)
	pdf.cell(120)														   	,pdf.cell(65,4,rue,0,0,'D',True)
	pdf.ln(4)
	pdf.cell(13,4,'Objet',0,0,'D'   ,True),pdf.cell(87,4,objet,0,0,'D',True) ,pdf.cell(20),pdf.cell(65,4,rue2,0,0,'D',True)
	pdf.ln(4)
	pdf.cell(87,4,objet_suite,0,0,'D',True),pdf.cell(33),pdf.cell(12,4,codepos,0,0,'D',True),pdf.cell(53,4,ville,0,0,'D',True)
	pdf.ln(4)
	pdf.cell(120)
	pdf.ln(12)
	pdf.set_font('Arial', '', 11)
	pdf.cell(20,2,'Facture N°'),pdf.cell(20,2, entier),	pdf.cell(50,),pdf.cell(10,2,'Date'),pdf.cell(60,2 ,str(date))
	pdf.set_font('Arial', '', 10)
	pdf.set_text_color(1,1,1) 
	pdf.set_fill_color(25,20,25)
	pdf.rect(130, 44, 65, 18, 'D') #  Gauche -Hauteur -Largeur -Epaisseur
	#################################
	pdf.ln(8)
	pdf.set_font('Arial', '', 10)
	pdf.set_text_color(1,1,1) 
	pdf.set_fill_color(250,250,250)
	###
	pdf.line(2, 81, 205, 81)
	pdf.line(2, 86, 205, 86)
	pdf.cell(-8,5,'','L',0,'L'),pdf.cell(25,5,'Réference','L',0,'L'),pdf.cell(109,5,'Designation','L',0,'L'),pdf.cell(6,5,'%','L',0,'L')
	pdf.cell(15,5,'QTE','L',0,'L'),pdf.cell(5,5,'','C',0,'L',),pdf.cell(19,5,'P.U HT','L',0,'L'),pdf.cell(24,5,'TOTAL HT',1,0,'C',1)
	pdf.ln(3)
	trait = 86
	
	pdf.set_draw_color(209,206,206)
	pdf.set_fill_color(2,2,1)
	
	totaltht = 0
	for lignecolonne in colone :
		ref   = lignecolonne.ref.get()
		desig = lignecolonne.desig.get()
		qte   = lignecolonne.qte.get()
		unite = lignecolonne.unite.get()
		remi  = lignecolonne.remi.get()
		uht   = lignecolonne.uht.get()
		tht   = lignecolonne.tht.get()
		pdf.ln(1)
		pdf.set_font('Times', '', 9)
		pdf.ln(1)
		
		pdf.cell(-8,5) 
		pdf.cell(33,5,racourcire(ref,25),'L',0,'L')
		pdf.cell(-8,5)
		pdf.cell(1,0,adapte(25,ref))  ,pdf.cell(-1,5),pdf.cell(108,5,racourcire(desig,54),'L',0,'L')
		pdf.cell(1,5,adapte(54,desig)),pdf.cell(4,5  ,racourcire(remi,4),'L',0,'L')
		pdf.cell(2,5,adapte(2 ,remi)) ,pdf.cell(4,5  ,racourcire(qte,4),'L',0,'L')
		pdf.cell(1,5,adapte(4 ,qte))  ,pdf.cell(14,5  ,racourcire(unite,15),'1',0,'R')
		pdf.cell(1,5,adapte(15 ,unite)),pdf.cell(18,5 ,racourcire(uht,10),'L',0,'L')
		pdf.cell(1,5,adapte(10,uht))  ,pdf.cell(24,5 ,racourcire(tht,10),'L',0,'L')
		pdf.cell(1,5,adapte(10,tht),'L',0,'L')
		
		pdf.ln(3)
		totaltht  += float(tht)
		trait += 5	

	pdf.line(2, trait, 205, trait)	
	pdf.cell(1,0,adapte(0,'')),pdf.cell(191,2,racourcire('',10),'B',0,'L')
	pdf.ln(2),pdf.set_font('Arial', '', 11)
	pdf.set_fill_color(280, 280, 280),pdf.set_text_color(0, 0, 255)
	pdf.cell(106),pdf.cell(65,5,'Sous total !',1,0,'L') ,pdf.cell(24,5,str(totaltht),1,0,'L'),pdf.ln(5),pdf.set_text_color(0, 0, 0)
	
	pdf.cell(60,5,'condition de  '+ select,0,0,'L'),pdf.set_text_color(0, 0, 255),pdf.cell(46)
	pdf.cell(65,5,'Acompte!',1,0,'L'),pdf.cell(24,5,str(acompte) ,1,0,'L') ,pdf.ln(5),pdf.set_text_color(0, 0, 0)
	pdf.cell(70,5,'',0,0,'L'),pdf.set_text_color(0, 0,255),pdf.cell(36)

		
	edition_pro(pro_check,pdf)

	#pdf.cell(65,5,taf,1,0,'L'),
	pdf.cell(24,5,str(netht) +' '+ chr(128),1,0,'L')
	pdf.ln(5),pdf.set_text_color(10, 10, 10) ,pdf.set_font('Times', '', 10)
	pdf.cell(136,5,'Mode de Réglement Chéque []  Espece []',0,0,'L'),pdf.cell(15,5,' TVA non applicable, art. 293 B du CGI',0,0,'L'),pdf.ln(1)
	pdf.ln(10)

	clause(close_check,pdf)
	pdf.set_font('Times', '', 8)
	pdf.ln(5)	
	banck(var1_banck,pdf)
	
	nom = ''.join(nom.split()) 	
	pdf.output(liste_facture + entier+'_'+ nom +'.pdf', 'F')
	#liste_facture

def edition_pro(pro_check,pdf):
	if pro_check == 0:
		pdf.cell(65,5,'PRIX GLOBAL EN EUROS !',1,0,'L',True)
	else:
		pdf.cell(65,5,'Total Net HT!',1,0,'L',True)

def clause(close_check,pdf):  
	mon_fichier = open(chem_note +'Condition_generale', 'r')
	clause = mon_fichier.read()
	if close_check == 0:
		pass
	else:
		pdf.cell(0,5,'Conditions générales',0,0,'L')
		pdf.ln(4),pdf.multi_cell(0,3,str(clause).replace("["," "),0,0,'L'),pdf.ln(0)
		mon_fichier.close()
def banck(var1_banck,pdf):
	fich1 =open(chem_note +'banquaires',"r")
	ibam = fich1.read()
	if var1_banck == 0:
		pass
	else:
		pdf.ln(3),pdf.multi_cell(0,3,str(ibam).replace("["," "),0,0,'L'),pdf.ln(0) #.replace("0"," ")
