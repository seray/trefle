#coding:utf-8
import tkinter.messagebox as tkMessageBox
from math import *
import tkinter
from PIL import Image
from lib.class_nline import *
import lib.class_nline as ClnL
from lib.fpdf import FPDF
import sys
import os
import time
import datetime
import locale
import sys
from lib.libAuxilaire import *
from Config  import *
class PDF(FPDF):
	
	def footer(self):
		# Position at 1 cm 
		self.set_y(-10)
		self.set_font('Arial', 'I', 7)
		with open(chem_note + 'Pied_page' , "r") as filin:
			self.cell(0,10,filin.readline(),0,0,'C')
			self.set_y(-10)
			self.cell(0,5,filin.readline(),0,0,'C')
		self.set_y(-10)
		self.cell(0, 4, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'R')	
def adapte(nobre_d_esspace_a_vide,variable):
	variable = racourcire(variable,nobre_d_esspace_a_vide)
	lenVar = len(str(variable))
	nonbre_d_espace = nobre_d_esspace_a_vide - lenVar
	if nonbre_d_espace <= 0:
		return ''
	return nonbre_d_espace * ' '
def positif(val):
	if val < 0:
		val = val * -1
	return val
def racourcire(date,l):
	print(date ,l)
	return str(date)[:int(l)]

class PDF(FPDF):
	
	def footer(self):
		# Position at 1 cm 
		self.set_y(-10)
		self.set_font('Arial', 'I', 7)
		with open(chem_note +'Pied_page' , "r") as filin:
			self.cell(0,10,filin.readline(),0,0,'C')
			self.set_y(-10)
			self.cell(0,5,filin.readline(),0,0,'C')
		self.set_y(-10)
		self.cell(0, 4, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'R')	
def adapte(nobre_d_esspace_a_vide,variable):
	variable = racourcire(variable,nobre_d_esspace_a_vide)
	lenVar = len(str(variable))
	nonbre_d_espace = nobre_d_esspace_a_vide - lenVar
	if nonbre_d_espace <= 0:
		return ''
	return nonbre_d_espace * ' '
def positif(val):
	if val < 0:
		val = val * -1
	return val
def racourcire(date,l):
	#print(date ,l)
	return str(date)[:int(l)]

def Solde(resultat ,forme_date): # forme_date
	fichier = eval(open('/home/'+ Nnom +'/Gestion_Kiwi_AE/Parametres/raport_solde','r').read())
	fichier[forme_date] =  str(resultat )
	open(chem_note +'/home/'+ Nnom +'/Gestion_Kiwi_AE/Parametres/raport_solde','w').write(str(fichier))	
def main(titre,*rows):
	pdf = PDF('L','mm','A4')
	pdf.alias_nb_pages()
	pdf.add_page()
	def openf(colone):
		pdf.ln(0)
	myfile = open(chem_note +'Entete', 'r')
	pdf.set_font('Times', '', 10)	
	pdf.cell(130),pdf.cell(30,7,titre,1,0,'L'),pdf.ln(0)
	line1= (myfile.readline())
	line2= (myfile.readline())
	line3= (myfile.readline())
	line4= (myfile.readline())
	line5= (myfile.readline())
	pdf.ln(0)
	pdf.cell(0,0,line1),pdf.ln(0)
	pdf.cell(0,8,line2),pdf.ln(0)
	pdf.set_text_color(1,1,1)
	pdf.cell(0,16,line3),pdf.ln(0)
	pdf.cell(0,24,line4),pdf.ln(0)
	pdf.cell(0,32,line5),pdf.ln(0)
	#######
	rouge = pdf.set_text_color(255,10,10)# Rouge
	bleu =  pdf.set_text_color(0,0,255)#Bleu
	pdf.set_text_color(10,10,10)# Noir
	
	pdf.ln(20)
	pdf.set_font('Arial', '', 10)
	pdf.set_text_color(1,1,1) 
	pdf.set_fill_color(250,250,250)
	pdf.cell(-9,5,'','L',0,'L'),pdf.cell(18,5,'Date',1,0,'C',True),pdf.cell(37,5,'Tier',1,0,'C',True),pdf.cell(35,5,'Identifiant (N° facture)',1,0,'C',True),pdf.cell(26,5,'Genre',1,0,'C',True),pdf.cell(20,5,'Note',1,0,'C',True),pdf.cell(26,5,'Nature',1,0,'C',True),pdf.cell(30,5,'Banque',1,0,'C',True),pdf.cell(35,5,'Libelle',1,0,'C',True),pdf.cell(17,5,'Débit',1,0,'C',True)  ,pdf.cell(18,5,'Crédit',1,0,'C',True),pdf.cell(16,5,'Service',1,0,'C',True),pdf.cell(17,5,'Vente',1,0,'C',True)#,pdf.ln(5)
	
	trait = 17	
	#pdf.line(1, trait, 296, trait)
	pdf.ln(2)
	pdf.set_font('Arial', '', 10)
	t = 20
	# Ventilation
	total_debit   = 0
	total_credit  = 0
	total_vente   = 0
	total_service = 0
	total_bank    = 0
	
	for lignecolonne in rows :
		date        = lignecolonne[1] 
		nom         = lignecolonne[2]
		identifiant = lignecolonne[3]											
		genre       = lignecolonne[4]
		note        = lignecolonne[5]
		nature      = lignecolonne[6]
		bank        = lignecolonne[7]
		libelle     = lignecolonne[8]
		debit       = lignecolonne[9]
		credit      = lignecolonne[10]
		service     = lignecolonne[11]
		vente       = lignecolonne[12]
		
		pdf.ln(3)
		pdf.set_font('Times', '', 10)
		pdf.cell(-9,5,'',0,0,'L'),pdf.cell(17,5,racourcire(date,10),'L',0,'L'),pdf.set_text_color(10,10,10)# Noir
		pdf.cell(1,0,adapte(0,date))       ,pdf.cell(36,5 ,racourcire(nom,36),'L',0,'L')
		pdf.cell(1,0,adapte(0,nom))        ,pdf.cell(34,5 ,racourcire(identifiant,50),'L',0,'L')
		pdf.cell(1,0,adapte(0,identifiant)),pdf.cell(25,5 ,racourcire(genre ,15),'L',0,'L')
		pdf.cell(1,0,adapte(0,genre ))     ,pdf.cell(19,5 ,racourcire(note,24),'L',0,'L')  
		pdf.cell(1,0,adapte(0,note))       ,pdf.cell(25,5 ,racourcire(nature ,25),'L',0,'L')  #,pdf.set_text_color(255,10,10) # rouge
		pdf.cell(1,0,adapte(0,nature ))    ,pdf.cell(29,5 ,racourcire(bank,29),'L',0,'L')
		pdf.cell(1,0,adapte(0,bank))       ,pdf.cell(34,5 ,racourcire(libelle,34),'L',0,'L')  #, pdf.set_text_color(0,0,255)#Bleu
		pdf.cell(1,0,adapte(0,libelle))    ,pdf.cell(16,5 ,racourcire(debit,16),'L',0,'L')    #,pdf.set_text_color(0,0,255)#Bleu
		pdf.cell(1,0,adapte(0,debit))      ,pdf.cell(17,5 ,racourcire(credit,16),'L',0,'L')
		pdf.cell(1,0,adapte(0,credit))     ,pdf.cell(15,5 ,racourcire(service,16),'L',0,'L')
		pdf.cell(1,0,adapte(0,service))    ,pdf.cell(17,5 ,racourcire(vente,17),'LR',0,'L')
		pdf.cell(1,0,adapte(0,vente))      ,pdf.set_text_color(10, 10,10) 
		pdf.set_text_color(10, 10, 10)
		t +=3
		total_debit   += float(debit)
		total_credit  += float(credit)
		total_vente   += float(vente)
		total_service += float(service)		
		Trait2 = trait + t
	try:
		pdf.line(1, Trait2, 296, Trait2),pdf.ln(5)  
	except:
		Trait2 = 30  
	pdf.cell(218,5,' '),pdf.cell(17,5,str(total_debit)+' ' + chr(128), 1, 0, 'C',True),pdf.cell(18,5,str(total_credit)+' ' + chr(128),1, 0, 'C',True),pdf.cell(16,5,str(total_service)+' ' + chr(128), 1, 0, 'C',True) ,pdf.cell(17,5,str(total_vente)+' ' + chr(128),1, 0, 'C',True)
	
	pdf.ln(0)
	pdf.output (liste_banck + titre + '.pdf','F')
	'''
	if not os.path.exists(path) :
		messagebox.showinfo(title = "Probléme du Fichier", message = "Fichier Recette.dat\n absent ou erronée")
	FILE_NAME = '/home/'+ Nnom +'/Gestion_Trefle/Gestion/Recette/Recette_PDF/'+ titre + '.pdf'
	pdf.cell(-9,5,'')
	#pdf.output ('/home/'+ Nnom +'/Gestion_Trefle/Gestion/Recette/Recette_PDF/'+ titre + '.pdf','F')
	'''
		
		
			
													 
