#!/usr/bin/python3
# -*- coding: utf-8 -*-
import tkinter
from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from tkinter import simpledialog
import lib.Scrolled
from lib.Scrolled import *
import tkinter.font as tkFon
import tkinter.colorchooser
import fnmatch
import os, sys
from os import system
from PIL import Image, ImageTk

Nnom=os.getlogin()
chem_icon           = '/opt/Kiwi_AE/Icon/'
chem_config         = '/home/'+ Nnom +'/Gestion_Kiwi_AE/Parametres/Note/Config_Kiwi_AE'
chem_parametre      = '/home/'+Nnom+'/Gestion_Kiwi_AE/Parametres/'
chem_config_secours = '/home/'+ Nnom +'/Gestion_Kiwi_AE/Parametres/Note/Config_Kiwi_AE.000'
class PassPlusApp:
	def __init__(self, master):
		self.entry_ligne = {}
		self.master = master
		self.frame_header = ttk.Frame(master)
		self.frame_header.pack()
		ttk.Label(self.frame_header, text = "Bienvenue ")
		self.frame_pass = ttk.Frame(master)
		self.frame_pass.pack()

		padx_place = 260
		Label(fen2,text=" Configuration ",fg='blue',font = 'Arial 17' ).grid(row=0, column=0,padx=120,ipady=5,sticky=W)
		Label(fen2,text=" Couleur & dimension",fg='brown',font = '("Arial 12 bold').grid(row=1,column=0,padx=75,ipady=0,sticky=W) 
		font1 = 'Arial 12'
		crearor = tkinter.Label(fen2,text = "Couleur Kiwi_AE ",anchor="w",width=21,font = font1 ).grid(row=2,padx=5,sticky=W) 
		self.entry_ligne[1] = tkinter.Entry(fen2,font =font1,width=16) 
		self.entry_ligne[1].grid(row=2,column=0,columnspan=1,padx=padx_place,sticky=W)
		
		Label(fen2,text = "Dimension Menu ",anchor="w",width=21 ,font = font1 ).grid(row=3,padx=5,sticky=W)  ## 1
		self.entry_ligne[2] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[2].grid(row=3,column=0,columnspan=1,padx=padx_place,sticky=W) 
		
		Label(fen2,text = "Couleur Facture ",anchor="w",width=21 ,font = font1 ).grid(row=4,padx=5,sticky=W) ##2
		self.entry_ligne[3] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[3].grid(row=4,column=0,columnspan=1,padx=padx_place,sticky=W)
		
		Label(fen2,text = "Couleur Devis Avoir ",anchor="w",width=21,font = font1 ).grid(row=5,padx=5,sticky=W) ## 3
		self.entry_ligne[4] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[4].grid(row=5,column=0,columnspan=1,padx=padx_place,sticky=W) 
		
		Label(fen2,text = "Dimension Devis Avoir Facture",anchor="w",width=25,font = font1).grid(row=6,padx=5,sticky=W)## 4
		self.entry_ligne[5] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[5].grid(row=6,column=0,columnspan=1,padx=padx_place,sticky=W) 
		
		crearor = tkinter.Label(fen2,text="Couleur Devis ",anchor="w",width=21,font =font1).grid(row=7,padx=5,sticky=W)  ##5
		self.entry_ligne[6] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[6].grid(row=7,column=0,columnspan=1,padx=padx_place,sticky=W)
		
		Label(fen2,text ="Couleur Avoir ",anchor="w",width=21,font = font1 ) .grid(row=8,padx=5,sticky=W) 	 		
		self.entry_ligne[7] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[7].grid(row=8,column=0,columnspan=1,padx=padx_place,sticky=W)
		
		Label(fen2,text ="Couleur Agenda ",anchor="w",width=21 ,font = font1 ) .grid(row=9,padx=5,sticky=W) 	 		
		self.entry_ligne[8] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[8].grid(row=9,column=0,columnspan=1,padx=padx_place,sticky=W)
		
		Label(fen2,text="Couleur Tier ",anchor="w",width=21,font =font1 ) .grid(row=10,padx=5,sticky=W) 
		self.entry_ligne[9] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[9].grid(row=10,column=0,columnspan=1,padx=padx_place,sticky=W)
		
		Label(fen2,text="Dimension Tier ",anchor="w",width=21,font = font1 ).grid(row=11,padx=5,sticky=W)  ## 9
		self.entry_ligne[10] = tkinter.Entry(fen2,font =font1,width=16)
		self.entry_ligne[10].grid(row=11,column=0,columnspan=1,padx=padx_place,sticky=W)
		
		Label(fen2,text="Couleur Recette ",anchor="w",width=21,font = font1) .grid(row=12,padx=5,sticky=W) 
		self.entry_ligne[11] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[11].grid(row=12,column=0,columnspan=1,padx=padx_place,sticky=W)
		
		Label(fen2,text="Dimension Recette ",anchor="w",width=21,font = font1 ) .grid(row=13,padx=5,sticky=W) ## 11
		self.entry_ligne[12] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[12].grid(row=13,column=0,columnspan=1,padx=padx_place,sticky=W)
		
		Label(fen2,text="Couleur Article ",anchor="w",width=21,font = font1 ).grid(row=14,padx=5,sticky=W) 
		self.entry_ligne[13] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[13].grid(row=14,column=0,columnspan=1,padx=padx_place,sticky=W)
		
		Label(fen2,text="Dimension Article ",anchor="w",width=21,font = font1 ) .grid(row=15,padx=5,sticky=W)   ## 13
		self.entry_ligne[14] = tkinter.Entry(fen2,font = font1,width=16                 )
		self.entry_ligne[14].grid(row=15,column=0,columnspan=1,padx=padx_place,sticky=W)
		
		Label(fen2,text="Couleur Config ",anchor="w",width=21,font = font1).grid(row=16,padx=5,sticky=W)  ## 14
		self.entry_ligne[15] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[15].grid(row=16,column=0,columnspan=1,padx=padx_place,sticky=W)
				
		Label(fen2,text="Couleur Sauvegarde ",anchor="w",width=21,font = font1 ).grid(row=17,column=0,padx=5,sticky=W)  ##  15
		self.entry_ligne[16] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[16].grid(row=17,column=0,columnspan=1,padx=padx_place,sticky=W) 
		
		Label(fen2,text="Dimension Planning ",anchor="w",width=21,font = font1 ).grid(row=18,column=0,padx=5,sticky=W)  ##  15
		self.entry_ligne[17] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[17].grid(row=18,column=0,columnspan=1,padx=padx_place,sticky=W) 
		
		Label(fen2,text="Couleur Planning ",anchor="w",width=21,font =font1 ).grid(row=19,column=0,padx=5,sticky=W)  ##  15
		self.entry_ligne[18] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[18].grid(row=19,column=0,columnspan=1,padx=padx_place,sticky=W)
		
		Label(fen2,text="Couleur Alerte ",anchor="w",width=21,font = font1 ).grid(row=20,column=0,padx=5,sticky=W)  ##  15
		self.entry_ligne[19] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[19].grid(row=20,column=0,columnspan=1,padx=padx_place,sticky=W)
		###
		Label(fen2,text="Couleur Graphique ",anchor="w",width=21,font = font1 ).grid(row=21,column=0,padx=5,sticky=W)  ##  15
		self.entry_ligne[20] = tkinter.Entry(fen2,font = font1,width=16)
		self.entry_ligne[20].grid(row=21,column=0,columnspan=1,padx=padx_place,sticky=W)

		#Label(fen2,text="Position Graphique ",anchor="w",width=21,font = font1 ).grid(row=22,column=0,padx=5,sticky=W)  ##  15
		#self.entry_ligne[21] = tkinter.Entry(fen2,font = font1,width=10)
		#self.entry_ligne[21].grid(row=22,column=0,columnspan=1,padx=padx_place,sticky=W)

		Label(fen2,text="Placement widget",anchor="w",width=21,fg='brown',font ='("Arial 12 bold').grid(row=25,column=0,padx=5,sticky=W)  ##  15
		Label(fen2,text="Gauche",anchor="w",width=21,font = font1 ).grid(row=26,column=0,padx=26,sticky=W)  ##  15
		
		self.entry_ligne[21] = tkinter.Entry(fen2,font = font1,width=6)
		self.entry_ligne[21].grid(row=26,column=0,columnspan=1,padx=padx_place,sticky=W)
		Label(fen2,text="Haut",anchor="w",width=21,font = font1 ).grid(row=27,column=0,padx=25,sticky=W)  ##  15
		self.entry_ligne[22] = tkinter.Entry(fen2,font = font1,width=6)
		self.entry_ligne[22].grid(row=27,column=0,columnspan=1,padx=padx_place,sticky=W)

		Label(fen2,text="Livre de Banque",fg='brown',font = '("Arial 12 bold').grid(row=30,column=0,padx=105,ipady=15,sticky=W)
		Label(fen2,text="Icon Banque 1",anchor="w",font = 'Arial 12' ).grid(row=31,column=0,padx=5,sticky=W)
		def choixLD(event):													# Fonction associée à la liste déroulante
			pass
		svCombo = StringVar()	
		path_es =chem_parametre +'Bank/'
		files = os.listdir(path_es)
		
		self.entry_ligne[23] = ttk.Combobox(fen2,font = 'Arial 11',width=20, textvariable=svCombo)
		self.entry_ligne[23].grid(row=31,column=0,padx=200,sticky=W)
		self.entry_ligne[23]['values'] = files	# Liste des valeurs de la liste
		self.entry_ligne[23].current(0)										# Choix de la valeur courante
		self.entry_ligne[23].bind('<<ComboboxSelected>>', choixLD)	# Action déclenchée par une sélection dans la liste 

		crearor = tkinter.Label(fen2,text="Banque 1",anchor="w",width=17,font = 'Arial 12' ) .grid(row=32,column=0,padx=5,sticky=W)
		self.entry_ligne[24] = tkinter.Entry(fen2,font = 'Arial 12',width=19)
		self.entry_ligne[24].grid(row=32,column=0,padx=200,sticky=W)
		
		crearor = tkinter.Label(fen2,text="Solde Banque 1",anchor="w",width=17,font = 'Arial 12' ).grid(row=33,column=0,padx=5,sticky=W)
		self.entry_ligne[25] = tkinter.Entry(fen2,font = 'Arial 12',width=19)
		self.entry_ligne[25].grid(row=33,column=0,padx=200,sticky=W)

		crearor = tkinter.Label(fen2,text='').grid(row=34,column=0,padx=5,sticky=W)
		crearor = tkinter.Label(fen2,text="Icon Banque 2",anchor="w",width=17,font = 'Arial 12' ).grid(row=35,column=0,padx=5,sticky=W)
		def choixLD(event):													# Fonction associée à la liste déroulante
			pass
		svCombo = StringVar()		# Variable de type str associée à la valeur affichée dans la liste
		path_es = chem_parametre +'Bank/'
		files = os.listdir(path_es)
		self.entry_ligne[26] = ttk.Combobox(fen2,font = 'Arial 11',width=20, textvariable=svCombo)
		self.entry_ligne[26].grid(row=35,column=0,padx=200,sticky=W)
		self.entry_ligne[26]['values'] = files	# Liste des valeurs de la liste
		self.entry_ligne[26].current(0)										# Choix de la valeur courante
		self.entry_ligne[26].bind('<<ComboboxSelected>>', choixLD)	# Action déclenchée par une sélection dans la liste 

		crearor = tkinter.Label(fen2,text="Banque 2",anchor="w",width=17,font = 'Arial 12' ).grid(row=36,column=0,padx=5,sticky=W)
		self.entry_ligne[27] = tkinter.Entry(fen2,font = 'Arial 12',width=19)
		self.entry_ligne[27].grid(row=36,column=0,padx=200,sticky=W)
		
		crearor = tkinter.Label(fen2,text="Solde Banque 2",anchor="w",width=17,font = 'Arial 12' ) .grid(row=37,column=0,padx=5,sticky=W)
		self.entry_ligne[28] = tkinter.Entry(fen2,font = 'Arial 12',width=19)
		self.entry_ligne[28].grid(row=37,column=0,padx=200,sticky=W)
				
		Label(fen2).grid(row=38)
		b1 = Button(fen2,text='Libéllé',font=("bold",10,), command=Alimentation_liste)
		b1.grid(row=39,column=0,padx=2,pady=5,sticky=W)
		b2 = Button(fen2,text='Mode Paiement', font=("bold",10,),command=tools_bank)
		b2.grid(row=39,column=0,columnspan=1,padx=73,sticky=W)
		b3 = Button(fen2,text='Note',font=("bold",10,),command=notes)
		b3.grid(row=39,column=0,columnspan=1,padx=204,sticky=W)
		b4 = Button(fen2,text='Genre',font=("bold",10,),command=genre)
		b4.grid(row=39,column=0,columnspan=1,padx=264,sticky=W)
		b5 = Button(fen2,text='Détail banquaires',font=("bold",10,),width=13,command=Détail_banquaires_1)
		b5.grid(row=40,column=0,padx=2,sticky=W)
		#b6 = Button(fen2,text='Détail banquaires 2',font=("bold",10,),width=13,command=Détail_banquaires_2)
		#b6.grid(row=40,column=0,columnspan=1,padx=148,sticky=W)
		b7 = Button(fen2,text='Echéances',font=("bold",10,),width=7,command=echeances)
		b7.grid(row=40,column=0,columnspan=1,padx=148,sticky=W)
		Label(fen2,text="Gestion Stock",fg='brown',font = '("Arial 12 bold').grid(row=41,column=0,padx=105,ipady=15,sticky=W)
		b8 = Button(fen2,text='Famille',font=("bold",10,), command=familles_liste)
		b8.grid(row=42,column=0,padx=2,pady=5,sticky=W)
		b9 = Button(fen2,text='Marque article',font=("bold",10,), command=familles_marque)
		b9.grid(row=42,column=0,columnspan=1,padx=80,sticky=W)
		b10 = Button(fen2,text='Conditionnement',font=("bold",10,), command=familles_conditionnement)
		b10.grid(row=42,column=0,columnspan=1,padx=205,sticky=W) 
		b11 = Button(fen2,text='Dictionnaire désignation',font=("bold",10,), command=désignation)
		b11.grid(row=43,column=0,padx=2,sticky=W) 
		
		crearor = tkinter.Label(fen2,text=" Raccourcis PDF",width=15,fg='brown',font = '("Arial 12 bold').grid(row=53,column=0,padx=105,ipady=15,sticky=W)
		self.entry_ligne[29] = tkinter.Entry(fen2,font = font1,width=18)
		self.entry_ligne[29].grid(row=54,column=0,padx=5,sticky=W)
		self.entry_ligne[30] = tkinter.Entry(fen2,font = font1,width=18)
		self.entry_ligne[30].grid(row=55,column=0,padx=5,sticky=W)
		self.entry_ligne[31] = tkinter.Entry(fen2,font =font1,width=18)
		self.entry_ligne[31].grid(row=56,column=0,padx=5,sticky=W)
		
		self.entry_ligne[32] = tkinter.Entry(fen2,font = font1,width=18)
		self.entry_ligne[32].grid(row=54,column=0,padx=200,sticky=W)
		self.entry_ligne[33]= tkinter.Entry(fen2,font = font1,width=18)
		self.entry_ligne[33].grid(row=55,column=0,padx=200,sticky=W)
		self.entry_ligne[34] = tkinter.Entry(fen2,font = font1,width=18)
		self.entry_ligne[34].grid(row=56,column=0,padx=200,sticky=W)
		
		cocher01 = tkinter.Label(fen2,text="Intervention Dépannage",fg='brown',font = '("Arial 12 bold').grid(row=57,column=0,columnspan=1,padx=10,ipadx=60,ipady=15,sticky=W)
		label = tkinter.Label(fen2,text="Désignation",relief=SUNKEN,bg ='#CDD8FA', fg = "black",font = ("Times",12,"bold"), bd = 2)
		label.grid(row=59,column=0,columnspan=1,ipadx=134,padx=5,sticky=W)
		label = tkinter.Label(fen2,text="Tarif UHT",relief=SUNKEN,bg ='#CDD8FA',width=10, fg = "black",font = ("Times",12,"bold"), bd = 2)
		label.grid(row=59,column=0,padx=360,sticky=W)
		
		self.entry_ligne[35] = tkinter.Entry(fen2,font = 'Arial 11',width=43)
		self.entry_ligne[35].grid(row=60,column=0,padx=5,ipadx=2,sticky=W)
		self.entry_ligne[36] = tkinter.Entry(fen2,font = 'Arial 11',width=10)
		self.entry_ligne[36].grid(row=60,column=0,columnspan=1,padx=360,sticky=W)
		self.entry_ligne[37] = tkinter.Entry(fen2,font = 'Arial 11',width=43)
		self.entry_ligne[37].grid(row=61,column=0,padx=5,ipadx=2,sticky=W) ##
		self.entry_ligne[38] = tkinter.Entry(fen2,font = 'Arial 11',width=10)
		self.entry_ligne[38].grid(row=61,column=0,columnspan=1,padx=360 ,sticky=W) 
		
		self.entry_ligne[39] = tkinter.Entry(fen2,font = 'Arial 11',width=43)
		self.entry_ligne[39].grid(row=62,column=0,padx=5,ipadx=2,sticky=W) 
		self.entry_ligne[40] = tkinter.Entry(fen2,font = 'Arial 11',width=10)
		self.entry_ligne[40].grid(row=62,column=0,columnspan=1,padx=360 ,sticky=W)
		 
		crearor = tkinter.Label(fen2,text="Chemin de Sauvegarde",anchor="w",width=22,fg='brown',font = 'Arial 12 bold' ).grid(row=63,column=0,sticky=W)
		self.entry_ligne[41] = tkinter.Entry(fen2,font = 'Arial 12',width=49)
		self.entry_ligne[41].grid(row=64,column=0,columnspan=5,sticky=W)
		
		crearor = tkinter.Label(fen2,text="Sauvegarde amovible",anchor="w",width=22,fg='brown',font = 'Arial 12 bold' ).grid(row=65,column=0,sticky=W)
		self.entry_ligne[42] = tkinter.Entry(fen2,font = 'Arial 12',width=49)
		self.entry_ligne[42].grid(row=66,column=0,columnspan=5,sticky=W)
		
		Label(fen2,text=" Chemin (Facture Devis Avoir Inter....) ",anchor="w",fg='brown',font ='("Arial 12 bold').grid(row=67,column=0,ipady=10,sticky=W)
		Label(fen2,text="Gestion_Kiwi_AE/Gestion/Devis/Devis_PDF_",anchor="e",bg='#F6F6D0',width=50,font ='("Arial 10').grid(row=68,column=0,padx=0,ipady=0,sticky=W)
		self.entry_ligne[43] = tkinter.Entry(fen2,font = 'Arial 11',width=5)
		self.entry_ligne[43].grid(row=68,column=0,columnspan=5,padx=405,sticky=W)
		##
		Label(fen2,text="Gestion_Kiwi_AE/Gestion/Devis/Devis_brut_",anchor="e",bg='#F6F6D0',width=50,font ='("Arial 10').grid(row=69,column=0,padx=0,ipady=0,sticky=W)
		self.entry_ligne[44] = tkinter.Entry(fen2,font = 'Arial 11',width=5)
		self.entry_ligne[44].grid(row=69,column=0,columnspan=5,padx=405,sticky=W)
		
		Label(fen2,text="Gestion_Kiwi_AE/Gestion/Facture/Facture_PDF_",anchor="e",bg='#F6F6D0',width=50,font ='("Arial 10').grid(row=70,column=0,padx=0,ipady=0,sticky=W)
		self.entry_ligne[45] = tkinter.Entry(fen2,font = 'Arial 11',width=5)
		self.entry_ligne[45].grid(row=70,column=0,columnspan=5,padx=405,sticky=W)
		
		Label(fen2,text="Gestion_Kiwi_AE/Gestion/Facture/Facture_brut_",anchor="e",bg='#F6F6D0',width=50,font ='("Arial 10').grid(row=71,column=0,padx=0,ipady=0,sticky=W)
		self.entry_ligne[46] = tkinter.Entry(fen2,font = 'Arial 11',width=5)
		self.entry_ligne[46].grid(row=71,column=0,columnspan=5,padx=405,sticky=W)
		
		Label(fen2,text="Gestion_Kiwi_AE/Gestion/Avoir/Avoir_PDF_",anchor="e",bg='#F6F6D0',width=50,font ='("Arial 10').grid(row=72,column=0,padx=0,ipady=0,sticky=W)
		self.entry_ligne[47] = tkinter.Entry(fen2,font = 'Arial 11',width=5)
		self.entry_ligne[47].grid(row=72,column=0,columnspan=5,padx=405,sticky=W)
		
		Label(fen2,text="Gestion_Kiwi_AE/Gestion/Avoir/Avoir_brut_",anchor="e",bg='#F6F6D0',width=50,font ='("Arial 10').grid(row=73,column=0,padx=0,ipady=0,sticky=W)
		self.entry_ligne[48] = tkinter.Entry(fen2,font = 'Arial 11',width=5)
		self.entry_ligne[48].grid(row=73,column=0,columnspan=5,padx=405,sticky=W)

		Label(fen2,text=" Gestion_Kiwi_AE/Gestion/InterVention/Intervention_PDF_",anchor="e",bg='#F6F6D0',width=50,font ='("Arial 10').grid(row=74,column=0,padx=0,ipady=0,sticky=W)
		self.entry_ligne[49] = tkinter.Entry(fen2,font = 'Arial 11',width=5)
		self.entry_ligne[49].grid(row=74,column=0,columnspan=5,padx=405,sticky=W)
		
		Label(fen2,text=" Gestion_Kiwi_AE/Gestion/InterVention/Intervention_brut_",anchor="e",bg='#F6F6D0',width=50,font ='("Arial 10').grid(row=75,column=0,padx=0,ipady=0,sticky=W)
		self.entry_ligne[50] = tkinter.Entry(fen2,font = 'Arial 11',width=5)
		self.entry_ligne[50].grid(row=75,column=0,columnspan=5,padx=405,sticky=W)
		
		Label(fen2,text="Gestion_Kiwi_AE/Recette_PDF/Banque_PDF_",anchor="e",bg='#F6F6D0',width=50,font ='("Arial 10').grid(row=76,column=0,padx=0,ipady=0,sticky=W)
		self.entry_ligne[51] = tkinter.Entry(fen2,font = 'Arial 11',width=5)
		self.entry_ligne[51].grid(row=76,column=0,columnspan=5,padx=405,sticky=W)
		
		Label(fen2,text="Gestion_Kiwi_AE/Gestion/Recette/Banque_ods/Banque_ods_",anchor="e",bg='#F6F6D0',width=50,font ='("Arial 10').grid(row=77,column=0,padx=0,ipady=0,sticky=W)
		self.entry_ligne[52] = tkinter.Entry(fen2,font = 'Arial 11',width=5)
		self.entry_ligne[52].grid(row=77,column=0,columnspan=5,padx=405,sticky=W)
		
		Passe = Button(fen2,text="Mot de passe",anchor="w",fg='black',command=modifier_pass,font ='("Arial 12')
		Passe.grid(row=78,column=0,padx=0,pady=10,sticky=W)
		#########################
		
		Label(fen2,text=" Affichage ",anchor="w",fg='brown',font ='("Arial 12').grid(row=80,column=0,ipady=5,sticky=W)
		
		Label(fen2,text=" Votre Ville pour météo",anchor="w",font ='("Arial 12').grid(row=81,column=0,ipady=5,sticky=W)
		self.entry_ligne[53] = tkinter.Entry(fen2,font = 'Arial 12',width=25)
		self.entry_ligne[53].grid(row=81,column=0,columnspan=5,padx=200,sticky=W)
		
		Label(fen2,text="Fond écran",anchor="w",width=17,font = 'Arial 12' ) .grid(row=82,column=0,padx=5,sticky=W)
		def choixLD(event):													# Fonction associée à la liste déroulante
			pass
		svCombo = StringVar()		# Variable de type str associée à la valeur affichée dans la liste
		path_es = chem_parametre +'Font_ecran/'
		files = os.listdir(path_es)
		self.entry_ligne[54] = ttk.Combobox(fen2,font = 'Arial 11',width=26, textvariable=svCombo)
		self.entry_ligne[54].grid(row=82,column=0,padx=200,ipadx=2,sticky=W)
		self.entry_ligne[54]['values'] = files	# Liste des valeurs de la liste
		self.entry_ligne[54].current(0)										# Choix de la valeur courante
		self.entry_ligne[54].bind('<<ComboboxSelected>>', choixLD)	# Action déclenchée par une sélection dans la liste 
		
		Label(fen2,text="Carte de visite",anchor="w",width=17,fg='brown',font ='("Arial 12 bold').grid(row=84,column=0,padx=5,sticky=W)
		Label(fen2,text="Chemin",anchor="w",font = 'Arial 12' ) .grid(row=86,column=0,padx=5,sticky=W)

		self.entry_ligne[55] = tkinter.Entry(fen2,font = 'Arial 11',width=44)
		self.entry_ligne[55].grid(row=86,column=0,columnspan=5,padx=65,pady=5,ipadx=1,sticky=W)
		f = open(chem_config , 'r')
		ligne49 = f.readlines()[54]
		if ligne49[-1] == "\n":
			ligne49 = ligne49[:-1]
		carte=LabelFrame(fen2,bd=3,relief=RAISED,font=("Arial",10))
		carte.grid(row=87,column=0,padx=5,ipadx=197,ipady=80,sticky=W)
		
		def Kiwi_AE(): # Stock recette
			try:	
				global carte_pro
				photo = PhotoImage(file='/home/'+Nnom+'/'+ligne49)
				photo = photo.subsample(1,1)
				carte_pro = Button(fen2,image = photo)
				carte_pro.image = photo
				carte_pro.grid(row=87,column=0,padx=5,pady=5,sticky=W)
			except:
				label=Label(fen2,text="Chemin carte invalide (Format PNG, GIF rester \ndans le cadre réglage d'image largeur: 388 px \nhauteur 154 px. les réglages peut-être faits avec gimp " ,anchor="w",font=("Times New Roman", 12, "bold"),fg='red' )
				label.grid(row=89,column=0,padx=15,sticky=W)
		Kiwi_AE()
		Label(fen2).grid(row=90,column=0,padx=5,ipady=2)
		self.ligne54 = IntVar()
		self.entry_ligne54 = Checkbutton(fen2, text="Graphique",font ='("Arial 12',anchor="w",width=15,justify=tkinter.LEFT, variable=self.ligne54)
		self.entry_ligne54.grid(row=91,column=0,sticky=W)			
		
		self.ligne55= IntVar()
		self.entry_ligne55 = Checkbutton(fen2, text="Dossier Facture",font ='("Arial 12',anchor="w",width=15,justify=tkinter.LEFT, variable=self.ligne55)
		self.entry_ligne55.grid(row=92,column=0,sticky=W)
		
		self.ligne56= IntVar()
		self.entry_ligne56 = Checkbutton(fen2, text="Dossier Devis",font ='("Arial 12',anchor="w",width=15,justify=tkinter.LEFT, variable=self.ligne56)
		self.entry_ligne56.grid(row=93,column=0,sticky=W)
		
		self.ligne57= IntVar()
		self.entry_ligne57 = Checkbutton(fen2, text="Dossier Avoir",font ='("Arial 12',anchor="w",width=15,justify=tkinter.LEFT, variable=self.ligne57)
		self.entry_ligne57.grid(row=94,column=0,sticky=W)
		
		self.ligne58= IntVar()
		self.entry_ligne58 = Checkbutton(fen2, text="Dossier Banque",font ='("Arial 12',anchor="w",width=15,justify=tkinter.LEFT, variable=self.ligne58)
		self.entry_ligne58.grid(row=95,column=0,sticky=W)
		
		self.ligne59= IntVar()
		self.entry_ligne59 = Checkbutton(fen2, text="Rendez_vous",font ='("Arial 12',anchor="w",width=15,justify=tkinter.LEFT, variable=self.ligne59)
		self.entry_ligne59.grid(row=96,column=0,sticky=W)
		
		self.ligne60= IntVar()
		self.entry_ligne60 = Checkbutton(fen2, text="Rappel annuel anniversaire",font ='("Arial 12',anchor="w",justify=tkinter.LEFT, variable=self.ligne60)
		self.entry_ligne60.grid(row=97,column=0,sticky=W)
		
		self.ligne61= IntVar()
		self.entry_ligne61 = Checkbutton(fen2, text='Bienvenue',font ='("Arial 12',anchor="w",justify=tkinter.LEFT, variable=self.ligne61)
		self.entry_ligne61.grid(row=98,column=0,sticky=W)
		
				
		Label(fen2,text="").grid(row=106,column=0,ipady=0,sticky=W)
	def color_button(self):
		color = tkinter.colorchooser.askcolor(parent=fen2)
	
	def submit(self):# chem_config
		listes_config = open(chem_config , "w")
		text = ""
		for x in range(1,56):	
			text += "{}".format(self.entry_ligne[x].get()) + "\n"
			x=x+1
		text += "{}".format(self.ligne54.get()) + "\n"
		text += "{}".format(self.ligne55.get()) + "\n"
		text += "{}".format(self.ligne56.get()) + "\n"
		text += "{}".format(self.ligne57.get()) + "\n"
		text += "{}".format(self.ligne58.get()) + "\n"
		text += "{}".format(self.ligne59.get()) + "\n"
		text += "{}".format(self.ligne60.get()) + "\n"
		text += "{}".format(self.ligne61.get()) + "\n"
		listes_config.write(text)
		listes_config.close()
	
	def submit_000(self):# chem_config
		messagebox.showinfo("Fichier de copie" ,"Si vous avait un problème /Gestion_Kiwi_AE/Parametres/Note/Config_Kiwi_AE.000 renommer et écraser par Config_Kiwi_AE ",parent=config)	
		listes_config = open(chem_config_secours , "w")
		text = ""
		for x in range(1,56):	
			text += "{}".format(self.entry_ligne[x].get()) + "\n"
			x=x+1
		text += "{}".format(self.ligne54.get()) + "\n"
		text += "{}".format(self.ligne55.get()) + "\n"
		text += "{}".format(self.ligne56.get()) + "\n"
		text += "{}".format(self.ligne57.get()) + "\n"
		text += "{}".format(self.ligne58.get()) + "\n"
		text += "{}".format(self.ligne59.get()) + "\n"
		text += "{}".format(self.ligne60.get()) + "\n"
		text += "{}".format(self.ligne61.get()) + "\n"
		listes_config.write(text)
		listes_config.close()

	def Open(self):
		listes_config = open(chem_config, 'r')
		for x in range(1,56):
			char_ligne = listes_config.readline()
			self.entry_ligne[x].insert(0,char_ligne[:len(char_ligne)-1])
			x=x+1
		char_ligne = listes_config.readline()
		if eval(char_ligne,{},{}) :self.entry_ligne54.select()
		char_ligne = listes_config.readline()
		if eval(char_ligne,{},{}) :self.entry_ligne55.select()
		char_ligne = listes_config.readline()
		if eval(char_ligne,{},{}) :self.entry_ligne56.select()
		char_ligne = listes_config.readline()
		if eval(char_ligne,{},{}) :self.entry_ligne57.select()
		char_ligne = listes_config.readline()
		if eval(char_ligne,{},{}) :self.entry_ligne58.select()
		char_ligne = listes_config.readline()
		if eval(char_ligne,{},{}) :self.entry_ligne59.select()
		char_ligne = listes_config.readline()
		if eval(char_ligne,{},{}) :self.entry_ligne60.select()
		char_ligne = listes_config.readline()
		if eval(char_ligne,{},{}) :self.entry_ligne61.select()
		listes_config.close()
def LireGuid():
	global chem_config
	global ligne, IndexDroite
	try:
		maconfig = open(chem_config, 'r')
	except:
		messagebox.showerror("Vous avait un problème config" ,"/Gestion_Kiwi_AE/Parametres/Note/Config_Kiwi_AE.000 \nrenommer et écraser par Config_Kiwi_AE ")	
	x=[]
	ligne = []
	for x in range(1,57):
		char_ligne = maconfig.readline()
		ligne.append(char_ligne[:len(char_ligne)-1])
	char_ligne = maconfig.readline()
	ligne54=char_ligne[:len(char_ligne)-1]
	char_ligne = maconfig.readline()
	ligne55=char_ligne[:len(char_ligne)-1]
	char_ligne = maconfig.readline()
	ligne56=char_ligne[:len(char_ligne)-1]
	char_ligne = maconfig.readline()
	ligne57=char_ligne[:len(char_ligne)-1]
	char_ligne = maconfig.readline()
	ligne58=char_ligne[:len(char_ligne)-1]
	char_ligne = maconfig.readline()
	ligne59=char_ligne[:len(char_ligne)-1]
	char_ligne = maconfig.readline()
	ligne60=char_ligne[:len(char_ligne)-1]
	char_ligne = maconfig.readline()
	ligne61=char_ligne[:len(char_ligne)-0]
	
	maconfig.close()
LireGuid()
def modifier_pass():
	global affich_Pas
	mot_passe ()
	affich_Pas = Label(fen2,text= '',fg='brown',width=30,height=2)#,font ='("Arial 12 bold')
	affich_Pas.grid(row=78,column=0,padx=200,ipadx=2,sticky=W)
	
	fiche = chem_parametre +'Note/Pass_Kiwi_AE'
	def sauvegarder():
		open(fiche, "w").write(text.get(0., END))
	text = Text(affich_Pas,font=('Arial 15'),bg ='White',fg='black')
	text.insert(1.,open(fiche,'r').read())
	text.place(x=1,y=0,height=65,width=250)
	
	photo = PhotoImage(file= chem_icon +'Ok.png')
	photo = photo.subsample(8,8)
	B = Button(text, image = photo,command = sauvegarder, compound=BOTTOM)
	B.image = photo
	B.place(x = 180, y =0,width=30, height=30)
	
	photo = PhotoImage(file= chem_icon +'exit.png')
	photo = photo.subsample(1,1)
	Ba = Button(text, image = photo,command=text.destroy, compound=BOTTOM)
	Ba.image = photo
	Ba.place(x = 211, y =0,width=30, height=30)
def give_passwor():
	global password1,passwor
	passe = (chem_parametre +'Note/Pass_Kiwi_AE')
	f = open(passe, "r")
	passwor = f.readline().replace("\n","")
	#print(passwor)
	f.close()
	return passwor
def mot_passe ():
	login = ""
	password_ = give_passwor();
	while login != password_:
		login = simpledialog.askstring(title="Identifiant?",
						prompt="Mot de Passe Kiwi_AE?:",
						show="*",
						parent=fen2)
		if login == None:
			quit()
def bouton_1(config):
	global Ba,B,sauvegarder
	photo = PhotoImage(file= chem_icon +'exit.png')
	photo = photo.subsample(1,1)
	Ba = Button(config, image = photo,command=config.destroy, compound=BOTTOM)
	Ba.image = photo
	Ba.pack(side='right',anchor='n')	
	
	photo = PhotoImage(file= chem_icon +'Ok.png')
	photo = photo.subsample(8,8)
	B = Button(config, image = photo,command = sauvegarder, compound=BOTTOM)
	B.image = photo
	B.pack(side='right',anchor='n',ipady=1)
def bouton_2(config):
	global Ba,B,sauvegarder
	photo = PhotoImage(file= chem_icon +'Ok.png')
	photo = photo.subsample(7,7)
	B = Button(config, image = photo,command = sauvegarder, compound=BOTTOM)
	B.image = photo
	B.place(relx=0.4,rely=0.8)
	
	photo = PhotoImage(file= chem_icon +'exit.png')
	photo = photo.subsample(1,1)
	Ba = Button(config, image = photo,command=config.destroy, compound=BOTTOM)
	Ba.image = photo
	Ba.place(relx=0.7,rely=0.8)
def familles_marque():
	global ligne,Ba,B,sauvegarder
	config = tkinter.Toplevel()
	config.geometry("140x230+960+150")
	config.overrideredirect(1)
	wid,hei =40,40
	def sauvegarder():
		open(chem_parametre +"Note/Famille_Marque", "w").write(text.get(0., END))
	fichier = open(chem_parametre + "Note/Famille_Marque",'r')
	text = Text(config,font='Arial 10')
	text.insert(1.,open(chem_parametre +"Note/Famille_Marque",'r').read())
	bouton_2(config)
	text.pack(anchor=N)	
def familles_liste():
	global ligne,Ba,B,sauvegarder
	config = tkinter.Toplevel()
	config.geometry("140x190+960+150")
	config.overrideredirect(1)
	wid,hei =40,40
	def sauvegarder():
		open(chem_parametre +"Note/Famille_article", "w").write(text.get(0., END))
	fichier = open(chem_parametre + "Note/Famille_article",'r')
	text = Text(config,font='Arial 10')
	text.insert(1.,open(chem_parametre +"/Note/Famille_article",'r').read())
	bouton_2(config)
	text.pack()
def Alimentation_liste():
	global ligne,Ba,B,sauvegarder
	config = tkinter.Toplevel()
	config.geometry("230x230+720+150")
	config.overrideredirect(1)
	wid,hei =40,40
	def sauvegarder():
		open(chem_parametre + "Note/Nature", "w").write(text.get(0., END))
	fichier = open(chem_parametre + "Note/Nature",'r')
	text = Text(config,font='Arial 10')
	text.insert(1.,open(chem_parametre + "Note/Nature",'r').read())
	bouton_2(config)	
	text.pack()
def notes():
	global ligne,Ba,B,sauvegarder
	config = tkinter.Toplevel()
	config.geometry("140x190+960+400")
	config.overrideredirect(1)
	config.configure(bg='#E5E5E5')
	wid,hei =40,40
	def sauvegarder():
		open(chem_parametre + "Note/Note_effets", "w").write(text.get(0., END))
	fichier = open(chem_parametre + "Note/Note_effets",'r')
	text = Text(config,font='Arial 10')
	print (chem_parametre ,"Note/Note_effets")
	text.insert(1.,open(chem_parametre +"Note/Note_effets",'r').read())
	bouton_2(config)	
	
	text.pack()
def Détail_banquaires_1():
	global ligne,Ba,B,sauvegarder
	config = tkinter.Toplevel()
	config.geometry("500x40+960+150")
	config.overrideredirect(1)
	wid,hei =40,40
	def sauvegarder():
		open(chem_parametre +"Note/banquaires", "w").write(text.get(0., END))
	
	fichier = open(chem_parametre +"Note/banquaires",'r')
	text = Text(config,font='Arial 10')
	text.insert(1.,open(chem_parametre +"Note/banquaires",'r').read())
	bouton_1(config)
	
	text.pack(side=LEFT)
def echeances():
	global Ba,B,sauvegarder
	config = tkinter.Toplevel()
	config.geometry("190x190+960+150")
	config.overrideredirect(1)
	wid,hei =40,40
	def sauvegarder():
		open(chem_parametre + "Note/echeances", "w").write(text.get(0., END))
	
	fichier = open(chem_parametre + "Note/echeances",'r')
	text = Text(config,font='Arial 10')
	text.insert(1.,open(chem_parametre +"Note/echeances",'r').read())
	bouton_2(config)	
	text.pack()
def genre():
	global Ba,B,sauvegarder
	config = tkinter.Toplevel()
	config.geometry("140x190+960+150")
	config.overrideredirect(1)
	wid,hei =40,40
	def sauvegarder():
		open(chem_parametre +"Note/genre", "w").write(text.get(0., END))
	
	fichier = open(chem_parametre +"Note/genre",'r')
	text = Text(config,font='Arial 10')
	text.insert(1.,open(chem_parametre +"Note/genre",'r').read())
	bouton_2(config)	
	text.pack()
def tools_bank():
	global Ba,B,sauvegarder
	config = tkinter.Toplevel()
	config.geometry("200x247+960+150")
	config.overrideredirect(1)
	wid,hei =40,40
	def sauvegarder():
		open(chem_parametre + "Note/tools_bank", "w").write(text.get(0., END))
	fichier = open(chem_parametre +"Note/tools_bank",'r')
	text = Text(config,bg='#E5E5E5',font='Arial 10')
	text.insert(1.,open(chem_parametre +"Note/tools_bank",'r').read())
	bouton_2(config)	
	text.pack()
def familles_conditionnement():
	global Ba,B,sauvegarder
	config = tkinter.Toplevel()
	config.geometry("140x185+960+150")
	config.overrideredirect(1)
	wid,hei =35,35
	def sauvegarder():
		open(chem_parametre +"Note/Conditionnement", "w").write(text.get(0., END))
	
	fichier = open(chem_parametre +"Note/Conditionnement",'r')
	text = Text(config,font='Arial 10')
	text.insert(1.,open(chem_parametre +"Note/Conditionnement",'r').read())
	bouton_2(config)
	text.pack()
def désignation():
	global Ba,B,sauvegarder
	config = tkinter.Toplevel()
	config.geometry("440x190+960+150")
	config.overrideredirect(1)
	wid,hei =35,35
	def sauvegarder():
		open(chem_parametre +"Note/designation", "w").write(text.get(0., END))
	
	fichier = open(chem_parametre +"Note/designation",'r')
	text = Text(config,font='Arial 10')
	text.insert(1.,open(chem_parametre +"Note/designation",'r').read())
	bouton_2(config)
	text.pack()
def del_Poupmenu ():
	showMenu.destroyer()
def copy():
	fen2.clipboard_clear()
	fen2.clipboard_append(fen2.selection_get())
def coller():
	t=fen2.selection_get(selection='CLIPBOARD')
	fen2.focus_get().insert(INSERT,t) 
def Poupmenu (config):
	def showMenu(e):
		global showMenu
		pmenu.post(e.x_root, e.y_root)
	pmenu = tkinter.Menu(config,font = 'Arial 14',relief=SUNKEN, fg = "black", tearoff=0)	
	pmenu.add_command(label="Copier",command=copy)
	pmenu.add_command(label="Coller",command=coller)
	pmenu.add_command(label="Exit Kiwi_AE", command=config.destroy)
	config.bind("<Button-3>", showMenu)
def aide_config():
	try:
		config.wm_attributes('-topmost', False)
		os.system('xdg-open'+ ' /home/'+Nnom+'/Gestion_Kiwi_AE/Parametres/Aide_Kiwi_AE/Aide_Kiwi_AE.pdf')
	except:
		messagebox.showerror("Erreure","Pas de fichier ",parent=config)	
		#config.wm_attributes('-topmost', True)
'''
def bouton():
	photo = PhotoImage(file= chem_icon +'aide.png')
	photo = photo.subsample(4,4)
	#b1 = Button(Bt,image = photo,command= aide_config)
	b1.image = photo
		
	photo = PhotoImage(file= chem_icon +'Save.png')
	photo = photo.subsample(4,4)
	B = Button(Bt, image = photo,command = passPlusApp.submit)
	B.image = photo
	#B.place(x=312, y=1,height=60, width=60)
	
	photo = PhotoImage(file= chem_icon +'exit.png')
	photo = photo.subsample(1,1)
	Ba = Button(Bt, image = photo,command=config.destroy)
	Ba.image = photo
	Ba.place(x=1, y= 1,height=58, width=58)
'''
def Guid():
	global ligne ,fen2,color_button,ligne39,Bt,config
	config = tkinter.Toplevel()
	config.geometry("474x629+305+115")
	#config.bind('<Escape>',  lambda event:config.destroy())
	f = open(chem_config, 'r')
	ligne[15] = f.readlines()[14]
	if ligne[15][-1] == "\n":
		ligne[15] = ligne[15][:-1]
	try:
		config.configure(bg=ligne[15])
	except :
		messagebox.showinfo("Information","Couleur blanc par defaut\n Probléme voire configuration")  
		config.configure(bg='#FFFFFF')
		ligne[15]='#FFFFFF'
	
	config.title("           Configuration")
	#config.bind('<Escape>',  lambda event:config.destroy())
	config.config(relief = RAISED,bd=1 )
	#config.protocol('WM_DELETE_WINDOW', lambda: None)
	config.wm_attributes('-type', 'splash')
	config.attributes('-topmost', True)
	frameq = Frame(config) 
	frameq.place(width=475, height=550) 
	frame = SimpleScrolledFrame(frameq, 
			width=400, 
			borderwidth=1, 
			relief= SUNKEN, 
			background=ligne[15])
	frame.pack(fill= BOTH, expand=True) # fill window
	fen2 = frame.inner
	Poupmenu(config)

	passPlusApp = PassPlusApp(config)
	passPlusApp.Open()
	heiy = 550
	Bt = LabelFrame(config,bd=2,relief=RAISED,bg=ligne[15])
	Bt.pack(anchor='se',expand=True)
	
	photo = PhotoImage(file=chem_icon + 'Dublique.png')
	photo = photo.subsample(4,4)
	b1 = Button(Bt,image = photo,command= passPlusApp.submit_000,bg='#FFFFFF')
	b1.image = photo
	b1.grid(row=0,column=1,ipady=2,sticky='wns')

	photo = PhotoImage(file=chem_icon + 'aide.png')
	photo = photo.subsample(4,4)
	b1 = Button(Bt,image = photo,command= aide_config,bg='#FFFFFF')
	b1.image = photo
	b1.grid(row=0,column=2,ipady=2,sticky='wns')
	
	photo = PhotoImage(file=chem_icon +'Color.png')
	photo = photo.subsample(4,4)
	b1 = Button(Bt,image = photo,command= passPlusApp.color_button)
	b1.image = photo
	b1.grid(row=0,column=3,ipady=2,sticky='wns')
	
	photo = PhotoImage(file=chem_icon +'Save.png')
	photo = photo.subsample(4,4)
	B = Button(Bt, image = photo,command = passPlusApp.submit)
	B.image = photo
	B.grid(row=0,column=4,ipady=1,sticky='wns')

	photo = PhotoImage(file=chem_icon +'sortie.png')
	photo = photo.subsample(4,4)
	Ba = Button(Bt,image = photo,command=config.destroy)
	Ba.image = photo
	Ba.grid(row=0,column=5,ipady=1,sticky='wnse')
	config.mainloop()	
if __name__ == "__main__":
	Guid()