# coding: utf-8$
from tkinter import *
from tkinter.font import Font
import tkinter
from tkinter import messagebox
import os
from Config  import *
import lib.class_nline as ClnL
import saveLoadTier
import tkinter as tk
from tkinter import ttk
from lib.libAuxilaire import *

#from date import update_combobox
#pid = os.getpid()
#os.system("""/usr/bin/osascript -e 'tell application "System Events" to set frontmost of (first process whose unix id is %d) to true'""" % pid)
MIN_WHITE = 600
class SaisieClass():
	def __init__(self):
		
		photo = PhotoImage(file=chem_icon+'Ok.png')
		photo = photo.subsample(4,5)
		createFileButune=Button(fen2,fg='#FFFFFF',image = photo,command = self.addLigne,bg='white') 
		createFileButune.image = photo
		createFileButune.grid(row=0, column=0,ipadx=5,ipady=1,sticky=NW)
		
		label = tkinter.Button(fen2,text="Nom Prénon",command = tri_nom,width=23,relief=RAISED,activeforeground = 'red',font=("Times",10,'bold'),bg =ligne[8])
		label.grid(row=0, column=3,ipadx=2,sticky=NW)
		
		self.listeTiers = self.select()
		validate = (fen2.register(self._validate), '%P')    
		self.listeCombo = ttk.Combobox(fen2,width=25,font=("Times",10,'bold'),values = self.listeTiers,validatecommand=validate,validate='key')
		self.listeCombo.bind('<<ComboboxSelected>>', self.setTier)
		self.listeCombo.grid(row=0, column=3,ipady=3,sticky=SW)
		self.nom = self.listeCombo
		
		label = tkinter.Button(fen2,text="Adresse",width=27,relief=RAISED,font=("Times",11,'bold'),bg =ligne[8])
		label.grid(row=0, column=4,ipadx=7,sticky=NW)
		self.rue = tkinter.Entry(fen2,relief=RAISED,font=("Times",11),width=31)
		self.rue.grid(row=0, column=4,ipady=2,ipadx=4,sticky=SW)
		
		label = tkinter.Button(fen2,text="Adresse-Suite",width=26,relief=RAISED,font=("Times",11,'bold'),bg =ligne[8])
		label.grid(row=0, column=5,ipadx=3,pady=0,sticky=NW)
		self.rue2 = tkinter.Entry(fen2,width=29,relief=RAISED,font=("Times",11))#,font = (line1))
		self.rue2.grid(row=0, column=5,ipadx=3,ipady=2,sticky=SW) 
		
		label = tkinter.Button(fen2,text="Postal",width=3,relief=RAISED,font=("Times",11,'bold'),bg =ligne[8])
		label.grid(row=0, column=6,ipadx=0,pady=0,sticky=NW)
		self.codePostal = tkinter.Entry(fen2,width=6,relief=RAISED,font=("Times",11))
		self.codePostal.grid(row=0, column=6,ipady=2,ipadx=0,sticky=SW)
		
		label = tkinter.Button(fen2,text="Ville",width=21,relief=RAISED,command = tri_ville,activeforeground = 'red',font=("Times",11,'bold'),bg =ligne[8])
		label.grid(row=0, column=7,ipadx=1,sticky=NW)
		self.ville = tkinter.Entry(fen2,width=24,relief=RAISED,font=("Times",11))#,font = (line1))
		self.ville.grid(row=0, column=7,ipady=2,ipadx=1,sticky=SW)
		
		label = tkinter.Button(fen2,text="Portable",command = tri_portable,width=12,activeforeground = 'red',relief=RAISED,font=("Times",11,'bold'),bg =ligne[8])
		label.grid(row=0, column=8,sticky=NW)
		self.portable = tkinter.Entry(fen2,width=15,relief=RAISED,font=("Times",11)) #,font = (line1))
		self.portable.grid(row=0, column=8,ipady=2,ipadx=0,sticky=SW)
		
		label = tkinter.Button(fen2,text="Téléphone",command = tri_fix,width=12,activeforeground = 'red',relief=RAISED,font=("Times",11,'bold'),bg =ligne[8])
		label.grid(row=0, column=9,ipady=0,sticky=NW)
		self.fix = tkinter.Entry(fen2,width=15,relief=RAISED,font=("Times",11)) #,font = (line1))
		self.fix.grid(row=0, column=9,ipady=2,ipadx=0,sticky=SW)
		
		label = tkinter.Button(fen2,text="Email", width=28,relief=RAISED,font=("Times",11,'bold'),bg =ligne[8])
		label.grid(row=0, column=10,ipadx=5,sticky=NW)
		self.email = tkinter.Entry(fen2, width=32,relief=RAISED,font=("Times",11))
		self.email.grid(row=0, column=10,ipadx=2,ipady=2,sticky=SW)
		
		label = tkinter.Button(fen2,text="Fax",width=12,relief=RAISED,font=("Times",11,'bold'),bg =ligne[8])
		label.grid(row=0, column=11,ipadx=0,sticky=NW)
		self.fax = tkinter.Entry(fen2,width=14,relief=RAISED,font=("Times",11))
		self.fax.grid(row=0, column=11,ipady=2,ipadx=4,sticky=SW)
	### Bouton Fenetre N° 2 """"""""""""""
		wid = 50
		hei = 50
	def _validate(self,s):
		nv = [ x for x in self.listeTiers if x.startswith(s) ]
		if not nv in self.listeTiers:
			nv.append("ajouté : " + s)
		self.listeCombo['values'] = nv
		return True

	#@update_combobox
	def addLigne(self):
		ad_colone(
			nom = self.nom.get(),
			rue = self.rue.get(),
			rue2 = self.rue2.get(),
			codePostal = self.codePostal.get(), 
			ville = self.ville.get(), 
			mobil = self.portable.get(), 
			fix = self.fix.get(),
			email = self.email.get(),
			fax = self.fax.get()
			)
		
	def setTier(self,x):
		print(x)
		name = self.listeCombo.get()
		if not name in self.dicTier:
			name = name[9:]
			self.nom.delete(0,END)
			self.nom.insert(0, (str(name)))
			self.listeCombo.delete(0,END)
			self.listeCombo.insert(0, (str(name)))
			self.addLigne()
			self.listeTiers.append(name)
			return
		self.nom.delete(0,END)
		self.nom.insert(0, (str(self.dicTier[name]["nom"])))
		self.rue.delete(0,END)
		self.rue.insert(0, (str(self.dicTier[name]["rue"])))
		self.rue2.delete(0,END)
		self.rue2.insert(0, (str(self.dicTier[name]["rue2"])))
		self.codePostal.delete(0,END)
		self.codePostal.insert(0, (str(self.dicTier[name]["codePostal"])))
		self.ville.delete(0,END)
		self.ville.insert(0, (str(self.dicTier[name]["ville"])))
		self.portable.delete(0,END)
		self.portable.insert(0, (str(self.dicTier[name]["portable"])))
		self.fix.delete(0,END)
		self.fix.insert(0, (str(self.dicTier[name]["fix"])))
		self.email.delete(0,END)
		self.email.insert(0, (str(self.dicTier[name]["email"])))
		self.fax.delete(0,END)
		self.fax.insert(0, (str(self.dicTier[name]["fax"])))
	def select(self):
		global colone
		self.dicTier = {}
		listTier = []
		for x in colone:
			nom = x.nom.get()
			listTier.append(nom)
			self.dicTier[nom] = {
				"nom":x.nom.get(),
				"rue":x.rue.get(),
				"rue2":x.rue2.get(),
				"codePostal":x.codePostal.get(),
				"ville":x.ville.get(),
				"portable":x.mobil.get(),
				"fix":x.fix.get(),
				"email":x.email.get(),
				"fax":x.fax.get()
			}
		return listTier
#
def tri_nom():
	global colone
	dict_colone = {}
	for ligne in colone:dict_colone = {**dict_colone,**(ligne.paste_to_dict())}
	list_name_colone = sorted(dict_colone)
	colone = [dict_colone[index] for index in list_name_colone]
	for ligne in range(len(colone)):colone[ligne].replacer(ligne)
	for ligne in range(len(colone)):colone[ligne].coucouJeSuisLaFrame.forget()
	for ligne in range(len(colone)):colone[ligne].coucouJeSuisLaFrame.pack()	
def tri_portable():
	global colone
	dict_colone = {}
	for ligne in colone:ligne.normalize_format_mobile()
	for ligne in colone:dict_colone = {**dict_colone,**(ligne.paste_to_dict("mobil"))}
	list_name_colone = sorted(dict_colone)
	colone = [dict_colone[index] for index in list_name_colone]
	for ligne in range(len(colone)):colone[ligne].replacer(ligne)
	for ligne in range(len(colone)):colone[ligne].coucouJeSuisLaFrame.forget()
	for ligne in range(len(colone)):colone[ligne].coucouJeSuisLaFrame.pack()
def tri_fix():
	global colone
	dict_colone = {}
	for ligne in colone:ligne.normalize_format_mobile()
	for ligne in colone:dict_colone = {**dict_colone,**(ligne.paste_to_dict("fix"))}
	list_name_colone = sorted(dict_colone)
	colone = [dict_colone[index] for index in list_name_colone]
	for ligne in range(len(colone)):colone[ligne].replacer(ligne)
	for ligne in range(len(colone)):colone[ligne].coucouJeSuisLaFrame.forget()
	for ligne in range(len(colone)):colone[ligne].coucouJeSuisLaFrame.pack()
def tri_ville():
	global colone
	dict_colone = {}
	for ligne in colone:ligne.normalize_format_mobile()
	for ligne in colone:dict_colone = {**dict_colone,**(ligne.paste_to_dict("ville"))}
	list_name_colone = sorted(dict_colone)
	colone = [dict_colone[index] for index in list_name_colone]
	for ligne in range(len(colone)):colone[ligne].replacer(ligne)
	for ligne in range(len(colone)):colone[ligne].coucouJeSuisLaFrame.forget()
	for ligne in range(len(colone)):colone[ligne].coucouJeSuisLaFrame.pack()	
def Saisie():
	tier = SaisieClass()	
def action_recherche():
	select	= listeCombo.get()
	print('Votre recheche Tier', select)
#
def save():
	global colone,frame
	saveLoadTier.save(colone)
def load():
	global colone,frame
	saveLoadTier.load(colone,supre_colone_select,frame,frame_inter)
def aide_tier():
	global voire
	try:
		tier.wm_attributes('-topmost', False)
		voire = os.system('xdg-open '+ chem_aide + 'Tier.pdf')
	except :
		messagebox.showerror("Erreure","Pas de fichier ",parent=tier)    
		
	finally:		
		f.close()
		tier.wm_attributes('-topmost', True)
def main(sett,fenètrePrimère = None):
	global colone,frame, Conpteur,fen2,Haut2,Haut,info_zone,tier
	global nom,rue,rue2,codepostal,ville,mobil,fix,email,fax,ligne,frame_inter
	###################################### Button ###########################
	tier=tkinter.Toplevel(fenètrePrimère)
	try:
		tier.configure(bg=ligne[8])
	except :
		messagebox.showinfo("Information","Couleur blanc par defaut\n Probléme voire configuration")  
		tier.configure(bg='#FFFFFF')
		ligne[8]='#FFFFFF'
	try:
		x = fenètrePrimère.winfo_x()
		y = fenètrePrimère.winfo_y()
		w = int(ligne[9].split('x',1)[0]) 
		h = int(ligne[9].split('x',1)[1].split('+',1)[0])
		dx = ligne[20] #80
		dy = ligne[21]
		tier.geometry("%dx%d+%d+%d" % (w, h, x + int(str(dx)), y + int(str(dy))))
	except:
		messagebox.showinfo("Information","Dimention par defaut\n Probléme voire configuration") 
		tier.geometry('790x350')
		ligne[9] = '790x350'
	#tier.title(" Menu Tier ")
	#icone = PhotoImage(file=ligne[36]+'/Icon/Client.png')
	#tier.iconphoto(True, icone)
	#tier.protocol('WM_DELETE_WINDOW', lambda: None)
	tier.wm_attributes('-topmost', True)
	tier.transient(fenètrePrimère)
	tier.wm_attributes('-type','splash')
	#tier.bind('<Escape>',  lambda event:tier.destroy())
	def tk_popup( x, y, entry=""): 
		tk.call('tk_popup', self._w, x, y, entry) 
	def couper():
		global tier
		tier.clipboard_clear()
		a=tier.focus_get().index(ANCHOR)
		i=tier.focus_get().index(INSERT)
		if i<a:a,i=i,a
		t=tier.focus_get().get()
		s=t[a:i]
		t=t[0:a] + t[i:]
		tier.focus_get().delete(0,END)
		tier.focus_get().insert(0, t)
		tier.focus_get().icursor(a)
		tier.clipboard_append(s)
	def copier():
		tier.clipboard_clear()
		tier.clipboard_append(tier.selection_get())
	def coller():
		t=tier.selection_get(selection='CLIPBOARD')
		tier.focus_get().insert(INSERT,t)	
	def Poupmenu ():
		def showMenu(e):
			global showMenu
			pmenu.post(e.x_root, e.y_root)
		pmenu = tkinter.Menu(tier,font = 'Arial 11',relief=SUNKEN, fg = "#4D4D4D", tearoff=0)
		pmenu.add_command(label="Copier", command=copier)
		pmenu.add_command(label="Coller",command=coller)
		#pmenu.add_command(label="Effacer",command=couper)
		pmenu.add_command(label="Exit", command=tier.destroy)
		tier.bind("<Button-3>", showMenu)
	Poupmenu ()
	l10 = 0
	try:
		f = open(chem_note + 'Config_Kiwi_AE', 'r')
		ligne[9] = f.readlines()[9][:-1]
		l10 = int(str(ligne[9]).split('x',1)[0])
		lat = int(ligne[9].split('x',1)[1].split('+',1)[0])
		#if l10 < MIN_WHITE:l10 = MIN_WHITE
	except:
		pass
	frameq = Frame(tier,bg ='#000000' )#ligne[8],bd=2)
	frameq.place(x=0, y=62,width=l10, height=lat-60)
	frame = DoubleScrolledFrame(frameq, 
			width=lat -60, 
			borderwidth=1, 
			relief=tk.SUNKEN, 
			background="light gray")
	frame.pack(fill=tk.BOTH, expand=True) # fill window
	frame_inter = lat -60 #1463
	fen2 = Frame(tier,bg =ligne[8])
	fen2 = Frame(frame.inner,width=frame_inter,bd = 1)#,relief=RAISED)
	fen2.pack(side=TOP)
	info_zone = Frame(tier,bd = 2,bg =ligne[8])
	info_zone.place(x=0, y=0,width=l10, height=65)
	
	label_Conpteur = tkinter.Label(info_zone,text="Nombre de Tiers",font=('Arial 15'),bg=ligne[8])
	label_Conpteur.grid(row=0, column=0,sticky=tk.N,pady=5,padx=5)	
	Conpteur= tkinter.Label(info_zone,text=" ", fg = "green",bg =ligne[8],font = ('Arial 15'))
	Conpteur.grid(row=0, column=1 ,sticky=tk.N,pady=5,padx=5)
	hei = 45	
	
	photo = PhotoImage(file=chem_icon +'aide.png')
	photo = photo.subsample(4,4)
	nouvfac = Button(info_zone, image = photo, height = hei,width =hei,command= aide_tier)
	nouvfac.image = photo
	nouvfac.grid(row=0, column=3,ipady=5,padx=1,sticky=tk.N)	
	
	photo = PhotoImage(file=chem_icon+'Save.png')
	photo = photo.subsample(5,5)
	nouvfac = Button(info_zone, image = photo, height = hei,width =hei,command= save, compound=BOTTOM)
	nouvfac.image = photo
	nouvfac.grid(row=0, column=4,ipady=5,padx=1,sticky=tk.N)	
	def sortie_5():
		sett()
		tier.destroy()
	photo = PhotoImage(file=chem_icon+'sortie.png')
	photo = photo.subsample(5,5)
	Ba = Button(info_zone, image = photo,height = hei,width =hei,command=sortie_5)
	Ba.image = photo
	Ba.grid(row=0, column=5,ipady=5,padx=1,sticky=tk.N)		
	
	################################################## boucle 2
	colone = [ClnL.nligneClien(0,frame,supre_colone_select,frame_inter)]
	######################## Affiche	
	load()
	actualisationConteur()
	SaisieClass()
	tier.mainloop()
def actualisationConteur():
	global Conpteur, colone	
	Conpteur.config(text=len(colone))
#======================
#    gestion colone
#======================
def ad_colone(nom = "",rue = "",rue2 = "",codePostal = "",ville = "",mobil = "",fix = "",email = "",fax = ""):
	global colone,createFileBu,frame
	Dega_exitant = False
	t = 0
	l = 0
	for ligne in colone:
		if ligne.nom.get() == nom:
			Dega_exitant = True
			l = t
		t += 1
	if Dega_exitant:
		colone[l].set(nom,rue,rue2,codePostal,ville,mobil,fix,email,fax)
		actualisationConteur()
	else:
		colone.append(ClnL.nligneClien((len(colone)),frame,supre_colone_select,frame_inter))
		colone[-1].set(nom,rue,rue2,codePostal,ville,mobil,fix,email,fax)
		actualisationConteur()
def supre_colone_fin():
	global colone,createFileBu,frame
	supre_colone_select(len(colone)-1,frame)()
	actualisationConteur()
def Reset_colone():
	global colone,createFileBu
	for ligne in range(len(colone)):
		colone[0].delette()
		del colone[0]
	#createFileBu.pack()
	ad_colone()
	actualisationConteur()
def supre_colone_select(ligne):
	global colone,createFileBu,frame
	def supre_colone():
		global colone,createFileBu
		if len(colone) <= 1:
			return True
		colone[ligne].delette() 
		del colone[ligne]
		t = 0
		for ligne_saisi in colone:
			ligne_saisi.replacer(t)
			t += 1
			#createFileBu.pack()
		actualisationConteur()
	return supre_colone
if __name__ == '__main__':
	main()
