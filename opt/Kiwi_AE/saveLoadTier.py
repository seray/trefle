import datetime
import time
from tkinter import *
import lib.lireFacture
import lib.class_nline as ClnL
import os
from Config  import *

FILE_TIER ='/home/'+Nnom+'/Gestion_Kiwi_AE/Gestion/tier/listTier.dat'
Nnom=os.getlogin()
def clearSpecialCaracter(string):
	string=string.replace("é","e")
	string=string.replace("ä","a")
	string=string.replace("-",' ')
	string=string.replace("/","_")
	string=string.replace(">","_")
	string=string.replace('<','_')
	string=string.replace(":",";")
	return string
def save(colone):
	fileName = FILE_TIER
	#print(fileName)
	data = open(fileName,"w")
	# ======== Tierh
	data.write("#zone_a_imprimer#\n")
	data.write(" \n")
	data.write("#end#\n")
	data.write("#zone_de_donner#\n")
	data.write("colone~[")
	for x in colone:
		nom =        clearSpecialCaracter(x.nom.get())
		rue =        clearSpecialCaracter(x.rue.get())
		rue2 =       clearSpecialCaracter(x.rue2.get())
		codePostal = clearSpecialCaracter(x.codePostal.get())
		ville =      clearSpecialCaracter(x.ville.get())
		mobil =      x.mobil.get()
		fix =        x.fix.get()
		email =      x.email.get()
		fax =        x.fax.get()
		
		data.write('{')
		data.write('"nom" : "'        + str(nom) + '",')
		data.write('"rue" : "'        + str(rue) + '",')
		data.write('"rue2" : "'       + str(rue2) + '",')
		data.write('"codePostal" : "' + str(codePostal) + '",')
		data.write('"ville" : "'      + str(ville) + '",')
		data.write('"mobil" : "'      + str(mobil) + '",')
		data.write('"fix" : "'        + str(fix) + '",')
		data.write('"email" : "'      + str(email) + '",')
		data.write('"fax" : "'        + str(fax) + '"')
		data.write('},')
	data.write(']~\n')
	data.write("#end#\n")
	data = open(fileName,"r") # 
	print(data.read())
	data.close()
	
def load(colone,supre_colone_select,frame,frame_inter):
	text,a_apliqué = lib.lireFacture.lireDat(FILE_TIER)	
	for ligne in range(len(colone)):
		colone[len(colone)-1].delette()
		del colone[len(colone)-1]
	for x in range(len(a_apliqué['colone'])):
		colone.append(ClnL.nligneClien(len(colone),frame,supre_colone_select,frame_inter))
		colone[len(colone)-1].set(
			a_apliqué['colone'][x]['nom'],
			a_apliqué['colone'][x]['rue'],
			a_apliqué['colone'][x]['rue2'],
			a_apliqué['colone'][x]['codePostal'],
			a_apliqué['colone'][x]['ville'],
			a_apliqué['colone'][x]['mobil'],
			a_apliqué['colone'][x]['fix'],
			a_apliqué['colone'][x]['email'], 
			a_apliqué['colone'][x]['fax'])
def load_to_list():
	text,a_apliqué = lib.lireFacture.lireDat(FILE_TIER)
	#sortie = {}
	"""
	{"nom" : "Seray Roland",
	"rue" : "6 rue des Charbonnieres",
	"rue2" : "",
	"codePostal" : "79190",
	"ville" : "Melleran",
	"mobil" : "06 06 41 87 97",
	"fix" : "09 06",
	"email" : "mounierseray@free.fr"
	fax : "05 49 44 44 44"}
	"""
	#for Tier in a_apliqué['colone']:
		#sortie[Tier["nom"]] = Tier
	try:
		sortie = {}
		for Tier in a_apliqué['colone']:
			sortie[Tier["nom"]] = Tier
	except (IOError, ValueError):
		var = messagebox.showinfo("Erreure commande" , "Il faut d'abord faire une sauvegarde et aprés export en PDF")
	except:
		raise	
	return sortie
